import React from 'react';
import {
  Dimensions,
  Share,
  View,
  RefreshControl,
  StyleSheet,
  Linking,
  Alert,
  ToastAndroid,
} from 'react-native';
import ScreenWrapper from '../ui/ScreenWrapper';
import {Item} from 'react-navigation-header-buttons';
import {MaterialHeaderButtons} from '../ui/HeaderButtons';
import {inject, observer} from 'mobx-react';
import {withTranslation} from 'react-i18next';
import {SliderBox} from 'react-native-image-slider-box';
import {APP_COLOR, DEBUG_MODE} from '../utils/settings';
import {
  Caption,
  Card,
  Headline,
  Text,
  FAB,
  Portal,
  Dialog,
  Button,
  Colors,
  Subheading,
  ActivityIndicator,
} from 'react-native-paper';
import i18n from '../utils/i18n';
import {
  additionalQuestionsRenderer,
} from '../utils/generators';
import Icon from 'react-native-vector-icons/FontAwesome5';
import CategoriesRenderer from '../ui/CategoriesRenderer';
import {action, computed, observable} from 'mobx';
import {filterPhoneNumber, requester, size} from '../utils';
import SimilarAdsComponent from '../components/ad_view/SimilarAdsComponent';
import moment from 'moment';
import RootNavigation from '../utils/RootNavigation';
import {BannerAd, BannerAdSize, TestIds, RewardedAd, RewardedAdEventType} from "@react-native-firebase/admob";

const windowSizes = Dimensions.get('window');
const adUnitId = DEBUG_MODE || __DEV__ ? TestIds.BANNER : 'ca-app-pub-4787801224087227/6775157767';

const rewardedAddUnitId = __DEV__ ? TestIds.INTERSTITIAL : 'ca-app-pub-4787801224087227/2808827711';
const rewarded = RewardedAd.createForAdRequest(rewardedAddUnitId);

@withTranslation('ad_view')
@inject('store')
@observer
class AdViewScreen extends React.Component {
  @observable loading = false;
  @observable contacts_dialog_modal = false;
  @observable vip_dialog_modal = false;
  @observable free = false;
  @observable rewarded_ad_loaded = false;
  @observable item = null;

  constructor(props) {
    super(props);

    this.store = this.props.store.appStore;
    this.adStore = this.props.store.adStore;

    this.item = this.props.route.params.item;

    this.eventListener = rewarded.onAdEvent((type, error, reward) => {
      if (type === RewardedAdEventType.LOADED) {
        this.setValue('rewarded_ad_loaded', true);
      }

      if (type === RewardedAdEventType.EARNED_REWARD) {
        console.log('User earned reward of ', reward);

        requester.post('/ad/service', {
          days: reward.amount,
          type: 'vip',
          ad_id: this.props.route.params.item.id,
        }).then(res => {
          if (res.status === 'success') {
            ToastAndroid.showWithGravity(
              this.store.language === 'ru' ? "Спасибо за просмотр ваше объявление получило статус VIP" : 'Көрүү үчүн рахмат сиздин жарыя VIP статусун алды',
              ToastAndroid.SHORT,
              ToastAndroid.CENTER
            );
            this.fetchItem();
          }
        }).catch(e => {
          console.log(e);
        });
      }
    });
  }

  onShare = async () => {
    const {t} = this.props;
    try {
      await Share.share({
          title: this.item[`title_${this.store.language}`],
          message: `${
            this.item[`title_${this.store.language}`]
          } - https://som.kg/ads/view/${this.item.id}`,
        },
        {dialogTitle: t('share')},
      );
    } catch (error) {
      console.log(error.message);
    }
  };

  @action showRewardedAd = () => {
    if (!this.rewarded_ad_loaded || !this.vip_dialog_modal) return;
    rewarded.show().then((res) => {
      console.log(res);
    }).catch(err => {
      console.log(err);
    }).finally(() => {
      this.vip_dialog_modal = false;
      this.rewarded_ad_loaded = false;
    });
  };

  @action vipDismiss = () => {
    this.eventListener();
    this.vip_dialog_modal = false;
  };

  @action fetchItem = () => {
    if (this.loading) {
      return;
    }
    this.loading = true;
    this.adStore
      .fetchById(this.props.route.params.item.id)
      .then(response => {
        this.item = response;

        let w = 120;
        if (this.store.authenticated) w += 47;

        this.props.navigation.setOptions({
          headerTitleStyle: {width: windowSizes.width - w},
          headerRight: () => (
            <MaterialHeaderButtons>
              {this.store.authenticated && (
                <Item
                  title={'favorite'}
                  iconName={`favorite${this.in_favorite ? '' : '-border'}`}
                />
              )}
              <Item title={'share'} onPress={this.onShare} iconName={'share'}/>
            </MaterialHeaderButtons>
          ),
        });
      })
      .catch(error => console.log(error))
      .finally(() => (this.loading = false));
  };

  @action liftUpCheckBalance = async () => {
    if (this.lift_up_loading) {
      return false;
    }
    try {
      this.lift_up_loading = true;
      const data = await requester.get('/ad/lift_up', {id: this.item.id});
      if (data.status === 'success') {
        this.store.user.setBalance(data.data.balance);
        this.free = data.data.free;
      }
    } catch (e) {
      console.log(e);
    } finally {
      this.lift_up_loading = false;
    }
  };

  @action liftUpOpen = () => {
    if (this.lift_up_dialog_modal) {
      return false;
    }
    this.lift_up_dialog_modal = true;
    this.liftUpCheckBalance().then(() => {
    });
  };

  @computed get in_favorite() {
    return this.store.favorites.find(
      v => v.product_type === 'ad' && v.product_id === this.item.id,
    );
  }

  @observable deleting = false;

  @action confirmDelete = () => {
    if (this.deleting) return;
    const {t} = this.props;
    Alert.alert(t('confirm'), 'Вы уверены, что удалите это объявление?', [
      {
        text: t('cancel'),
        style: 'cancel',
      },
      {
        text: t('Delete'),
        onPress: () => this.delete(),
      },
    ]);
  };

  @action delete = async () => {
    if (this.deleting) return;

    this.deleting = true;

    try {
      await this.adStore.deleteById(this.item.id);
      this.store.showSuccess('Успешно удалено!');
      this.adStore.fetchMyItems().then();
      this.props.navigation.goBack();
    } catch (e) {
      console.log(e);
    } finally {
      this.deleting = false;
    }
  };

  @action setValue = (n, v) => (this[n] = v);

  componentDidMount() {
    this.fetchItem();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.route.params.item.id !== prevProps.route.params.item.id) {
      this.fetchItem();
    }
  }

  showContacts = () => {
    if (!this.item.author) {
      return;
    }
    this.setValue('contacts_dialog_modal', true);
  };

  openContact = (type, value) => {
    if (type === 'whatsapp') {
      Linking.openURL(
        `whatsapp://send?phone=+${value}&text=${"Саламатсызбы! Мен SOM.KG'деги жарыя боюнча кайрылып жатам."}`,
      );
    } else {
      Linking.openURL(`tel:+${value}`);
    }
  };

  renderPhone = (phone, k) => {
    if (!phone) {
      return;
    }
    phone = filterPhoneNumber(phone);
    return (
      <React.Fragment key={k}>
        <Button
          onPress={() => this.openContact('phone', phone.phone)}
          icon={'phone'}>
          +{phone.phone}
        </Button>
        {phone.whatsapp && (
          <Button
            onPress={() => this.openContact('whatsapp', phone.phone)}
            icon={'whatsapp'}>
            +{phone.phone}
          </Button>
        )}
      </React.Fragment>
    );
  };

  eventListener = null;

  @action requestRewardedAd = () => {
    if (this.vip_dialog_modal) return;
    this.vip_dialog_modal = true;

    if (!this.rewarded_ad_loaded) {
      rewarded.load();
    }
  };

  @action liftUpDismiss = () => {
    if (this.lift_up_loading) {
      return false;
    }
    this.lift_up_dialog_modal = false;
  };

  @action liftUp = async () => {
    if (this.lift_up_loading) {
      return false;
    }
    this.lift_up_loading = false;
    try {
      const data = await requester.post('/ad/lift_up', {id: this.item.id});
      if (data.status === 'success') {
        const {raised_on, user_balance} = data.data;
        this.item.setRaisedOn(raised_on);
        this.store.user.setBalance(user_balance);
        this.store.showSuccess('Успешно поднято!');
        this.lift_up_dialog_modal = false;
      }
    } catch (e) {
      console.log(e);
    } finally {
      this.lift_up_loading = false;
    }
  };

  userView = user => {
    RootNavigation.push('UserView', {user});
  };

  @observable lift_up_loading = false;
  @observable lift_up_dialog_modal = false;

  render() {
    const {t} = this.props;
    const balance_is_enough = this.store.user
      ? this.store.user.balance >= 5
      : false;
    const owner =
      this.store.authenticated &&
      this.store.user &&
      this.item.author &&
      this.store.user.id === this.item.author.id;

    return (
      <>
        <Portal>
          <Dialog
            visible={this.contacts_dialog_modal}
            onDismiss={() => this.setValue('contacts_dialog_modal', false)}>
            <Dialog.Content>
              <Text style={{marginBottom: 8}}>
                {t('please_tell_to_the_owner')}
              </Text>
              {this.item.phones.map((v, k) => this.renderPhone(v, k))}
            </Dialog.Content>
          </Dialog>

          <Dialog
            visible={this.lift_up_dialog_modal}
            onDismiss={() => this.liftUpDismiss()}>
            <Dialog.Content>
              {this.lift_up_loading ? (
                <ActivityIndicator/>
              ) : (
                <>
                  <Subheading style={{marginBottom: 8}}>
                    {t('Lift_up')}
                    {!this.free ? ' 5 сом' : null}
                  </Subheading>
                  <Caption style={{marginBottom: 8}}>
                    Поднимается выше бесплатных объявлений и дата публикации
                    обновляется
                  </Caption>
                  <Button
                    mode={'outlined'}
                    onPress={() => this.liftUp()}
                    icon={'check-circle'}
                    loading={this.lift_up_loading}
                    disabled={
                      this.lift_up_loading || (!balance_is_enough && !this.free)
                    }>
                    {t('Lift')}
                  </Button>
                </>
              )}
            </Dialog.Content>
          </Dialog>

          <Dialog
            visible={this.vip_dialog_modal}
            onDismiss={() => this.vipDismiss()}>
            <Dialog.Content>
              {!this.rewarded_ad_loaded ? (
                <ActivityIndicator/>
              ) : (
                <>
                  <Text style={{marginBottom: 8}}>
                    {this.store.language === 'ru' ? 'Посмотрите видео и получите статус VIP' : 'Видео көрүңүз жана VIP статусун алыңыз'}
                  </Text>
                  <Button
                    mode={'outlined'}
                    onPress={() => this.showRewardedAd()}
                    icon={'video'}>
                    {this.store.language === 'ru' ? 'Посмотреть видео' : 'Видео көрүү'}
                  </Button>
                </>
              )}
            </Dialog.Content>
          </Dialog>
        </Portal>

        <ScreenWrapper
          scroll
          scrollViewProps={{
            refreshControl: (
              <RefreshControl
                onRefresh={() => this.fetchItem()}
                refreshing={this.loading}
              />
            ),
          }}>
          {this.item.original_images.length > 0 && (
            <SliderBox
              dotColor={APP_COLOR}
              disableOnPress={true}
              parentWidth={Number(windowSizes.width.toFixed())}
              sliderBoxHeight={Number((windowSizes.height / 3).toFixed())}
              imageLoadingColor={APP_COLOR}
              resizeMode={'contain'}
              images={this.item.original_images}
            />
          )}

          <Headline style={{marginHorizontal: 8, marginTop: 8}}>
            {this.item[`title_${i18n.language}`]}
          </Headline>

          <View
            style={{
              marginHorizontal: 8,
              flexWrap: 'wrap',
              justifyContent: 'flex-start',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <CategoriesRenderer
              items={this.item.categories_path}
              language={i18n.language}
            />
          </View>

          <Card elevation={2} style={{marginHorizontal: 8, marginTop: 8}}>
            <Card.Content>
              {additionalQuestionsRenderer(this.item.additionals).map(
                (v, k) => (
                  <View
                    style={{
                      justifyContent: 'flex-start',
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}
                    key={k}>
                    {v.icon && (
                      <Icon
                        style={{marginRight: 8, width: 20, textAlign: 'center'}}
                        size={15}
                        name={v.icon}
                      />
                    )}
                    {v.title && (
                      <Caption style={{marginRight: 4, fontSize: 15}}>
                        {v.title}:
                      </Caption>
                    )}
                    <Text style={{fontSize: 15}}>{v.content}</Text>
                  </View>
                ),
              )}
            </Card.Content>
          </Card>

          <View flex={1} style={{marginTop: size(4)}} alignItems={'center'} justifyContent={'center'}>
            <BannerAd
              unitId={adUnitId}
              size={BannerAdSize.LARGE_BANNER}
            />
          </View>

          {this.item[`description_${i18n.language}`] ? (
            <Card elevation={2} style={{marginHorizontal: 8, marginTop: 8}}>
              <Card.Content>
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: 'bold',
                    fontFamily: 'SourceSansPro-Light',
                    color: Colors.black,
                  }}>
                  {this.item[`description_${i18n.language}`]}
                </Text>
              </Card.Content>
            </Card>
          ) : null}

          {this.item.author && (
            <Card elevation={2} style={{marginHorizontal: 8, marginTop: 8}}>
              <Card.Content>
                {this.item['published_on'] ? (
                  <View
                    style={{
                      justifyContent: 'flex-start',
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <Caption style={{marginRight: 4, fontSize: 15}}>
                      {t('published_on')}:
                    </Caption>
                    <Text style={{fontSize: 15}}>
                      {moment(this.item['published_on']).calendar()}
                    </Text>
                  </View>
                ) : null}
                {this.item['raised_on'] ? (
                  <View
                    style={{
                      justifyContent: 'flex-start',
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <Caption style={{marginRight: 4, fontSize: 15}}>
                      {t('raised_on')}:
                    </Caption>
                    <Text style={{fontSize: 15}}>
                      {moment(this.item['raised_on']).calendar()}
                    </Text>
                  </View>
                ) : null}
                <View
                  style={{
                    justifyContent: 'flex-start',
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Caption style={{marginRight: 4, fontSize: 15}}>
                    {t('author')}:
                  </Caption>
                  <Text style={{fontSize: 15}}>
                    {this.item['author'].full_name}
                  </Text>
                </View>
                {(this.store.user &&
                  this.store.authenticated &&
                  this.item.author.id !== this.store.user.id) || !this.store.user ? (
                  <Button
                    onPress={() => this.userView(this.item.author)}
                    mode={'text'}
                    style={{marginTop: 8}}>
                    {t('Other_user_ads')}
                  </Button>
                ) : null}
              </Card.Content>
            </Card>
          )}
          {owner ? (
            <View>
              {this.item.status !== 'sold' && !this.item.active_service && this.item.status !== 'rejected' ? (
                <Button
                  disabled={this.deleting}
                  icon="star"
                  style={{
                    marginHorizontal: 8,
                    marginTop: 8,
                  }}
                  mode="contained"
                  onPress={() => this.requestRewardedAd()}>
                  {this.store.language === 'ru' ? 'Включить VIP' : 'VIP статусун алуу'}
                </Button>
              ) : null}
              {this.item.status === 'published' ? (
                <>
                  <Button
                    disabled={this.deleting}
                    icon="arrow-up"
                    style={{
                      marginHorizontal: 8,
                      marginTop: 8,
                      backgroundColor: '#fff',
                    }}
                    mode="outlined"
                    onPress={() => this.liftUpOpen()}>
                    {t('Lift')}
                  </Button>
                </>
              ) : null}
              <View flex={1} justifyContent={'space-between'} flexDirection={'row'} alignItems={'center'}>
                <Button
                  flexGrow={1}
                  disabled={this.deleting || this.item.status === 'sold'}
                  icon="pencil"
                  style={{margin: 8, backgroundColor: '#fff'}}
                  mode="outlined"
                  onPress={() =>
                    RootNavigation.push('AdEdit', {item: this.item})
                  }>
                  {t('Edit')}
                </Button>
                <Button
                  flexGrow={1}
                  color={'rgb(255, 80, 80)'}
                  loading={this.deleting}
                  disabled={this.deleting}
                  icon="delete"
                  style={{marginRight: 8, backgroundColor: '#fff'}}
                  mode="outlined"
                  onPress={() => this.confirmDelete()}>
                  {t('Delete')}
                </Button>
              </View>
            </View>
          ) : null}

          {this.item.categories_path.length > 0 ? <SimilarAdsComponent category={this.item.categories_path[0]}/> : null}
        </ScreenWrapper>

        <FAB
          loading={!this.item.author}
          style={styles.fab}
          label={t('contact_with_owner')}
          icon="phone"
          onPress={() => this.showContacts()}
        />
      </>
    );
  }
}

const styles = StyleSheet.create({
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
  },
});

export default AdViewScreen;
