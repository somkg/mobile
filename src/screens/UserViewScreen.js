import React from "react";
import {FlatList, View} from 'react-native';
import {inject, observer} from "mobx-react";
import AdItem from "../ui/AdItem";
import {ActivityIndicator, Avatar, Card, Colors, Headline, Subheading, Text, Title} from "react-native-paper";
import Icon from "react-native-vector-icons/MaterialIcons";
import {APP_COLOR} from "../utils/settings";
import moment from "moment";
import User from "../models/User";
import {withTranslation} from "react-i18next";
import Ad from "../models/Ad";
import {requester, size} from "../utils";
import {action, observable} from "mobx";

@withTranslation('profile')
@inject('store')
@observer
class UserViewScreen extends React.Component {
  @observable loading = false;
  @observable items = {counts: {all: 0}, items: {all: []}};
  @observable next_page_disabled = false;

  @observable next_page_loading = false;
  @observable current_page = 1;

  constructor(props) {
    super(props);

    this.user = new User(this.props.route.params.user);
    this.store = this.props.store.appStore;
  }

  componentDidMount(): void {
    this.props.navigation.setOptions({
      title: this.user.full_name,
    });

    this.fetchItems();
  }

  @action fetchItems = async (page = 1) => {
    if (page === 1) this.loading = true;
    try {
      const data = await requester.get('/ad/get_items_by_author_id', {
        id: this.user.id,
        page
      });
      if (data.status === 'success') {
        const items = data.data;

        if (page === 1) {
          items['items'] = {
            ...items['items'],
            all: items.items.all.map(v => new Ad(v)),
            sold: items.items.sold.map(v => new Ad(v)),
            published: items.items.published.map(v => new Ad(v)),
          };
          if ('rejected' in items.items) items.items.rejected = items.items.rejected.map(v => new Ad(v));
          if ('pending' in items.items) items.items.pending = items.items.pending.map(v => new Ad(v));
          this.items = items;
        } else {
          items['items'] = {
            ...items['items'],
            all: [...this.items.items.all, ...items.items.all.map(v => new Ad(v))],
            sold: [...this.items.items.sold, ...items.items.sold.map(v => new Ad(v))],
            published: [...this.items.items.published, ...items.items.published.map(v => new Ad(v))],
          };
          if ('rejected' in items.items) items.items.rejected = [...this.items.items.rejected, ...items.items.rejected.map(v => new Ad(v))];
          if ('pending' in items.items) items.items.pending = [...this.items.items.pending, ...items.items.pending.map(v => new Ad(v))];
          this.items = items;
        }
      }
    } catch (e) {
      console.log(e);
    } finally {
      if (page === 1) this.loading = false;
    }
  };

  @action nextPage = async () => {
    if (this.next_page_loading || this.loading || this.next_page_disabled) return;
    this.next_page_loading = true;
    this.current_page++;
    try {
      let s = this.items.items.all.length;
      await this.fetchItems(this.current_page);
      if (s === this.items.items.all.length) {
        this.current_page--;
        this.next_page_disabled = true;
      }
    } catch (e) {
      this.current_page--;
    } finally {
      this.next_page_loading = false;
    }
  };

  render() {
    const {user} = this;
    const {t} = this.props;
    const last_action = moment(user.last_action);
    const is_online = moment().diff(last_action, 'minutes') <= 10;

    return (
      <FlatList
        style={{margin: 8}}
        data={this.loading ? [] : this.items.items.all}
        renderItem={({item}) => (
          <View style={{flex: 1}}>
            <AdItem data={item}/>
          </View>
        )}
        ListHeaderComponent={(
          <>
            <View flexDirection={'column'} alignItems={'center'} style={{marginVertical: 25}} justifyContent={'center'}>
              {user.picture ? <Avatar.Image size={80} source={{uri: user.picture}}/> :
                <Avatar.Text size={80} label={user.short_name}/>}
              <View style={{marginTop: 15}} flexDirection={'row'} alignItems={'center'}>
                <Headline style={{
                  fontSize: 20,
                  marginRight: user.is_agent ? 5 : 0,
                  color: 'rgba(0, 0, 0, 0.6)',
                }}>{user.full_name}</Headline>
                {user.is_agent ? <Icon color={APP_COLOR} size={20} name={'verified-user'}/> : null}
              </View>
            </View>
            <Card>
              <Card.Content>
                <Text>Идентификатор: {user.id}</Text>
                <Text>Регион: {user.place[`title_${this.store.language}`]}</Text>
                <Text>{t('date_of_last_action', {date: is_online ? 'онлайн' : last_action.calendar()})}</Text>
                <Text>{t('date_of_registration', {date: moment(user.created_at).calendar()})}</Text>
              </Card.Content>
            </Card>
            <Title style={{marginTop: 8, color: Colors.grey600}}>{t('ads')} ({this.items.counts.all})</Title>
          </>
        )}
        numColumns={2}
        keyExtractor={(item, index) => index.toString()}
        onEndReached={() => this.nextPage()}
        progressViewOffset={20}
        ListFooterComponent={this.next_page_loading ? <ActivityIndicator/> : null}
        ListFooterComponentStyle={{margin: size(8)}}
        ListEmptyComponent={(
          <Card style={{marginTop: 6}}>
            <Card.Content>
              {this.loading ? (
                <ActivityIndicator/>
              ) : (
                <Subheading style={{color: Colors.grey500, textAlign: 'center', fontWeight: 'bold'}}>{t('no_ads')}</Subheading>
              )}
            </Card.Content>
          </Card>
        )}
      />
    );
  }
}

export default UserViewScreen;
