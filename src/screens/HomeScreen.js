import React from 'react';
import {View, FlatList, TouchableOpacity, Linking, Image} from 'react-native';
import {inject, observer} from 'mobx-react';
import ScreenWrapper from '../ui/ScreenWrapper';
import {action, observable} from 'mobx';
import {requester} from '../utils';
import {ActivityIndicator, Dialog, List, Portal, Text} from 'react-native-paper';
import Ad from '../models/Ad';
import {withTranslation} from 'react-i18next';
import AdItem from '../ui/AdItem';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {APP_COLOR, APP_NAME, DEBUG_MODE} from '../utils/settings';
import {MaterialHeaderButtons} from '../ui/HeaderButtons';
import {HiddenItem, Item} from 'react-navigation-header-buttons';
import RootNavigation from '../utils/RootNavigation';
import AdViewScreen from './AdViewScreen';
import HelpScreen from './HelpScreen';
import AboutUsScreen from './AboutUsScreen';
import {createStackNavigator} from '@react-navigation/stack';
import SearchScreen from './SearchScreen';
import AdFilterByPlaceModal from '../components/AdFilterByPlaceModal';
import i18n from '../utils/i18n';
import UserViewScreen from './UserViewScreen';
import AdEditScreen from './AdEditScreen';
import LanguageModal from '../components/LanguageModal';
import {BannerAd, TestIds, BannerAdSize} from '@react-native-firebase/admob';
import FavoritesScreen from "./FavoritesScreen";

const Stack = createStackNavigator();
const adUnitId = DEBUG_MODE || __DEV__ ? TestIds.BANNER : 'ca-app-pub-4787801224087227/6775157767';

@inject('store') @observer
class Main extends React.Component {
  constructor(props) {
    super(props);

    this.store = this.props.store.appStore;
    this.adStore = this.props.store.adStore;
    this.placeStore = this.props.store.placeStore;

    this.current_page = 1;
    this.last_ads = -1;
  }

  @observable ads = observable.object({
    last: [],
  });

  @observable ad_filter_by_place_modal = false;
  @observable language_modal = false;

  @observable loading = observable.object({
    ads: false,
    ads_next_page: false,
  });

  @action fetchAds = async (page = 1, ignore_loading = false) => {
    if (this.last_ads === 0) {
      return;
    }
    this.current_page = page;
    if (!ignore_loading && (this.loading.ads || this.loading.ads_next_page)) {
      return;
    }
    if (page === 1) {
      this.loading.ads = true;
    } else {
      this.loading.ads_next_page = true;
    }

    let filters = {};

    if (!this.adStore.filter_is_disabled && this.store.place) {
      filters['place'] = this.store.place.id;
    }

    try {
      const data = await requester.get('/ad/items', {
        to: 'home',
        limit: 16,
        page: this.current_page,
        filters,
      });
      if (this.current_page === 1) {
        this.store.storage.set('last_ads', data.data.last);
      }
      this.last_ads = data.data.last.length;
      if (page > 1) {
        this.ads = {
          last: [...this.ads.last, ...data.data.last.map(v => new Ad(v))],
        };
      } else {
        this.ads = {
          last: data.data.last.map((v) => {
            return new Ad(v);
          }),
        };
      }
    } catch (e) {
      console.log(e);
    } finally {
      this.loading.ads = false;
      this.loading.ads_next_page = false;
    }
  };

  @action fetchAdsFromStorage = async () => {
    this.ads.last = (await this.store.storage.get('last_ads', [])).map(v => new Ad(v));
  };

  componentDidMount() {
    this.fetchAdsFromStorage().then(() => {
    }).catch(e => console.log(e));
    this.fetchAds().then(() => {
    }).catch(e => console.log(e));

    this.navigation_focus = this.props.navigation.addListener('focus', () => {
      this.fetchAds().then(() => {
      }).catch(e => console.log(e));
    });
  }

  componentWillUnmount() {
    this.navigation_focus();
  }

  @action toggleAdFilterByPlaceModal(s) {
    this.ad_filter_by_place_modal = s;
  }

  @action toggleLanguageModal(s) {
    this.language_modal = s;
  }

  render() {
    return (
      <ScreenWrapper>
        <AdFilterByPlaceModal
          dismiss={() => this.toggleAdFilterByPlaceModal(false)}
          onChange={() => this.fetchAds()}
          show={this.ad_filter_by_place_modal}/>

        <LanguageModal
          dismiss={() => this.toggleLanguageModal(false)}
          onSelect={() => this.toggleLanguageModal(false)}
          show={this.language_modal}/>

        <FlatList
          ListHeaderComponent={(
            <>
              <View style={{
                justifyContent: 'space-between',
                alignItems: 'center', flexDirection: 'row',
                fontWeight: 'bold', marginLeft: 8, marginTop: 8,
                marginRight: 8,
              }}>
                <TouchableOpacity
                  onPress={() => this.toggleLanguageModal(true)}
                  style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Icon size={20} name={'flag'}/>
                  <Text style={{
                    fontSize: 20,
                    marginLeft: 4,
                    color: APP_COLOR,
                    fontWeight: 'bold',
                  }}>{this.store.language_label}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.toggleAdFilterByPlaceModal(true)}
                                  style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Icon size={20} name={'room'}/>
                  <Text style={{
                    fontSize: 20,
                    marginLeft: 4,
                    color: APP_COLOR,
                    fontWeight: 'bold',
                  }}>{this.adStore.filter_name}</Text>
                </TouchableOpacity>
              </View>
            </>
          )}
          refreshing={this.loading.ads}
          onRefresh={() => this.fetchAds()}
          data={this.ads.last}
          renderItem={({item, index}) => {
            return (
              <>
                <View style={{flex: 1}}>
                  <AdItem data={item}/>
                </View>
                {(index + 1) % 3 === 0 ? (
                  <View flex={1} style={{marginTop: 8}} alignItems={'center'} justifyContent={'center'}>
                    <BannerAd
                      unitId={adUnitId}
                      size={BannerAdSize.LARGE_BANNER}
                    />
                  </View>
                ) : null}
              </>
            );
          }}
          onEndReached={() => this.fetchAds(this.current_page + 1)}
          keyExtractor={(item, index) => index.toString()}
          ListFooterComponent={this.loading.ads_next_page ? <ActivityIndicator/> : null}
          ListFooterComponentStyle={{margin: 16}}
        />

      </ScreenWrapper>
    );
  }
}

function LogoTitle() {
  return (
    <Image
      style={{width: 114, height: 44, backgroundColor: '#fff'}}
      source={require('../images/header-logo.png')}
    />
  );
}

@withTranslation('home')
@observer
class HomeScreen extends React.Component {
  @observable contacts_modal = false;

  @action toggleContactsModal = (s) => {
    this.contacts_modal = s;
  };

  render() {
    const {t} = this.props;

    return (
      <>
        <Portal>
          <Dialog
            visible={this.contacts_modal}
            onDismiss={() => this.toggleContactsModal(false)}>
            <Dialog.Content>
              <List.Item
                onPress={() => Linking.openURL(`tel:+996775600691`)}
                title={'+996 775 600 691'}
                left={props => <List.Icon {...props} icon={'phone'}/>}
              />
              <List.Item
                onPress={() => Linking.openURL(`whatsapp://send?phone=+996775600691&text='}`)}
                title={'+996 775 600 691'}
                left={props => <List.Icon {...props} icon={'whatsapp'}/>}
              />
              <List.Item
                onPress={() => Linking.openURL(`mailto:somkg2018@gmail.com`)}
                title={'somkg2018@gmail.com'}
                left={props => <List.Icon {...props} icon={'gmail'}/>}
              />
            </Dialog.Content>
          </Dialog>
        </Portal>
        <Stack.Navigator screenOptions={{
          title: APP_NAME,
          headerStyle: {
            backgroundColor: APP_COLOR,
          },
          headerTintColor: '#fff',
        }} initialRouteName={'Main'}>
          <Stack.Screen
            name={'Main'}
            component={Main}
            options={{
              headerTitle: props => <LogoTitle {...props} />,
              headerRight: () => (
                <MaterialHeaderButtons>
                  <Item title={'search'} onPress={() => RootNavigation.navigate('Search')}
                        iconName={'search'}/>
                  <Item title={'favorites'} onPress={() => RootNavigation.navigate('Favorites')}
                        iconName={'favorite-border'}/>
                  <HiddenItem onPress={() => this.toggleContactsModal(true)} title={t('contacts')}/>
                  <HiddenItem onPress={() => RootNavigation.navigate('AboutUs')}
                              title={t('about_us')}/>
                  <HiddenItem onPress={() => RootNavigation.navigate('Help')} title={t('help')}/>
                  <HiddenItem onPress={() => Linking.openURL('https://som.kg/terms')} title={t('terms')}/>
                </MaterialHeaderButtons>
              ),
            }}/>

          <Stack.Screen
            name={'AdView'}
            component={AdViewScreen}
            options={({route}) => ({
              title: route.params.item[`title_${i18n.language}`],
            })}/>
          <Stack.Screen
            name={'AdEdit'}
            component={AdEditScreen}
            options={({route}) => ({
              title: route.params.item[`title_${i18n.language}`],
            })}/>
          <Stack.Screen
            name={'Search'}
            component={SearchScreen}
            options={{
              title: t('search'),
            }}/>
          <Stack.Screen
            name={'UserView'}
            component={UserViewScreen}/>
          <Stack.Screen
            name={'Help'}
            component={HelpScreen}
            options={{
              title: t('help'),
            }}/>
          <Stack.Screen
            name={'Favorites'}
            component={FavoritesScreen}
            options={{
              title: t('favorites'),
            }}/>
          <Stack.Screen
            name={'AboutUs'}
            component={AboutUsScreen}
            options={{
              title: t('about_us'),
            }}/>
        </Stack.Navigator>
      </>
    );
  }
}

export default HomeScreen;
