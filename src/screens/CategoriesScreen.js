import React from 'react';
import {ActivityIndicator, Appbar, Divider, List, Text} from 'react-native-paper';
import {withTranslation} from 'react-i18next';
import {action, computed, observable} from 'mobx';
import {inject, observer, Observer} from 'mobx-react';
import {requester} from '../utils';
import storage from '../utils/storage';
import {View, FlatList, TouchableOpacity} from 'react-native';
import Loading from '../ui/Loading';
import FastImage from 'react-native-fast-image';
import AdItem from '../ui/AdItem';
import Ad from '../models/Ad';
import AdFilterByPlaceModal from '../components/AdFilterByPlaceModal';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {APP_COLOR} from '../utils/settings';

@withTranslation() @inject('store') @observer
class CategoriesScreen extends React.Component {
  @observable loading = false;
  @observable fetching_ads = false;
  @observable items = observable.array([]);
  @observable ads = observable.array([]);
  @observable page = 1;
  @observable incrementing = false;
  @observable ad_filter_by_place_modal = false;

  @action fetchItemsFromStorage = async () => {
    this.items = await storage.get('categories', []);
  };

  @action fetchItems = async () => {
    if (this.loading) {
      return;
    }
    this.loading = true;
    try {
      const data = await requester.get('/category/get');
      this.items = data.data;
      await storage.set('categories', data.data);
    } catch (e) {
      console.log(e);
      this.items = [];
    } finally {
      this.loading = false;
    }
  };

  async componentDidMount() {
    const p = this.getItemFromParams();
    if (p) {
      this.fetchAds(p, true);
    }
    await this.fetchItemsFromStorage();
    await this.fetchItems();
  }

  openCategory = (item) => {
    this.props.navigation.push('Categories', {item});
  };

  renderItem = ({item}, params_item) => {
    let active_children = item.children;
    const {t} = this.props;

    if ([47, 136].includes(item.id)) {
      active_children = item.children[0].children;
    }

    return (
      <List.Item
        justifyContent={'center'}
        onPress={() => this.openCategory(item)}
        title={item[`title_${t('language')}`]}
        description={active_children.map(l => l[`title_${t('language')}`]).join(', ')}
        left={() => params_item ? null : (
          <View style={{width: 35, height: 60, marginRight: 4, justifyContent: 'center', alignItems: 'center'}}>
            <FastImage
              style={{width: 35, height: 35}}
              source={{uri: `https://som.kg/images/icons/${item.icon}`}}
              resizeMode={FastImage.resizeMode.cover}
            />
          </View>
        )}
      />
    );
  };

  @observable no_ads = 0;

  @action nextPage = (c, cl) => {
    if (this.fetching_ads || this.incrementing || this.no_ads > 0) {
      return;
    }
    this.page++;
    this.fetchAds(c, cl).then(() => {
    });
  };

  @action fetchAds = async (c, cl) => {
    this.fetching_ads = this.page === 1;
    this.incrementing = this.page > 1;
    try {
      let ads = await this.props.store.adStore.fetchByCategory(c.id, this.page);
      ads = ads.items.map(v => new Ad(v));
      if (ads.length === 0) {
        this.no_ads = this.page;
      }
      if (cl) {
        this.ads = [];
        this.page = 1;
      }
      this.ads = [...this.ads, ...ads];
    } catch (e) {
      console.log(e);
      if (this.incrementing) {
        this.page--;
      }
    } finally {
      this.fetching_ads = false;
      this.incrementing = false;
    }
  };

  getItemFromParams = () => {
    return this.props.route.params && 'item' in this.props.route.params ? this.props.route.params.item : null;
  };

  @computed get title() {
    const {t} = this.props;
    const item = this.getItemFromParams();
    if (!item) {
      return t('categories');
    }
    return item[`title_${t('language')}`];
  }

  @computed get items_to_show() {
    const item = this.getItemFromParams();
    if (!item) {
      return this.items;
    }
    return item.children;
  }

  @action toggleAdFilterByPlaceModal(s) {
    this.ad_filter_by_place_modal = s;
  }

  render() {
    if (this.loading && this.items_to_show.length === 0) {
      return <View flex={1}><Loading/></View>;
    }
    const params_item = this.getItemFromParams();

    return (
      <>
        <Appbar.Header>
          {params_item && <Appbar.BackAction onPress={() => this.props.navigation.goBack()}/>}
          <Appbar.Content title={this.title}/>
        </Appbar.Header>
        <AdFilterByPlaceModal
          dismiss={() => this.toggleAdFilterByPlaceModal(false)}
          onChange={() => this.fetchAds(params_item, true)}
          show={this.ad_filter_by_place_modal}/>
        <View style={{padding: 8, paddingBottom: 48}}>
          {params_item && (
            <TouchableOpacity
              onPress={() => this.toggleAdFilterByPlaceModal(true)}
              style={{flexDirection: 'row', marginBottom: 8, alignItems: 'center'}}
            >
              <Icon size={20} name={'room'}/>
              <Text style={{
                fontSize: 20,
                marginLeft: 4,
                color: APP_COLOR,
                fontWeight: 'bold',
              }}>{this.props.store.adStore.filter_name}</Text>
            </TouchableOpacity>
          )}
          <FlatList
            ItemSeparatorComponent={() => <Divider/>}
            data={this.items_to_show}
            renderItem={(a) => this.renderItem(a, params_item)}
            keyExtractor={(item, index) => index.toString()}
            // style={{marginBottom: size(18)}}
            ListFooterComponent={(
              <Observer>
                {() => (
                  <>
                    {params_item && this.fetching_ads && (
                      <View style={{marginVertical: 24}}>
                        <ActivityIndicator/>
                      </View>
                    )}
                    {params_item && !this.fetching_ads && this.ads.length > 0 && (
                      <FlatList
                        data={this.ads}
                        renderItem={({item}) => (
                          <View style={{flex: 1}}>
                            <AdItem data={item}/>
                          </View>
                        )}
                        progressViewOffset={20}
                        onEndReached={() => this.nextPage(params_item, false)}
                        keyExtractor={(item, index) => index.toString()}
                        ListFooterComponent={(
                          <>
                            {this.incrementing && (
                              <View style={{marginVertical: 24}}>
                                <ActivityIndicator/>
                              </View>
                            )}
                          </>
                        )}
                      />
                    )}
                  </>
                )}
              </Observer>
            )}
          />
        </View>
      </>
    );
  }
}

export default CategoriesScreen;
