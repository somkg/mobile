import React from "react";
import {inject, observer} from "mobx-react";
import AuthComponent from "../components/profile/AuthComponent";
import Loading from "../ui/Loading";
import {createStackNavigator} from "@react-navigation/stack";
import MainComponent from "../components/notifications/MainComponent";
import {APP_COLOR} from "../utils/settings";
import {withTranslation} from "react-i18next";

const Stack = createStackNavigator();

@withTranslation('notifications') @inject('store') @observer
class NotificationsScreen extends React.Component {
  constructor(props) {
    super(props);

    this.store = this.props.store.appStore;
  }

  render() {
    if (this.store.authenticating) {
      return <Loading/>;
    }

    const {t} = this.props;

    return (
      <Stack.Navigator screenOptions={{
        title: t('title'),
        headerStyle: {
          backgroundColor: APP_COLOR,
        },
        headerTintColor: '#fff',
      }}>
        {this.store.authenticated ? (
          <Stack.Screen name={'Main'} component={MainComponent} />
        ) : (
          <Stack.Screen name={'SignIn'}>
            {props => <AuthComponent {...props} helpText={true} />}
          </Stack.Screen>
        )}
      </Stack.Navigator>
    );
  }
}

export default NotificationsScreen;
