import React from 'react';
import {Image, View} from "react-native";
import {Button, Text} from "react-native-paper";
import {APP_COLOR} from "../../utils/settings";
import ViewPager from '@react-native-community/viewpager';
import {action, observable} from "mobx";
import {observer} from "mobx-react";
import {normalize, percentage, SCREEN_HEIGHT, size} from "../../utils";
import storage from "../../utils/storage";

export const CODE = 1;

const items = [
  {title: 'Переходим в свой профиль', image: require('../../images/lift-up/step-1.jpeg')},
  {title: 'Спускаемся ниже', image: require('../../images/lift-up/step-2.jpeg')},
  {title: 'Выбираем любое объвление из списка', image: require('../../images/lift-up/step-3.jpeg')},
  {title: 'Нажимаем "Поднять вверх"', image: require('../../images/lift-up/step-4.jpeg')},
  {title: '', image: require('../../images/lift-up/step-5.jpeg')},
];

@observer
class LiftUpModalScreen extends React.Component {
  @observable current_page = 0;
  view_pager_ref = React.createRef();

  dismiss = () => {
    this.props.navigation.goBack();
    storage.set('skipped_lift_up_modal', CODE).then(() => {});
  };

  @action next = () => {
    this.current_page++;
    this.view_pager_ref.setPage(this.current_page);
  };

  @action onChangePage = (page) => {
    this.current_page = page;
  };

  render() {
    return (
      <View style={{
        flex: 1,
        paddingVertical: size(10),
        alignItems: 'center',
        backgroundColor: APP_COLOR,
        justifyContent: 'space-between'
      }}>
        <Text style={{textAlign: 'center', marginHorizontal: size(4), fontSize: normalize(6), color: '#fff'}}>Бесплатная услуга "Поднять вверх"</Text>

        <ViewPager
          onPageSelected={({nativeEvent}) => this.onChangePage(nativeEvent.position)}
          ref={r => this.view_pager_ref = r}
          style={{flex: 1, flexGrow: 1, width: '100%', height: '100%'}}
          initialPage={0}
        >
          {items.map((v, i) => (
            <View key={i + 1} style={{justifyContent: 'center', height: '100%', alignItems: 'center'}}>
              <Image style={{height: percentage(SCREEN_HEIGHT, 55), width: '100%'}} resizeMode={'contain'} source={v.image}/>
              {v.title ?
                <Text style={{
                  textAlign: 'center',
                  fontSize: normalize(5),
                  marginTop: size(4),
                  color: '#fff'
                }}>{v.title}</Text> : null}
            </View>
          ))}
        </ViewPager>

        <View flexDirection={'row'} style={{marginHorizontal: size(6)}}>
          {this.current_page === (items.length - 1) ? (
            <Button
              mode={'outlined'} flexGrow={1} onPress={() => this.dismiss()}
              style={{backgroundColor: '#fff'}}>ОК</Button>
          ) : (
            <>
              <Button
                mode={'outlined'} flexGrow={2} onPress={() => this.dismiss()}
                style={{backgroundColor: '#fff', marginRight: size(3)}}>Пропустить</Button>
              <Button
                mode={'outlined'} onPress={() => this.next()} flexGrow={4}
                style={{backgroundColor: '#fff', marginLeft: size(3)}}>Далее</Button>
            </>
          )}
        </View>
      </View>
    );
  }
};

export default LiftUpModalScreen;
