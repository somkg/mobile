import React from "react";
import {inject, observer} from "mobx-react";
import AuthComponent from "../components/profile/AuthComponent";
import Loading from "../ui/Loading";
import {createStackNavigator} from "@react-navigation/stack";
import MainComponent from "../components/profile/MainComponent";
import ReplenishComponent from "../components/profile/ReplenishComponent";
import {APP_COLOR} from "../utils/settings";
import {withTranslation} from "react-i18next";
import {MaterialHeaderButtons} from "../ui/HeaderButtons";
import {HiddenItem} from "react-navigation-header-buttons";
import AdViewScreen from "./AdViewScreen";
import i18n from "../utils/i18n";
import AdEditScreen from "./AdEditScreen";

const Stack = createStackNavigator();

@withTranslation('profile') @inject('store') @observer
class ProfileScreen extends React.Component {
  constructor(props) {
    super(props);

    this.store = this.props.store.appStore;
  }

  render() {
    if (this.store.authenticating) {
      return <Loading/>;
    }

    const {t} = this.props;

    return (
      <Stack.Navigator screenOptions={{
        title: t('title'),
        headerStyle: {
          backgroundColor: APP_COLOR,
        },
        headerTintColor: '#fff',
      }}>
        {this.store.authenticated ? (
          <>
            <Stack.Screen options={{
              headerRight: () => (
                <MaterialHeaderButtons>
                  {/*<HiddenItem onPress={() => {}} title={t('transactions')}/>*/}
                  <HiddenItem onPress={() => this.store.logOut()} title={t('sign_out')}/>
                </MaterialHeaderButtons>
              ),
            }} name={'Main'} component={MainComponent}/>
            <Stack.Screen options={{
              title: t('replenish')
            }} name={'Replenish'} component={ReplenishComponent}/>
            <Stack.Screen
              name={'AdView'}
              component={AdViewScreen}
              options={({route}) => ({
                title: route.params.item[`title_${i18n.language}`],
              })}/>
            <Stack.Screen
              name={'AdEdit'}
              component={AdEditScreen}
              options={({route}) => ({
                title: route.params.item[`title_${i18n.language}`],
              })}/>
          </>
        ) : (
          <Stack.Screen name={'SignIn'} component={AuthComponent}/>
        )}
      </Stack.Navigator>
    );
  }
}

export default ProfileScreen;
