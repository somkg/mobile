import React from "react";
import ScreenWrapper from "../ui/ScreenWrapper";
import {View} from 'react-native';
import {Subheading, List, Divider, Paragraph, Card} from "react-native-paper";

class HelpScreen extends React.Component {
  render() {
    return (
      <ScreenWrapper padding scroll>
        <Card>
          <Card.Title title={'Как пройти авторизацию?'} />
          <Card.Content>
            <Subheading>Нажмите кнопку Авторизация. Выберите удобный Вам вид авторизации:</Subheading>
            <List.Item
              title="По номеру мобильного телефона"
              descriptionNumberOfLines={5}
              titleNumberOfLines={3}
              description="напишите свой номер телефона - нажмите кнопку ОК – ожидайте смс сообщение с кодом – в строку код подтверждение впишите код, который поступил через смс – нажмите кнопку Вход."
            />
          </Card.Content>
        </Card>
        <Card style={{marginTop: 8}}>
          <Card.Title title={'Как добавить объявление?'} />
          <Card.Content>
            <Subheading style={{color: 'grey'}}>Чтобы добавить объявление необходимо пройти авторизацию. Смотрите раздел Как пройти авторизацию?</Subheading>
            <Paragraph style={{marginVertical: 8}}>Если Вы прошли авторизацию нажмите кнопку Добавить объявление и далее выберите и заполните соответствующие пункты.</Paragraph>
            <Divider/>
            <Paragraph style={{marginVertical: 8}}>Выберите подходящую Категорию. Изучите список категорий и выберите самую подходящую для вашего товара или услуги.</Paragraph>
            <Divider/>
            <Paragraph style={{marginVertical: 8}}>Если вы сомневаетесь, начните вводить название товара или услуги в поиске вы увидите подсказку с категорией. Посмотрите, в каких категориях другие пользователи публикуют похожие объявления.</Paragraph>
            <Divider/>
            <Paragraph style={{marginVertical: 8}}>Категорию нельзя самостоятельно изменить после публикации объявления.</Paragraph>
            <Divider/>
            <Paragraph style={{marginVertical: 8}}>Придумайте Заголовок объявления - оно должно передавать суть товара или услуги, которую вы предлагаете.Пример:«Продается 2 комнатная квартира», «Ремонт стиральных машин».</Paragraph>
            <Divider/>
            <Paragraph style={{marginVertical: 8}}>Важно: Не указывайте в Заголовке цену и контактную информацию для этого есть отдельные поля.</Paragraph>
            <Divider/>
            <Paragraph style={{marginVertical: 8}}>В поле Описание подробно расскажите о своем предложении, пропишите дополнительные условия сделки, расскажите о скидках и акциях. Например: «Продается 2 комнатная квартира с евроремонтом. Расположена в центре города Бишкек. 3 этаж. Имеется лифт, центральное отопление. Рядом школа и детский сад».</Paragraph>
            <Divider/>
            <Paragraph style={{marginVertical: 8}}>В поле «Цена» укажите именно ту цену, по которой вы готовы продать товар без учета скидок и акций.</Paragraph>
            <Divider/>
            <Paragraph style={{marginVertical: 8}}>Добавьте в объявление фотографии. Выберите, откуда вы хотите загрузить фото — из галереи изображений на устройстве, с google-диска и т.д. Выберите нужное изображение и кликните на него оно добавится в объявление. Вы можете выбрать сразу несколько фото.</Paragraph>
            <Divider/>
            <Paragraph style={{marginTop: 8}}>Требования к фото:</Paragraph>
            <View style={{marginLeft: 8, marginBottom: 4}}>
              <Paragraph style={{marginVertical: 4, fontSize: 12, color: 'grey'}}>В объявления можно добавить изображения форматов jpg, jpeg, png и gif.</Paragraph>
              <Paragraph style={{marginVertical: 4, fontSize: 12, color: 'grey'}}>Размер фото должен быть не более 5 МБ.</Paragraph>
              <Paragraph style={{marginVertical: 4, fontSize: 12, color: 'grey'}}>Добавляйте в объявления фотографии, которые вы сделали сами. Не загружайте фотографии из чужих объявлений и других сайтов, даже если у вас аналогичный или очень похожий товар.</Paragraph>
              <Paragraph style={{marginVertical: 4, fontSize: 12, color: 'grey'}}>Важно: не добавляйте фото эротического, вульгарного или оскорбительного характера; изображения, которые провоцируют жестокость, ненависть, или выражают неуважительное отношение.</Paragraph>
            </View>
            <Divider/>
            <Paragraph style={{marginVertical: 8}}>Убедитесь, что все заполнено верно, и нажмите Отправить.</Paragraph>
            <Divider/>
            <Paragraph style={{marginVertical: 8}}>Бесплатное объявление сразу после этого уйдет на проверку. Если объявление платное, вы сможете оплатить его размещение на этом этапе или позже. Для этого выберите нужный способ оплаты и следуйте инструкциям.</Paragraph>
          </Card.Content>
        </Card>
      </ScreenWrapper>
    );
  }
}

export default HelpScreen;
