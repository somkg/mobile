import React from "react";
import {inject, observer} from "mobx-react";
import AuthComponent from "../components/profile/AuthComponent";
import Loading from "../ui/Loading";
import ScreenWrapper from "../ui/ScreenWrapper";
import {requester} from "../utils";
import {Text} from "react-native-paper";
import {FlatList, View} from "react-native";
import AdItem from "../ui/AdItem";
import Ad from "../models/Ad";
import {withTranslation} from "react-i18next";

@withTranslation() @inject('store') @observer
class FavoritesScreen extends React.Component {
  constructor(props) {
    super(props);

    this.store = this.props.store.appStore;
    this.state = {
      items: [],
      busy: false,
    };
  }

  fetch = async () => {
    if (this.state.busy) return false;
    try {
      this.setState({busy: true});
      const response = await requester.get('/user/favorites');
      this.setState({items: response.data.map(v => new Ad(v.product_object))});
    } catch (e) {
      console.log(e);
    } finally {
      this.setState({busy: false});
    }
  };

  componentDidMount() {
    if (this.store.authenticated) this.fetch().then(() => {
    });
  }

  render() {
    if (this.store.authenticating) {
      return <Loading/>;
    }

    if (!this.store.authenticated) {
      return <AuthComponent helpText={true}/>;
    }

    const items = this.store.favorites;
    const {t} = this.props;

    return (
      <ScreenWrapper>
        {items.length === 0 ? (
          <Text style={{margin: 8}}>{t('list_is_empty')}</Text>
        ) : (
          <FlatList
            refreshing={this.state.busy}
            data={this.state.items}
            renderItem={({item}) => (
              <View style={{flex: 1}}>
                <AdItem data={item}/>
              </View>
            )}
            numColumns={2}
            keyExtractor={(item, index) => index.toString()}
            onRefresh={() => this.fetch()}/>
        )}
      </ScreenWrapper>
    );
  }
}

export default FavoritesScreen;
