import React from 'react';
import {View, TouchableOpacity, ScrollView, Image, Alert} from 'react-native';
import ScreenWrapper from '../ui/ScreenWrapper';
import {inject, observer, Observer} from 'mobx-react';
import {Caption, Card, Subheading, Button, TextInput} from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {action, computed, observable, toJS} from 'mobx';
import Ad from '../models/Ad';
import {
  buildArrayForBlob,
  checkBoolean,
  codeTranslator,
  requester,
} from '../utils';
import PhoneNumber from '../utils/PhoneNumber';
import {withTranslation} from 'react-i18next';
import ImagePicker from '../ui/ImagePicker';
import RNFetchBlob from 'rn-fetch-blob';
import {API_URL} from '../utils/settings';
import Questions from '../components/AdEditQuestions';

@withTranslation('add_ad')
@inject('store')
@observer
class AdEditScreen extends React.Component {
  image_picker = React.createRef();
  @observable item = null;
  @observable phones = [];
  @observable images = [];
  @observable busy = false;

  constructor(props) {
    super(props);

    this.store = this.props.store.appStore;
    this.adStore = this.props.store.adStore;

    this.item = new Ad(toJS(this.props.route.params.item));

    this.phones = this.item.phones.map(v => {
      v = v ? v.split('|') : ['', false];
      return {
        phone: v[0],
        p_phone: new PhoneNumber().parseFromString(v[0]),
        whatsapp: checkBoolean(v[1]),
      };
    });

    this.components = (this.item.categories_path.length > 0 ? this.item.categories_path[0].forms : ['place']).map((f, i) => {
      const Component = Questions[`${codeTranslator(f)}Question`];
      if (!Component) return null;
      return (
        <Observer>
          {() => (
            <Component
              defaultValue={this.item.additionals[f]}
              onValidValue={value => this.item.setAdditional(f, value)}
              key={i}
            />
          )}
        </Observer>
      );
    });

    this.images = this.item.original_images.map(v => ({
      type: 'server',
      data: v,
    }));
  }

  @action setValue = (n, v) => (this[n] = v);

  @action setItem = (n, v) => (this.item[n] = v);

  @action onChangePhone = (n, e) => {
    this.phones[n - 1].phone = e;
    this.phones[n - 1].p_phone = this.phones[n - 1].p_phone.parseFromString(e);
  };

  @action onChangeWhatsapp = n => {
    n = n.split('_')[1];
    this.phones[n - 1].whatsapp = !this.phones[n - 1].whatsapp;
  };

  // @action openImagePicker = () => {
  //   if (this.images.length >= 10) return;
  //
  //   const {t} = this.props;
  //
  //   const options = {
  //     title: t('add_ad:label.select_image'),
  //     cancelButtonTitle: t('add_ad:cancel'),
  //     takePhotoButtonTitle: t('add_ad:image_picker.take_photo'),
  //     chooseFromLibraryButtonTitle: t(
  //       'add_ad:image_picker.choose_from_library',
  //     ),
  //     mediaType: 'photo',
  //   };
  //
  //   ImagePicker.showImagePicker(options, response => {
  //     if (response.didCancel) {
  //       console.log('User cancelled image picker');
  //     } else if (response.error) {
  //       console.log('ImagePicker Error: ', response.error);
  //     } else {
  //       this.images.push({type: 'image_picker', data: response});
  //     }
  //   });
  // };

  @action removePhoto = (v, k) => {
    const {t} = this.props;
    if (v.type === 'image_picker') {
      let photos = [...this.images];
      photos.splice(k, 1);
      this.images = photos;
    } else if (v.type === 'server') {
      Alert.alert(t('add_ad:confirm'), t('add_ad:confirm_delete_photo'), [{
        text: t('add_ad:cancel'),
        style: 'cancel',
      },
        {
          text: t('add_ad:delete'),
          onPress: () => {
            requester
              .post('/ad/delete_image', {
                id: this.item.id,
                url: v.data,
              })
              .then(res => {
                if (res.status === 'success') {
                  this.item.deleteImage(v.data);
                  this.props.route.params.item.deleteImage(v.data);
                  this.images = this.images.filter(g => v === g);
                }
              })
              .catch(e => {
                console.log(e);
              });
          },
        },
      ]);
    }
  };

  @action reject = () => {
    const {t} = this.props;
    Alert.alert(t('add_ad:confirm'), t('add_ad:are_you_sure'), [
      {
        text: t('add_ad:cancel'),
        style: 'cancel',
      },
      {
        text: t('Reject'),
        onPress: () => {
          this.busy = true;
          this.adStore
            .rejectById(this.item.id)
            .then(() => {
              this.store.showSuccess('Успешно отклонено!');
              this.item.reject();
              this.props.route.params.item.reject();
            })
            .catch(e => {
              console.log(e);
            })
            .finally(() => {
              this.busy = false;
            });
        },
      },
    ]);
  };

  @action save = async send_to_admin => {
    if (this.busy) return;
    this.busy = true;
    try {
      const params = [
        {name: 'id', data: JSON.stringify(this.item.id),},
        {name: 'title_ru', data: this.item.title_ru,},
        {name: 'title_ky', data: this.item.title_ky,},
        {name: 'description_ru', data: this.item.description_ru,},
        {name: 'description_ky', data: this.item.description_ky,},
        {name: 'send_to_admin', data: JSON.stringify(send_to_admin),},
      ];

      this.images
        .filter(v => v.type === 'image_picker')
        .map((p, i) => {
          params.push({
            name: `images[${i}]`,
            filename: `image_${i}.jpg`,
            data: RNFetchBlob.wrap(p.data.path),
          });
        });

      buildArrayForBlob(this.item.additionals, [], 'additionals').map(v => {
        params.push(v);
      });

      let result = await RNFetchBlob.fetch(
        'POST',
        `${API_URL()}/ad/update`, {
          Authorization: `Basic ${this.store.token}`,
          'Content-Type': 'multipart/form-data',
          Accept: 'application/json',
        },
        params,
      );

      result = JSON.parse(result.data);
      if (result.status === 'success') {
        this.store.showSuccess('Успешно сохранено!');
        this.props.navigation.setOptions({
          headerTitle: this.item[`title_${this.store.language}`],
        });
        this.item._update(result.data.item);
        this.props.route.params.item._update(result.data.item);
        this.props.navigation.goBack();
      } else {
        console.log(result);
        this.store.showError('Error!');
      }
    } catch (e) {
      console.log(e);
    } finally {
      this.busy = false;
    }
  };

  @computed get can_be_saved() {
    return (
      this.item.title_ru.length > 0 &&
      this.item.title_ru.length <= 70 &&
      this.item.title_ky.length > 0 &&
      this.item.title_ky.length <= 70
    );
  }

  render() {
    const {t} = this.props;

    return (
      <>
        <ScreenWrapper scroll>
          <Card
            elevation={2}
            style={{margin: 8}}
          >
            <Card.Content>
              <TextInput
                mode={'outlined'}
                blurOnSubmit={true}
                dense={true}
                label={'Название (RU)'}
                autoFocus={true}
                onChangeText={text => this.setItem('title_ru', text.substr(0, 70))}
                error={this.item['title_ru'].length >= 70}
                value={this.item['title_ru']}
                returnKeyType={'next'}
              />
              <TextInput
                mode={'outlined'}
                blurOnSubmit={true}
                dense={true}
                label={'Название (KY)'}
                onChangeText={text => this.setItem('title_ky', text.substr(0, 70))}
                error={this.item['title_ky'].length >= 70}
                value={this.item['title_ky']}
                returnKeyType={'next'}
              />
              <TextInput
                label={'Описание (RU)'}
                returnKeyType={'next'}
                scrollEnabled={true}
                multiline={true}
                numberOfLines={5}
                mode={'outlined'}
                blurOnSubmit={true}
                value={this.item['description_ru']}
                dense={true}
                error={this.item['description_ru'].length >= 1300}
                onChangeText={text => this.setItem('description_ru', text.substr(0, 1300))}
              />
              <TextInput
                label={'Описание (KY)'}
                returnKeyType={'done'}
                scrollEnabled={true}
                multiline={true}
                numberOfLines={5}
                mode={'outlined'}
                blurOnSubmit={true}
                value={this.item['description_ky']}
                dense={true}
                error={this.item['description_ky'].length >= 1300}
                onChangeText={text => this.setItem('description_ky', text.substr(0, 1300))}
              />
            </Card.Content>
          </Card>

          {this.components}

          <Card
            elevation={2}
            style={{marginHorizontal: 8, marginBottom: 8}}
          >
            <Card.Content>
              <Subheading>{t('Photos')}({this.images.length})</Subheading>
              {this.images.length > 0 && (
                <Caption>{t('Click_on_the_photo_to_delete')}</Caption>
              )}
              <ScrollView
                horizontal={true}
                style={{marginTop: 4, paddingBottom: 6}}
              >
                {this.images.map((v, key) => {
                  if (v.type === 'image_picker')
                    return (
                      <TouchableOpacity
                        style={{width: 100, height: 100, marginRight: 4}}
                        onPress={
                          () => this.removePhoto(v, key)}
                        key={key}
                      >
                        <Image
                          style={{width: '100%', height: '100%'}}
                          source={{uri: v.data.path}}
                        />
                      </TouchableOpacity>
                    );
                  else if (v.type === 'server')
                    return (

                      <TouchableOpacity
                        style={{width: 100, height: 100, marginRight: 4}}
                        onPress={
                          () => this.removePhoto(v, key)}
                        key={key}>
                        <Image
                          style={{width: '100%', height: '100%'}}
                          source={{uri: v.data}}
                        />
                      </TouchableOpacity>
                    );
                })}
                <ImagePicker onSelectImage={(v) => {
                  this.images.push({type: 'image_picker', data: v})
                }} onSelectImages={(i) => {
                  i.map(v => {
                    if (this.images.length < 10) this.images.push({type: 'image_picker', data: v})
                  });
                }} getContext={r => this.image_picker = r} />

                {this.images.length < 10 && (
                  <TouchableOpacity
                    onPress={() => this.image_picker.open()}
                    style={{
                      width: 100,
                      marginRight: 4,
                      borderWidth: 1,
                      borderColor: '#eee',
                      height: 100,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    <Icon size={35} name={'plus'}/>
                  </TouchableOpacity
                      >
                )}
              </ScrollView>
            </Card.Content>
          </Card>
          <View style={{marginBottom: 8, marginHorizontal: 8}}>
            {!['published', 'sold'].includes(this.item.status) && (
              <Button
                loading={this.busy}
                disabled={this.busy || !this.can_be_saved}
                mode={'contained'}
                onPress={() => this.save(false)}>
                {t('Save')}
              </Button>
            )}
            {this.item.status !== 'pending' && (
              <Button
                style={{marginTop: 8}}
                disabled={this.busy || !this.can_be_saved}
                mode={'outlined'}
                onPress={() => this.save(true)}>
                {t('moderator')}
              </Button>
            )}
            {this.item.status !== 'rejected' && (
              <Button
                style={{marginTop: 8}}
                mode={'outlined'}
                disabled={this.busy}
                onPress={() => this.reject()}>
                {t('Reject')}
              </Button>
            )}
          </View>
        </ScreenWrapper>
      </>
    );
  }
}

export default AdEditScreen;
