import React from 'react';
import ScreenWrapper from '../ui/ScreenWrapper';
import {inject, observer} from 'mobx-react';
import {action, observable} from 'mobx';
import {FlatList, View, Keyboard, TouchableOpacity} from 'react-native';
import {APP_COLOR} from '../utils/settings';
import {
  ActivityIndicator,
  Searchbar,
  Card,
  Text,
  Button,
  Portal,
  Modal,
  Divider,
  List,
  Dialog,
  RadioButton,
} from 'react-native-paper';
import AdItem from '../ui/AdItem';
import {requester} from '../utils';
import Ad from '../models/Ad';
import {withTranslation} from 'react-i18next';
import Icon from 'react-native-vector-icons/MaterialIcons';
import AdFilterByPlaceModal from '../components/AdFilterByPlaceModal';
import storage from '../utils/storage';

@withTranslation()
@inject('store')
@observer
class SearchBar extends React.Component {
  constructor(props) {
    super(props);

    this.fetchCategories().then(() => {
    });
  }

  fetchCategories = async () => {
    this.categories = (await storage.get('categories', [])).filter(
      v => !v.parent_id,
    );
  };

  @observable query = '';
  @observable place = null;
  @observable category = null;
  @observable visible = false;
  @observable filter_by_place_modal = false;
  @observable category_modal = false;
  searchbar_ref = React.createRef();

  @action setValue = (name, value) => (this[name] = value);

  componentDidMount(): void {
    this.searchbar_ref.focus();
  }

  @action toggleFilterByPlaceModal(s) {
    this.filter_by_place_modal = s;
  }

  @action toggleModal = () => {
    this.visible = !this.visible;
    if (this.visible) {
      Keyboard.dismiss();
    }
  };

  applyFilters = () => {
    this.toggleModal();
    this.props.search(
      this.query,
      this.place ? this.place.id : 0,
      this.category ? this.category.id : 0,
    );
  };

  @action onSelectCategoryFilter = c => {
    this.category = c;
    this.category_modal = false;
  };

  @action clearFilters = () => {
    this.toggleModal();
    this.place = null;
    this.category = null;
    this.props.search(
      this.query,
      this.place ? this.place.id : 0,
      this.category ? this.category.id : 0,
    );
  };

  @action setValue = (name, value) => (this[name] = value);

  search = () => {
    this.props.search(
      this.query,
      this.place ? this.place.id : 0,
      this.category ? this.category.id : 0,
    )
  };

  render() {
    const {options, navigation, t} = this.props;

    return (
      <>
        <View
          flexDirection={'row'}
          alignItems={'center'}
          style={{padding: 8, paddingRight: 0, backgroundColor: APP_COLOR}}
        >
          <View flexGrow={1}>
            <Searchbar
              ref={r => (this.searchbar_ref = r)}
              style={
                {
                  ...options.headerStyle,
                  backgroundColor: '#fff',
                }
              }
              onIconPress={() => navigation.goBack()}
              icon={'arrow-left'}
              onSubmitEditing={() => this.search()}
              placeholder={t('search_placeholder')}
              onChangeText={t => this.setValue('query', t)}
              value={this.query}/>
          </View>
          <Button
            size={30}
            color={'#FBFCFC'}
            onPress={() => this.toggleModal()}>Фильтр</Button>
          <Button
            size={20}
            color={'#FBFCFC'}
            onPress={() => this.search()}>
            <Icon size={25} i name={'search'} />
          </Button>
        </View>
        <Portal onPress={() => this.toggleAdFilterByPlaceModal(true)}>
          <Modal
            visible={this.visible}
            onDismiss={() => this.toggleModal()}>
            <Card style={
              {margin: 28}}>
              <Card.Content>
                <List.Item
                  title={'Регион'}
                  description={
                    this.place ?
                      this.place[`title_${t('language')}`] :
                      'Кыргызстан'
                  }
                  onPress={() => this.toggleFilterByPlaceModal(true)}
                />
                <Divider/>
                <List.Item
                  title="Категория"
                  description={
                    this.category ?
                      this.category[`title_${t('language')}`] :
                      t('in_all_category')
                  }
                  onPress={() => this.setValue('category_modal', true)}
                />
                <Button
                  mode={'contained'}
                  onPress={() => this.applyFilters()}
                  style={{marginTop: 8}}>
                  {t('Apply_filter')}
                </Button>
                <Button
                  mode={'outlined'}
                  onPress={() => this.clearFilters()}
                  style={{marginTop: 8}}>
                  {t('Reset_filter')}
                </Button>
              </Card.Content>
            </Card>
          </Modal>

          <Dialog
            dismissable={true}
            onDismiss={() => this.setValue('category_modal', false)}
            visible={this.category_modal}>
            <Dialog.Content>
              <RadioButton.Group value={`id_${this.category ? this.category.id : 0}`}>
                {
                  (this.categories ? [
                      {
                        title_ru: 'Во всех категориях',
                        title_ky: 'Баардык категориялардан',
                        id: 0,
                      },
                      ...this.categories,
                    ] : []
                  ).map((v, k) => (
                    <TouchableOpacity
                      key={k}
                      style={{alignItems: 'center', flexDirection: 'row'}}
                      onPress={() => this.onSelectCategoryFilter(v)}>
                      <RadioButton
                        value={`id_${v.id}`}
                        onPress={() => this.onSelectCategoryFilter(v)}
                      />
                      <Text>{v[`title_${t('language')}`]}</Text>
                    </TouchableOpacity>
                  ))
                }
              </RadioButton.Group>
            </Dialog.Content>
          </Dialog>
        </Portal>

        <AdFilterByPlaceModal
          make_store={false}
          value={this.place ? this.place.id : 0}
          dismiss={() => this.toggleFilterByPlaceModal(false)}
          onChange={(on, p) => {
            this.setValue('place', p);
          }}
          show={this.filter_by_place_modal}
        />
      </>
    );
  }
}

@withTranslation()
@observer
class SearchScreen extends React.Component {
  @observable result = [];
  @observable loading = false;
  @observable fetched = false;

  @action setValue = (name, value) => (this[name] = value);

  query = '';

  @action search = async (q, p, c) => {
    if (this.loading || q.length < 3) {
      return false;
    }
    this.query = q;
    this.place = p;
    this.category = c;
    this.loading = true;
    this.result = [];
    try {
      const data = await requester.get('/ad/search', {
        query: this.query,
        filters: {
          place: this.place,
          category: this.category,
        },
      });
      this.setValue('result', data.data.items.map(v => new Ad(v)));
      this.setValue('fetched', true);
    } catch (e) {
      console.log(e);
    } finally {
      this.loading = false;
    }
  };

  componentDidMount(): void {
    this.props.navigation.setOptions({
      header: ({scene, navigation}) => {
        const {options} = scene.descriptor;
        return (
          <SearchBar
            options={options}
            search={(q, place, category) => this.search(q, place, category)}
            navigation={navigation}
          />
        );
      },
    });
  }

  render() {
    const {t} = this.props;
    return (
      <ScreenWrapper>
        <FlatList
          refreshing={this.loading}
          progressViewOffset={20}
          onRefresh={() => this.search(this.query, this.place, this.category)}
          data={this.result}
          renderItem={
            ({item}) => (
              <View style={
                {flex: 1}}>
                <AdItem data={item}/>
              </View>
            )
          }
          ListEmptyComponent={
            this.fetched && !this.loading ? (
              <Card
                elevation={2}
                style={{margin: 8}}
              >
                <Card.Content style={{flex: 1, alignItems: 'center'}}>
                  <Icon
                    size={40}
                    name={'new-releases'}
                    color={'#888'}
                    style={{marginVertical: 16}}
                  />
                  <Text style={{fontSize: 16, color: '#888', textAlign: 'center'}}>
                    {t('No_ads_found')}
                  </Text>
                </Card.Content>
              </Card>
            ) : null}
          //onEndReached={() => this.fetchAds(this.current_page + 1)}
          numColumns={2}
          keyExtractor={(item, index) => index.toString()}
          ListFooterComponent={this.loading ? <ActivityIndicator/> : null}
          ListFooterComponentStyle={{margin: 16}}
        />
      </ScreenWrapper>
    );
  }
}

export default SearchScreen;
