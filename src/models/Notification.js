import {computed, observable} from "mobx";
import BaseModel from "./BaseModel";
import i18n from '../utils/i18n';

class Notification extends BaseModel {
  @observable id = 0;
  @observable read_on = null;
  @observable data = '';
  @observable payload = null;
  @observable created_at = null;

  @computed get is_new() {
    return this.read_on === null;
  }

  get content(){
    let result = 'Unknown';

    switch (this.data) {
      case 'ads.rejected':
        result = i18n.t('notification_labels.rejected');
        break;

      case 'ads.published':
        result = i18n.t('notification_labels.published');
        break;

      case 'users.is_not_agent':
        result = i18n.t('notification_labels.is_not_agent');
        break;

      case 'users.is_agent':
        result = i18n.t('notification_labels.is_agent');
        break;

      case 'balance.replenish':
        result = i18n.t('notification_labels.replenish', {balance: this.payload.amount});
        break;
    }

    return result;
  }
}

export default Notification;
