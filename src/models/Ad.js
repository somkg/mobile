import {action, computed, observable} from "mobx";
import BaseModel from "./BaseModel";

class Ad extends BaseModel {
  @observable id = 0;
  @observable title_ru = '';
  @observable title_ky = '';
  @observable additionals = {};
  @observable main_image = '';
  @observable categories_path = [];
  @observable description_ru = '';
  @observable description_ky = '';
  @observable images = [];
  @observable raised_on = null;
  @observable published_on = null;
  @observable phones = [];
  @observable author = null;
  @observable status = '';
  @observable active_service = null;

  @action setRaisedOn(r) {
    this.raised_on = r;
  }

  @action setAdditional(n, v) {
    if (n in this.additionals) this.additionals[n] = v;
  }

  @action reject() {
    this.raised_on = null;
    this.published_on = null;
    this.status = 'rejected';
  }

  @action deleteImage(url) {
    this.images = this.images.filter(v => {
      if (v.indexOf('/thumbnail_') > -1) {
        const s = v.split('/thumbnail_');
        return `${s[0]}/${s[1]}` !== url;
      } else return v !== url;
    });
  }

  @action setImages(s) {
    this.images = s;
  }

  @computed get original_images() {
    return this.images.filter(v => v.split('/')[8].startsWith('tabekg_'));
  }

  @computed get optimized_images() {
    return this.images.filter(v => v.split('/')[8].startsWith('thumbnail_'));
  }
}

export default Ad;
