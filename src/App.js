import React from 'react';
import {StatusBar, View, TouchableOpacity, ScrollView, Linking} from "react-native";
import {Caption, Card, Modal, Portal, Subheading, Text, Button} from "react-native-paper";
import {APP_COLOR, APP_VERSION, initialize} from "./utils/settings";
import HomeScreen from "./screens/HomeScreen";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import {inject, observer} from "mobx-react";
import storage from "./utils/storage";
import WelcomeModal from "./components/WelcomeModal";
import {withTranslation} from "react-i18next";
import PlaceModal from "./components/PlaceModal";
import Icon from "react-native-vector-icons/MaterialIcons";
import ProfileScreen from "./screens/ProfileScreen";
import NotificationsScreen from "./screens/NotificationsScreen";
import AddAdScreen from "./screens/AddAdScreen";
import AdViewScreen from "./screens/AdViewScreen";
import CategoriesScreen from "./screens/CategoriesScreen";
import i18n from "./utils/i18n";
import {createStackNavigator} from "@react-navigation/stack";
import UserViewScreen from "./screens/UserViewScreen";
import AdEditScreen from "./screens/AdEditScreen";
import 'moment/locale/ru';
import 'moment/locale/ky';
import {observable, observe, action} from "mobx";
import moment from "moment";
import messaging from '@react-native-firebase/messaging';
import {requester} from "./utils";
import admob, {MaxAdContentRating} from '@react-native-firebase/admob';
import LiftUpModalScreen from "./screens/modals/LiftUpModalScreen";

const Tab = createBottomTabNavigator();
const CategoriesStack = createStackNavigator();

function TabBar(props) {
  const {state, descriptors, navigation} = props;

  return (
    <View style={{flexDirection: 'row', backgroundColor: '#fff', justifyContent: 'space-between', alignItems: 'center'}}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            key={index}
            accessibilityRole="button"
            accessibilityStates={isFocused ? ['selected'] : []}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{flex: 1, padding: 3, justifyContent: 'center', alignItems: 'center'}}
          >
            {options.tabBarIcon({size: 25, focused: isFocused, color: !isFocused ? APP_COLOR : 'grey'})}
            <Text style={{fontSize: 10, color: !isFocused ? APP_COLOR : 'grey'}}>{label}</Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

const RootStack = createStackNavigator();

@withTranslation() @inject('store') @observer
class App extends React.Component {
  @observable last_version = null;

  constructor(props) {
    super(props);

    this.store = this.props.store;
    this.appStore = this.store.appStore;
    this.placeStore = this.store.placeStore;
    this.adStore = this.store.adStore;

    initialize(this.appStore);

    this.appStore.useI18n(this.props.i18n);
    this.useStorage();
    this.checkAuth();
    this.placeStore.fetchAllItems();

    observe(
      this.appStore,
      "language",
      ({newValue}) => {
        moment.locale(newValue);
      },
      true
    );

    admob()
      .setRequestConfiguration({
        maxAdContentRating: MaxAdContentRating.G,
        tagForChildDirectedTreatment: true,
        tagForUnderAgeOfConsent: true,
      })
      .then(() => {
        console.log('success');
      });
  }

  useStorage = () => {
    this.appStore.useStorage(storage);
    this.placeStore.useStorage(storage);
    this.adStore.useStorage(storage);
  };

  checkAuth = () => {
    storage.get('token').then(token => {
      this.appStore.setToken(token, true);
      if (token) this.appStore.checkAuth();
    });
  };

  componentDidMount(): void {
    this.unsubscribe = messaging().onMessage(async remoteMessage => {
      console.log(remoteMessage);
    });

    messaging().getToken().then(fcm_token => this.setFcmToken(fcm_token));
    messaging().onTokenRefresh(fcm_token => this.setFcmToken(fcm_token));

    this.checkLastVersion();
  }

  setFcmToken = (token) => {
    this.appStore.setFcmToken(token);
  };

  componentWillUnmount() {
    this.unsubscribe();
  }

  @action checkLastVersion = () => {
    requester.get('/user/last_app', {platform: 'android'}).then(({data}) => {
      this.last_version = data;
    }).catch((e) => {
      console.log(e);
    });
  };

  updateApp = () => {
    Linking.openURL(this.last_version.link);
  };

  render() {
    const {t} = this.props;
    const new_notifications = this.store.appStore.new_notifications;

    return (
      <>
        <StatusBar animated={true} barStyle="light-content" backgroundColor={APP_COLOR}/>

        <WelcomeModal/>
        <PlaceModal/>

        <Portal>
          <Modal dismissable={false} contentContainerStyle={{padding: 8}}
                 visible={this.last_version && this.last_version.version_code > APP_VERSION}>
            <ScrollView>
              <Card>
                <Card.Content>
                  <Subheading style={{textAlign: 'center'}}>{t('new_version.title')}</Subheading>
                  <Caption style={{fontSize: 14}}>{t('new_version.description')}</Caption>
                  <Button onPress={() => this.updateApp()} style={{marginTop: 8}}
                          mode={'contained'}>{t('new_version.update')}</Button>
                </Card.Content>
              </Card>
            </ScrollView>
          </Modal>
        </Portal>

        <RootStack.Navigator mode="modal">

          <RootStack.Screen name="Main" options={{ headerShown: false }}>
            {() => (
              <Tab.Navigator tabBar={props => <TabBar {...props} />} tabBarOptions={{
                style: {height: 60},
              }} backBehavior={'none'} initialRouteName={'Home'}>

                <Tab.Screen options={{
                  title: t('home'),
                  tabBarIcon: ({size, color}) => <Icon color={color} name={'home'} size={size}/>,
                }} name={'Home'} component={HomeScreen}/>

                <Tab.Screen options={{
                  title: t('categories'),
                  tabBarIcon: ({size, color}) => <Icon color={color} name={'view-list'} size={size}/>
                }} name={'Categories'}>
                  {() => (
                    <CategoriesStack.Navigator screenOptions={{headerShown: false}} initialRouteName={'Categories'}>
                      <CategoriesStack.Screen name={'Categories'} component={CategoriesScreen}/>
                      <CategoriesStack.Screen
                        name={'AdView'}
                        component={AdViewScreen}
                        options={({route}) => ({
                          headerShown: true,
                          headerStyle: {
                            backgroundColor: APP_COLOR,
                          },
                          headerTintColor: '#fff',
                          title: route.params.item[`title_${i18n.language}`],
                        })}/>
                      <CategoriesStack.Screen
                        name={'AdEdit'}
                        component={AdEditScreen}
                        options={({route}) => ({
                          headerShown: true,
                          headerStyle: {
                            backgroundColor: APP_COLOR,
                          },
                          headerTintColor: '#fff',
                          title: route.params.item[`title_${i18n.language}`],
                        })}/>
                      <CategoriesStack.Screen
                        name={'UserView'}
                        component={UserViewScreen}
                        options={{
                          headerShown: true,
                          headerStyle: {
                            backgroundColor: APP_COLOR,
                          },
                          headerTintColor: '#fff',
                        }}/>
                    </CategoriesStack.Navigator>
                  )}
                </Tab.Screen>

                <Tab.Screen options={{
                  title: t('add_ad'),
                  tabBarIcon: ({size, color}) => <Icon color={color} name={'add-circle'} size={size}/>
                }} name={'AddAd'} component={AddAdScreen}/>

                <Tab.Screen options={{
                  title: t('notifications'),
                  tabBarIcon: ({size, color}) => (
                    <View style={{position: 'relative'}}>
                      {new_notifications.length > 0 ? <View style={{
                        position: 'absolute',
                        right: 1,
                        top: 3,
                        borderColor: '#fff',
                        borderWidth: 1,
                        zIndex: 10,
                        borderRadius: 10,
                        backgroundColor: APP_COLOR,
                        color: '#fff',
                        width: 10,
                        height: 10
                      }}/> : null}
                      <Icon color={color} name={'notifications'} size={size}/>
                    </View>
                  )
                }} name={'Notifications'} component={NotificationsScreen}/>

                <Tab.Screen options={{
                  title: t('profile'),
                  tabBarIcon: ({size, color}) => <Icon color={color} name={'person'} size={size}/>
                }} name={'Profile'} component={ProfileScreen}/>

              </Tab.Navigator>
            )}
          </RootStack.Screen>

          <RootStack.Screen name="LiftUpModal" options={{ headerShown: false }} component={LiftUpModalScreen}/>

        </RootStack.Navigator>
      </>
    );
  }
}

export default App;
