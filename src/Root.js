import 'react-native-gesture-handler';
import React, {Suspense} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import App from "./App";
import {Provider} from 'mobx-react';
import stores from './stores';
import {DefaultTheme, Provider as PaperProvider} from 'react-native-paper';
import {navigationRef} from "./utils/RootNavigation";
import i18n from './utils/i18n';
import {I18nextProvider} from "react-i18next";
import {APP_COLOR} from "./utils/settings";
import AppMetrica from 'react-native-appmetrica';
import Loading from "./ui/Loading";

//NativeModules.DevSettings.setIsDebuggingRemotely(true);

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: APP_COLOR,
    accent: '#17a2b8',
  },
};

console.reportErrorsAsExceptions = false;

class Root extends React.Component {
  constructor(props) {
    super(props);

    AppMetrica.activate({
      apiKey: 'e9580d22-acd7-4a6b-a3ce-6e0233c99f7b',
      sessionTimeout: 120,
      firstActivationAsUpdate: true,
    });
  }

  render() {
    return (
      <Provider store={stores}>
        <PaperProvider theme={theme}>
          <Suspense fallback={<Loading/>}>
            <I18nextProvider i18n={i18n}>
              <NavigationContainer ref={navigationRef}>
                <App/>
              </NavigationContainer>
            </I18nextProvider>
          </Suspense>
        </PaperProvider>
      </Provider>
    );
  }
}

export default Root;
