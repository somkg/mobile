import React from 'react';
import Icon from "react-native-vector-icons/FontAwesome5";
import {Text} from "react-native-paper";

class CategoriesRenderer extends React.Component {
  render() {
    const delimiter = <Icon size={15} color={'#808080'} style={{marginHorizontal: 8}} name={'caret-right'} />;

    let list = [];

    const {items, language} = this.props;
    const itemRenderer = (item, i) => <Text key={i} style={{fontSize: 17, color: '#808080'}}>{item[`title_${language}`]}</Text>;

    for (let i = items.length - 1; i >= 0; i--) {
      if (list.length > 0) list.push(delimiter);
      list.push(itemRenderer(items[i], i));
    }

    return list;
  }
}

export default CategoriesRenderer;
