import React from 'react';
import {View} from "react-native";
import {ActivityIndicator} from "react-native-paper";

export default class Loading extends React.Component {
  render(){
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <ActivityIndicator/>
      </View>
    );
  }
}
