import React from 'react';
import {Portal, Dialog, RadioButton, Text} from "react-native-paper";
import {TouchableOpacity, ScrollView} from "react-native";
import {observer} from "mobx-react";
import {action, observable} from "mobx";

@observer
class SelectModal extends React.Component {
  @observable visible = false;

  @action setValue = (name, value) => this[name] = value;

  @action show = () => {
    this.visible = true;
  };

  @action hide = () => {
    this.visible = false;
  };

  @action onSelect = (v) => {
    this.visible = false;
    this.props.onSelect(v.value);
  };

  render() {
    return (
      <Portal>
        <Dialog
          dismissable={true}
          onDismiss={() => this.setValue('visible', false)}
          visible={this.visible}>
          <Dialog.Title>{this.props.title}</Dialog.Title>
          <Dialog.ScrollArea>
            <ScrollView style={{marginVertical: 18}}>
              <RadioButton.Group flex={1} value={this.props.value}>
                {this.props.items.map((v, k) => (
                  <TouchableOpacity
                    key={k}
                    style={{alignItems: 'center', flexDirection: 'row'}}
                    onPress={() => this.onSelect(v)}>
                    <RadioButton value={v.value} onPress={() => this.onSelect(v)}/>
                    <Text>{v.title}</Text>
                  </TouchableOpacity>
                ))}
              </RadioButton.Group>
            </ScrollView>
          </Dialog.ScrollArea>
        </Dialog>
      </Portal>
    );
  }
}

export default SelectModal;
