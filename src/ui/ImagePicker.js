import React, {Component} from 'react';
import IPicker from "react-native-image-crop-picker";
import {observer} from 'mobx-react';
import {action, observable} from 'mobx';
import {Button, Card, Modal, Portal} from 'react-native-paper';
import {withTranslation} from 'react-i18next';

@withTranslation('add_ad')
@observer
class ImagePicker extends Component {
  @observable visible = false;

  @action open = () => {
    this.visible = true;
  }

  @action close = () => {
    this.visible = false;
  }

  openCamera = () => {
    IPicker.openCamera({
      cropperToolbarTitle: 'Редактирование',
      width: 500,
      height: 400,
      cropping: true,
      mediaType: 'photo',
      compressImageMaxWidth: 500,
      compressImageMaxHeight: 300,
      compressImageQuality: 0.8,
    }).then(v => {
      this.props.onSelectImage(v);
      this.close();
    });
  };

  openGallery = () => {
    IPicker.openPicker({
      multiple: true,
      width: 500,
      height: 400,
      cropping: true,
      mediaType: 'photo',
      compressImageMaxWidth: 500,
      compressImageMaxHeight: 300,
      compressImageQuality: 0.8,
    }).then(images => {
      this.props.onSelectImages(images);
      this.close();
    });
  };

  constructor(props) {
    super(props);

    this.props.getContext(this);
  }

  render() {
    const {t} = this.props;

    return (
      <Portal>
        <Modal visible={this.visible} onDismiss={() => this.close()}>
          <Card style={{margin: 20}}>
            <Card.Content>
              <Button
                mode={'outlined'}
                onPress={() => this.openCamera()}
              >
                {t('image_picker.take_photo')}
              </Button>
              <Button
                mode={'outlined'}
                onPress={() => this.openGallery()}
                style={{marginTop: 10}}
              >
                {t('image_picker.choose_from_library')}
              </Button>
            </Card.Content>
          </Card>
        </Modal>
      </Portal>
    );
  }
}
export default ImagePicker;
