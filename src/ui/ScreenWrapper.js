import React from "react";
import {View, ScrollView} from "react-native";

class ScreenWrapper extends React.Component {
  static defaultProps = {
    padding: null,
    scroll: false,
    scrollViewProps: {},
    style: {},
  };

  render() {
    return (
      <View style={{flex: 1, padding: this.props.padding ? 8 : 0, ...this.props.style}}>
        {this.props.scroll ? (
          <ScrollView {...this.props.scrollViewProps}>
            {this.props.children}
          </ScrollView>
        ) : (
          <>
            {this.props.children}
          </>
        )}
      </View>
    );
  }
}

export default ScreenWrapper;
