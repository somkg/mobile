import React from 'react';
import {Card, Colors, Text, Title} from "react-native-paper";
import {View, Image} from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome5";
import {withTranslation} from "react-i18next";
import RootNavigation from "../utils/RootNavigation";
import {additionalQuestionRenderer} from "../utils/generators";
import {APP_COLOR} from "../utils/settings";
import {inject, observer} from "mobx-react";
import {action, computed, observable} from "mobx";

@withTranslation() @inject('store') @observer
class AdItem extends React.Component {
  static defaultProps = {
    inline: false,
  };

  @observable favorite_is_busy = false;

  constructor(props) {
    super(props);

    this.card_styles = this.props.inline ? {
      marginHorizontal: 8,
      marginBottom: 10,
      width: 150,
    } : {
      margin: 8,
    };

    this.card_styles['paddingBottom'] = 8;
    this.card_styles['flex'] = 1;

    this.store = this.props.store.appStore;
  }

  @computed get in_favorite() {
    return this.store.favorites.find((v) => v.product_type === 'ad' && v.product_id === this.props.data.id);
  }

  @action toggleFavorite = async () => {
    if (this.favorite_is_busy) return false;
    this.favorite_is_busy = true;
    try {
      await this.store.toggleFavorite(this.props.data.id, 'ad');
    } catch (e) {
      console.log(e);
    } finally {
      this.favorite_is_busy = false;
    }
  };

  render() {
    const {t, inline} = this.props;
    const v = this.props.data;
    let badge_color = APP_COLOR;

    if (v.status !== 'published') {
      if (v.status === 'sold') badge_color = Colors.cyan700;
      else if (v.status === 'rejected') badge_color = Colors.red700;
      else if (v.status === 'pending') badge_color = Colors.yellow700;
    }

    return (
      <Card
        onPress={() => RootNavigation.push('AdView', {item: v})}
        elevation={2}
        style={this.card_styles}
      >
        <View style={{position: 'relative'}}>
          <Image style={{height: inline ? 150 : (v.original_images.length > 0 ? 250 : 100), width: '100%'}} resizeMode={v.original_images.length > 0 ? 'contain' : 'cover'}
                 source={{uri: v.main_image}}/>
          {v.original_images.length > 1 && (
            <View style={{
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: 'rgba(23, 162, 184, 0.7)',
              border: '1px solid #eee',
              position: 'absolute',
              borderRadius: 3,
              flexDirection: 'row',
              padding: 3,
              left: 10,
              bottom: 10,
            }}>
              <Icon size={13} color={'#fff'} name={'camera-alt'}/>
              <Text style={{fontSize: 13, marginLeft: 4, color: '#fff'}}>{v.original_images.length}</Text>
            </View>
          )}
          {v.status !== 'published' && (
            <View style={{
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: badge_color,
              border: '1px solid #eee',
              position: 'absolute',
              borderRadius: 3,
              flexDirection: 'row',
              padding: 3,
              left: 10,
              top: 10
            }}>
              <Text style={{fontSize: 13, color: '#fff'}}>{t(`ad_status.${v.status}`)}</Text>
            </View>
          )}
          {v.status === 'published' && this.store.authenticated && (
            <Icon style={{
              backgroundColor: 'rgba(200, 200, 200, 0.7)',
              padding: 8,
              borderRadius: 8,
              color: APP_COLOR,
              position: 'absolute',
              right: 10,
              bottom: 10
            }} size={23} onPress={() => this.toggleFavorite()} name={`favorite${this.in_favorite ? '' : '-border'}`}/>
          )}
          {v.active_service ? (
            <View style={{position: 'absolute', top: 5, left: 5}}>
              <Text style={{backgroundColor: '#ff1493', color: '#fff', paddingHorizontal: 8, paddingVertical: 3, borderRadius: 5}}>
                VIP
              </Text>
            </View>
          ) : null}
        </View>
        <View flex={1} justifyContent={'space-between'}>
          <Title style={{
            fontSize: 17,
            color: '#17a2b8',
            marginLeft: 6,
            marginTop: 6,
            marginRight: 6,
            lineHeight: 17,
          }} numberOfLines={2}>{v[`title_${t('language')}`]}</Title>
          {(
            <>
              {Object.keys(v.additionals).map((name, key) => {
                if (!(
                  name === 'price' ||
                  name === 'place'
                )) return;
                const value = v.additionals[name];
                const {icon, title, content} = additionalQuestionRenderer({name, value});
                return (
                  <View style={{justifyContent: 'flex-start', marginLeft: 6, alignItems: 'center', flexDirection: 'row'}}
                        key={key}>
                    <Text style={{fontSize: 15}}>{icon ? <FontAwesomeIcon size={12} name={icon}/> : `${title}:`}</Text>
                    <Text style={{marginLeft: 4, fontSize: 15}}>{content}</Text>
                  </View>
                );
              })}
            </>
          )}
        </View>
      </Card>
    );
  }
}

export default AdItem;
