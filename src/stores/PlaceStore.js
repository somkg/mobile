import {observable, action} from "mobx";
import {requester} from '../utils';

export default class PlaceStore {
  storage = null;

  @observable items = observable.array([]);

  @action setItems(value, from_storage = false) {
    this.items = value;
    if (!from_storage) this.storage.set('places', value);
  }

  useStorage(s) {
    this.storage = s;
    s.get('places', []).then(value => {
      this.setItems(value, true);
    });
  }

  fetchAllItems() {
    requester.get('/place/get').then(data => {
      if (data.status === 'success') this.setItems(data.data);
    }).catch(e => {
      console.log(e);
    });
  }
}
