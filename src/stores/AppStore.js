import {observable, computed, action} from "mobx";
import {requester} from '../utils';
import {APP_VERSION} from "../utils/settings";
import User from "../models/User";
import {Platform, ToastAndroid} from "react-native";
import Notification from "../models/Notification";
import i18n from '../utils/i18n';

export default class AppStore {
  i18n = null;
  storage = null;

  @observable token = null;
  @observable authenticated = false;
  @observable authenticating = false;
  @observable busy_count = 0;
  @observable language = 'ru';
  @observable show_welcome_modal = false;
  @observable show_place_modal = false;
  @observable place = null;
  @observable fcm_token = null;

  @observable user = null;
  @observable notifications = observable.array([]);
  @observable notification_busy = false;

  @observable transactions = observable.array([]);
  @observable transactions_fetching = false;
  @observable transactions_page = 1;

  @observable favorites = observable.array([]);

  @computed get is_busy() {
    return this.busy_count > 0;
  }

  @computed get is_agent() {
    return this.authenticated && this.user.is_agent;
  }

  @computed get has_phone() {
    if (!this.authenticated || !this.user) return false;
    const phones = this.user.phones;
    return phones[0].exists || phones[1].exists || phones[2].exists || this.user.provider === 'phone';
  }

  @action markAsReadNotifications = () => {
    this.notifications = this.notifications.map(v => new Notification({...v, read_on: new Date()}));
  };

  @computed get language_label() {
    if (this.language === 'ru') return 'Русский';
    else return 'Кыргызча';
  }

  @action setFcmToken = (token) => {
    this.fcm_token = token;
    if (!this.token) return;
    requester.post('/user/session', {
      version_code: APP_VERSION,
      platform: Platform.OS,
      fcm_token: token,
    }).then(() => {}).catch(e => console.error(e));
  };

  @action setTransactionsPage(l) {
    this.transactions_page = l;
  }

  @action incTransactionsPage(r = 1) {
    this.transactions_page += r;
  }

  @action setTransactionsFetching(s) {
    this.transactions_fetching = s;
  }

  @action setTransactions(s) {
    this.transactions = s;
  }

  @action setBusyState(state) {
    this.busy_count += state ? 1 : -1;
  }

  @action setAuthenticated(value) {
    this.authenticated = value;
  }

  @action setAuthenticating(value) {
    this.authenticating = value;
  }

  @action setLanguage(lng, request = true, first = false) {
    if (lng === null) lng = 'ru';
    if (lng === null) {
      //this.show_welcome_modal = true;
    } else {
      this.show_welcome_modal = false;
      this.language = lng;
      this.i18n.changeLanguage(lng);
      this.storage.set('language', lng);

      if (!first) {
        if (lng === 'ru') this.showInfo('Язык сайта изменен на русский');
        else this.showInfo('Сайттын тили кыргызчага которулду');
      }

      if (this.authenticated && request) {
        requester.post(`/user/language`, {language: lng}).then((res) => {
        }).catch((e) => {
          console.log(e);
        });
      }
    }
  }

  @action setPlace(place, request = true) {
    if (place === null) {
      this.show_place_modal = true;
    } else {
      this.show_place_modal = false;
      if (place) this.storage.set('place', place);
      this.place = place;
      if (this.authenticated && this.user) {
        if (request) requester.post(`/user/place`, {place: place.id}).then(r => {
        });
        this.user.place = place;
      }
    }
  }

  @action setUser(user) {
    this.user = user ? new User(user) : null;
    if (this.user !== null) {
      if (this.user.place !== this.place) this.setPlace(user.place, false);
      if (this.user.language !== this.language) this.setLanguage(user.language, false);
      this.favorites = user.favorites.map(v => ({product_id: v.product_id, product_type: v.product_type}));
    }
  }

  @action setToken(token, fromStorage = false) {
    this.token = token;
    if (!fromStorage) {
      if (token === null) this.storage.remove('token');
      else this.storage.set('token', token);
    }
  }

  useI18n = (v) => {
    this.i18n = v;
  };

  @action useStorage(s) {
    this.storage = s;
    s.get('language').then(value => {
      this.setLanguage(value, false, true);
    });
    s.get('place').then(value => {
      this.setPlace(value);
    });
  }

  @action setFavorites(s) {
    this.favorites = s;
  }

  @action
  async toggleFavorite(pid, ptype = 'ad') {
    if (this.checkFavorite(pid, ptype)) {
      this.favorites = this.favorites.filter(v => !(v.product_type === ptype && v.product_id === pid));
      this.showWarning(i18n.t('favorite.deleted'));
      await requester.post('/user/favorites/delete', {
        product_type: ptype,
        product_id: pid,
      });
    } else {
      this.favorites.push({product_id: pid, product_type: ptype});
      this.showSuccess(i18n.t('favorite.added'));
      await requester.post('/user/favorites/add', {
        product_type: ptype,
        product_id: pid,
      });
    }
  }

  checkFavorite = (pid, ptype) => {
    return this.favorites.filter(v => v.product_type === ptype && v.product_id === pid).length > 0;
  };

  async fetchTransactions() {
    if (this.transactions_fetching) return;
    this.setTransactionsFetching(true);
    this.setTransactionsPage(1);
    try {
      const data = await requester.get('/user/balance/transactions', {
        limit: 30,
        page: 1
      });
      if (data.status === 'success') this.setTransactions(data.data);
    } finally {
      this.setTransactionsFetching(false);
    }
  }

  @action fetchNotifications = async () => {
    if (this.notification_busy) return false;
    try {
      this.notification_busy = true;
      const data = await requester.get(`/user/notifications`);
      this.notifications = data.data.map(v => new Notification(v));
    } catch (e) {
      console.log(e);
    } finally {
      this.notification_busy = false;
    }
  };

  @computed get new_notifications() {
    return this.notifications.filter(v => v.is_new);
  }

  checkAuth() {
    this.setAuthenticating(true);
    requester.get('/user/get', {id: 0}).then(data => {
      if (data.status === 'success') {
        this.setUser(data.data);
        this.setAuthenticated(true);
        this.fetchNotifications();
      } else {
        this.logOut();
      }
    }).catch(e => {
      console.log(e);
    }).finally(() => {
      this.setAuthenticating(false);
    });
  }

  logOut() {
    if (this.user) this.showInfo(this.i18n.t('toast.info.logOut', {name: this.user.full_name}));
    this.setAuthenticated(false);
    this.setUser(null);
    this.setToken(null);
  }

  makeAuth(data, status) {
    this.setUser(data.user);
    this.setAuthenticating(false);
    this.setAuthenticated(data.user !== null);
    this.setToken(data.token);
    this.showSuccess(this.i18n.t(`toast.success.${status}`, {name: data.user.full_name}));
    this.setFcmToken(this.fcm_token);
  }

  showSuccess(message) {
    this.pushMessage(message, 'success');
  }

  showError(message) {
    this.pushMessage(message, 'danger');
  }

  showInfo(message) {
    this.pushMessage(message, 'info');
  }

  showWarning(message) {
    this.pushMessage(message, 'warn');
  }

  @action pushMessage(message/*, level*/) {
    ToastAndroid.show(message, ToastAndroid.LONG);
  }
}
