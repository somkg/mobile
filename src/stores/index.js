import AppStore from './AppStore';
import AdStore from './AdStore';
import PlaceStore from './PlaceStore';

const appStore = new AppStore();

const stores = {
  appStore,
  adStore: new AdStore(appStore),
  placeStore: new PlaceStore(appStore),
};

export default stores;
