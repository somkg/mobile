import {axios_instance} from "./settings";
import {PixelRatio, Dimensions} from 'react-native';

export const daysToWord = (days, lang) => {
  if (lang === 'ky') return 'күн';
  else if (lang === 'ru') {
    if (days === 0 || days === 1) return 'день';
    else if (days >= 2 && days <= 4) return 'дня';
    else return 'дней';
  }
  return 'unknown';
};

export const checkBoolean = (s) => {
  return s === "1" || s === "true" || s === 1 || s === true;
};

export const codeTranslator = (f) => {
  let array = f.split('_');
  let result = '';
  array.map((v) => {
    result += v.substr(0, 1).toLocaleUpperCase() + v.substr(1, v.length - 1).toLocaleLowerCase();
  });
  return result;
};

export const filterPhoneNumber = (number) => {
  number = number || '|0|0';
  let r = {phone: "", whatsapp: false, confirmed: false, exists: false};
  if (number && number.length > 0) {
    const p = number.split('|');
    r.phone = p[0] || '';
    r.whatsapp = checkBoolean(p[1]);
    r.confirmed = checkBoolean(p[2]);
    r.exists = r.phone.length === 12;
  }
  return r;
};

export const services = {
  vip: {
    title: 'VIP',
    description: '',
    price: 150,
    style: 'dark',
    name: 'vip',
  },
  premium: {
    title: 'Премиум',
    description: '',
    price: 100,
    style: 'danger',
    name: 'premium',
  },
  exclusive: {
    title: 'Exclusive',
    description: '',
    price: 50,
    style: 'warning',
    name: 'exclusive',
  },
  free: {
    title: 'Обычная продажа',
    description: '',
    price: 0,
    style: 'secondary',
    name: 'free',
  },
};

export const buildArrayForBlob = (data, arr = [], p_key = '') => {
  if (data && typeof (data) == "object" && (data.join === undefined)) {
    Object.keys(data).map(v => {
      buildArrayForBlob(data[v], arr, p_key === '' ? v : `${p_key}[${v}]`);
    });
  } else if (data && typeof (data) == "object" && !(data.join === undefined)) {
    data.map((v, i) => {
      buildArrayForBlob(v, arr, p_key === '' ? i : `${p_key}[${i}]`);
    });
  } else {
    if (Number.isInteger(data)) data = data + '';
    else if (data === true) data = 'true';
    else if (data === false) data = 'false';
    else if (data === null) data = 'null';
    arr.push({name: p_key, data});
  }
  return arr;
};

export const buildFormData = (formData, data, parentKey) => {
  if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
    Object.keys(data).forEach(key => {
      buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
    });
  } else {
    let value = data == null ? '' : data;
    if (value === true) value = "1";
    else if (value === false) value = "0";
    formData.append(parentKey, value);
  }
};

export const requester = {
  get: async function (url, params) {
    const response = await axios_instance.get(url, {params: params});
    return response.data;
  },
  post: async function (url, data, config = {}) {
    const form_data = new FormData();
    if (data) buildFormData(form_data, data);
    const response = await axios_instance.post(url, form_data, config);
    return response.data;
  },
  put: async function (url, data) {
    const response = await axios_instance.put(url, data);
    return response.data;
  },
  delete: async function (url, params) {
    const response = await axios_instance.delete(url, {params: params});
    return response.data;
  }
};

export const size = (dp) => PixelRatio.getPixelSizeForLayoutSize(dp);

export const percentage = (num, per) => (num / 100) * per;

export const {
  width: SCREEN_WIDTH,
  height: SCREEN_HEIGHT,
} = Dimensions.get('window');

export const normalize = (size) => {
  return PixelRatio.roundToNearestPixel((SCREEN_WIDTH * size) / 100);
};
