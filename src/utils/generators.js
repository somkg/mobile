import i18n from '../utils/i18n';
import {EDUCATION_OF_SUMMARY, EXPERIENCES_OF_VACANCY, GENDER, SCHEDULES_OF_VACANCY, SUMMARY, VACANCIES} from "../utils/data";

export const ruAgeToWord = (age) => {
  age = age % 10;
  if (age === 1) return 'год';
  else if ([2, 3, 4].includes(age)) return 'года';
  else return 'лет';
};

export function additionalQuestionsRenderer(items) {
  const result = [];
  for (const [name, value] of Object.entries(items)) {
    result.push(additionalQuestionRenderer({name, value}));
  }
  return result;
}

export function additionalQuestionRenderer({name, value}) {
  const key = `adQuestions.formNames.${name}`;
  let title = i18n.exists(key) ? i18n.t(key) : null;
  let content = name;
  let icon = 'quote-right';

  switch (name) {
    case 'furniture':
      icon = 'chair';
      content = value ? i18n.t('adQuestions.has') : i18n.t('adQuestions.has_not');
      break;

    case 'rooms':
      icon = 'hotel';
      content = value;
      break;

    case 'place':
      icon = 'map-marker-alt';
      content = value[`title_${i18n.language}`];
      break;

    case 'experience_of_vacancy':
      content = EXPERIENCES_OF_VACANCY[value - 1][i18n.language];
      break;

    case 'schedule_of_vacancy':
      content = SCHEDULES_OF_VACANCY[value - 1][i18n.language];
      break;

    case 'education_of_summary':
      icon = 'graduation-cap';
      content = EDUCATION_OF_SUMMARY[value - 1][i18n.language];
      break;

    case 'vacancy':
      content = VACANCIES[value - 1][i18n.language];
      break;

    case 'gender':
      icon = 'venus-mars';
      content = GENDER[value - 1][i18n.language];
      break;

    case 'summary':
      content = SUMMARY[value - 1][i18n.language];
      break;

    case 'price':
      icon = 'dollar-sign';
      content = !value ? i18n.t('adQuestions.negotiable') : value.price + ' ' + value.currency;
      break;

    case 'wage_of_vacancy':
      icon = 'dollar-sign';
      content = !value ? i18n.t('adQuestions.negotiable') : value.price + ' ' + value.currency;
      break;

    case 'age_of_summary':
      content = `${value} ${i18n.t('language') === 'ky' ? 'жаш' : ruAgeToWord(value)}`;
      break;

    case 'floor':
      icon = 'home';
      content = value;
      break;

    case 'agency_or_owner':
      icon = 'user';
      content = i18n.t('adQuestions.' + (value === 'agency' ? 'agency' : 'owner'));
      break;

    case 'area_hundredths':
      icon = 'chart-area';
      content = `${value} сотых`;
      break;

    case 'area_square_meter':
      icon = 'chart-area';
      content = `${value} кв метр`;
      break;

    case 'car_make':
      icon = 'car';
      content = value;
      break;

    case 'car_model':
      icon = 'car';
      content = value;
      break;

    case 'year_of_issue':
      content = value;
      break;

    case 'fuel':
      icon = 'gas-pump';
      content = i18n.t(`adQuestions.${name}.${value}`);
      break;

    case 'gear_type':
      content = i18n.t(`adQuestions.${name}.${value}`);
      break;

    case 'minibus_make':
      icon = 'truck-moving';
      content = value;
      break;

    case 'truck_make':
      icon = 'truck-moving';
      content = value;
      break;

    case 'state':
      content = i18n.t(`adQuestions.${name}.${value}`);
      break;

    case 'phone_make':
      icon = 'mobile-alt';
      content = value;
      break;

    case 'computer_make':
      icon = 'laptop';
      content = value;
      break;

    case 'action':
      icon = 'handshake';
      content = i18n.t(`adQuestions.${name}.${value}`);
      break;

    default:
      break;
  }

  return {title, content, icon, name, value};
}
