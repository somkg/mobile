import {AsyncStorage} from 'react-native';

const get = async (name, default_value = null) => {
  try {
    const value = JSON.parse(await AsyncStorage.getItem(`@SomkgStore:${name}`));
    return value || default_value;
  } catch (error) {
    console.log(error);
    return default_value;
  }
};

const set = async (name, value) => {
  try {
    value = JSON.stringify(value);
    await AsyncStorage.setItem(`@SomkgStore:${name}`, value);
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
};

const remove = async (name) => {
  try {
    await AsyncStorage.removeItem(`@SomkgStore:${name}`);
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export default {
  get, set, remove,
};
