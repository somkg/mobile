import axios from "axios";

export const APP_NAME = 'Som.kg';
export const APP_COLOR = '#016BF4';

export const DEBUG_MODE = process.env.NODE_ENV !== "production";
export const API_VERSION = 1;
export const APP_VERSION = 66;
export const APP_VERSION_NAME = '2.9.6';

export const API_URL_WITHOUT_VERSION = "https://api.som.kg";

export function API_URL(version = API_VERSION) {
  return `${API_URL_WITHOUT_VERSION}/v${version}`;
}

export const axios_instance = axios.create({
  baseURL: API_URL(),
  responseType: 'json',
  responseEncoding: 'utf8',
});

axios_instance['_configs'] = {
  silence: false,
};

export function initialize(appStore) {
  axios_instance.interceptors.request.use(
    function (config) {
      appStore.setBusyState(true);
      if (appStore.token) config.headers.common.Authorization = "Basic " + appStore.token;
      return config;
    },
    function (error) {
      appStore.setBusyState(false);
      appStore.showError(error.message || error);
      return Promise.reject(error);
    }
  );

  axios_instance.interceptors.response.use(
    function (response) {
      const {data} = response;
      appStore.setBusyState(false);
      if (data.result < 0) {
        appStore.showError(data.data.message);
        Promise.reject(data);
      }
      return response;
    },
    function (error) {
      appStore.setBusyState(false);
      let msg = error.message;
      if (msg === 'Network Error') msg = 'Нет соединения с интернетом!';
      appStore.showError(msg || error);
      Promise.reject(error);
    }
  );
}
