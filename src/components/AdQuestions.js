import React from 'react';
import {withTranslation} from "react-i18next";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {Button, Dialog, Divider, Portal, RadioButton, Text, TextInput} from "react-native-paper";
import {TouchableOpacity, View, TouchableWithoutFeedback, FlatList} from "react-native";
import {checkBoolean} from "../utils";
import {inject, observer} from "mobx-react";
import {CAR_MAKES, GENDER, MINIBUS_MAKES, TRUCK_MAKES} from "../utils/data";
import {action, observable} from "mobx";
import moment from 'moment';
import SelectModal from "../ui/SelectModal";

@withTranslation(['add_ad', 'common'])
class PriceQuestion extends React.Component {
  form_name = 'price';

  constructor(props) {
    super(props);

    const {defaultValue} = this.props;

    this.initialState = {
      price: (defaultValue && defaultValue.price) || '',
      currency: (defaultValue && defaultValue.currency) || 'сом',
    };

    this.state = this.initialState;
  }

  setData = () => {
    let data = null;
    if (!(this.state.price.length < 1 || this.state.price < 1) && this.state.price.length > 0 && this.state.price > 0) data = {
      price: this.state.price,
      currency: this.state.currency
    };
    return data;
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState.price !== this.state.price || prevState.currency !== this.state.currency) {
      this.props.onValidValue(this.setData());
    }
  }

  negotiableHandler = () => {
    this.props.onValidValue(null);
    this.props.nextQuestion && this.props.nextQuestion();
  };

  render() {
    const {t} = this.props;

    return (
      <>
        <TextInput
          returnKeyType={'done'}
          mode={'outlined'}
          blurOnSubmit={true}
          value={this.state.price}
          dense={true}
          keyboardType={'numeric'}
          style={{marginBottom: 6}}
          placeholder={t(`common:adQuestions.questions.${this.form_name}`)}
          autoFocus={true}
          onChangeText={(text) => this.setState({price: text})}
          error={this.state.price.length > 0 && this.state.price < 1}
        />

        <RadioButton.Group style={{marginBottom: 8}} value={this.state.currency}>
          <TouchableOpacity
            style={{alignItems: 'center', flexDirection: 'row'}}
            onPress={() => this.setState({currency: 'сом'})}>
            <RadioButton onPress={() => this.setState({currency: 'сом'})} value="сом"/>
            <Text>сом</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{alignItems: 'center', flexDirection: 'row'}}
            onPress={() => this.setState({currency: 'доллар'})}>
            <RadioButton onPress={() => this.setState({currency: 'доллар'})} value="доллар"/>
            <Text>доллар</Text>
          </TouchableOpacity>
        </RadioButton.Group>

        <Button onPress={() => this.negotiableHandler()} icon={(props) => <FontAwesome5 name={'handshake'} {...props}/>}
                mode={this.state.price.length < 1 || this.state.price < 1 ? 'contained' : 'outlined'}>
          {t(`common:adQuestions.negotiable`)}
        </Button>
      </>
    );
  }
}

class RoomsQuestion extends React.Component {
  form_name = 'rooms';

  render() {
    return (
      <View flexDirection={'row'} justifyContent={'space-between'} flexWrap={'wrap'}>
        {[1, 2, 3, 4, 5, '6+'].map((v, key) => (
          <Button key={key} style={{margin: 4, width: '47%'}}
                  onPress={() => this.props.onValidValue(v)}
                  mode={this.props.defaultValue === v ? 'contained' : 'outlined'}>{v}</Button>
        ))}
      </View>
    );
  }
}

@withTranslation()
class FurnitureQuestion extends React.Component {
  form_name = 'furniture';

  render() {
    const {t} = this.props;
    const defaultValue = checkBoolean(this.props.defaultValue);

    return (
      <View flexDirection={'row'}>
        <Button
          flexGrow={1} style={{marginRight: 4}}
          onPress={() => this.props.onValidValue(true)}
          disabled={defaultValue === true && this.props.defaultValue !== undefined}
          mode={defaultValue === true && this.props.defaultValue !== undefined ? 'contained' : 'outlined'}>{t(`adQuestions.has`)}</Button>
        <Button
          flexGrow={1} style={{marginLeft: 4}}
          onPress={() => this.props.onValidValue(false)}
          disabled={defaultValue === false && this.props.defaultValue !== undefined}
          mode={defaultValue === false && this.props.defaultValue !== undefined ? 'contained' : 'outlined'}>{t(`adQuestions.has_not`)}</Button>
      </View>
    );
  }
}

@withTranslation() @inject('store') @observer
class PlaceQuestion extends React.Component {
  form_name = 'place';
  modal_ref = React.createRef();
  @observable modal_item = null;

  constructor(props) {
    super(props);

    this.placeStore = this.props.store.placeStore;
  }

  @action select = (v, anyway = false) => {
    if (v.children.length === 0 || anyway) {
      this.props.onValidValue(v);
      this.modal_ref.hide();
      this.modal_item = null;
    } else {
      this.modal_ref.show();
      this.modal_item = v;
    }
  };

  render() {
    const {t} = this.props;
    const id = (this.props.defaultValue && this.props.defaultValue.id) || null;

    return (
      <>
        <View flexDirection={'row'} justifyContent={'space-between'} flexWrap={'wrap'}>
          {this.placeStore.items.map((v, key) => (
            <Button key={key} style={{marginVertical: 4, width: '100%'}}
                    onPress={() => this.select(v)}
                    mode={id === v.id ? 'contained' : 'outlined'}>{v[`title_${t('language')}`]}</Button>
          ))}
        </View>

        <SelectModal
          ref={r => this.modal_ref = r}
          items={this.modal_item ? [{value: this.modal_item.id, title: this.modal_item[`title_${t('language')}`]}, ...this.modal_item.children.map(v => ({
            value: v.id,
            title: v[`title_${t('language')}`],
          }))] : []}
          value={id}
          title={'Выберите регион'}
          onSelect={(v) => this.select(this.modal_item.id === v ? this.modal_item : this.modal_item.children.find(g => g.id === v), true)}
        />
      </>
    );
  }
}

@withTranslation()
class AgencyOrOwnerQuestion extends React.Component {
  form_name = 'agency_or_owner';

  render() {
    const {t} = this.props;

    return (
      <View flexDirection={'row'} justifyContent={'space-between'} flexWrap={'wrap'}>
        {[
          {name: 'agency', value: t(`adQuestions.agency`)},
          {name: 'owner', value: t(`adQuestions.owner`)},
        ].map((v, key) => (
          <Button key={key} style={{marginVertical: 4, width: '100%'}}
                  onPress={() => this.props.onValidValue(v.name)}
                  mode={this.props.defaultValue === v.name ? 'contained' : 'outlined'}>{v.value}</Button>
        ))}
      </View>
    );
  }
}

@withTranslation()
class GenderQuestion extends React.Component {
  form_name = 'gender';

  render() {
    const {t} = this.props;

    return (
      <View flexDirection={'row'} justifyContent={'space-between'} flexWrap={'wrap'}>
        {GENDER.map((v, key) => (
          <Button key={key} style={{marginVertical: 4, width: '100%'}}
                  onPress={() => this.props.onValidValue(v.code)}
                  mode={this.props.defaultValue === v.code ? 'contained' : 'outlined'}>{v[t('common:language')]}</Button>
        ))}
      </View>
    );
  }
}

class AreaHundredthsQuestion extends React.Component {
  form_name = 'area_hundredths';

  constructor(props) {
    super(props);

    const {defaultValue} = this.props;

    this.initialState = {
      value: defaultValue || '',
    };

    this.state = this.initialState;
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.value !== this.state.value) {
      this.props.onValidValue(this.state.value);
    }
  }

  render() {
    return (
      <>
        <TextInput
          returnKeyType={'done'}
          mode={'outlined'}
          blurOnSubmit={true}
          value={this.state.value}
          dense={true}
          keyboardType={'numeric'}
          placeholder={'Площадь цифры сотых'}
          autoFocus={true}
          onChangeText={(text) => this.setState({value: text})}
          error={this.state.value.length > 0 && +this.state.value < 1}
        />
      </>
    );
  }
}

class AreaSquareMeterQuestion extends React.Component {
  form_name = 'area_square_meter';

  constructor(props) {
    super(props);

    const {defaultValue} = this.props;

    this.initialState = {
      value: defaultValue || '',
    };

    this.state = this.initialState;
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.value !== this.state.value) {
      this.props.onValidValue(this.state.value);
    }
  }

  render() {
    return (
      <>
        <TextInput
          returnKeyType={'done'}
          mode={'outlined'}
          blurOnSubmit={true}
          value={this.state.value}
          dense={true}
          keyboardType={'numeric'}
          placeholder={'Площадь — цифры кв.м.'}
          autoFocus={true}
          onChangeText={(text) => this.setState({value: text})}
          error={this.state.value.length > 0 && +this.state.value < 1}
        />
      </>
    );
  }
}

@withTranslation()
class ActionQuestion extends React.Component {
  form_name = 'action';

  render() {
    const {t} = this.props;

    return (
      <>
        <View flexDirection={'row'} justifyContent={'space-between'} flexWrap={'wrap'}>
          {[
            {name: 'buy', value: t(`adQuestions.${this.form_name}.buy`)},
            {name: 'sell', value: t(`adQuestions.${this.form_name}.sell`)},
            {name: 'rent', value: t(`adQuestions.${this.form_name}.rent`)},
          ].map((v, key) => (
            <Button key={key} style={{marginVertical: 4, width: '100%'}}
                    onPress={() => this.props.onValidValue(v.name)}
                    mode={this.props.defaultValue === v.name ? 'contained' : 'outlined'}>{v.value}</Button>
          ))}
        </View>
      </>
    );
  }
}

@withTranslation()
class FuelQuestion extends React.Component {
  form_name = 'fuel';

  render() {
    const {t} = this.props;

    return (
      <>
        <View flexDirection={'row'} justifyContent={'space-between'} flexWrap={'wrap'}>
          {[
            {name: 'petrol', value: t(`adQuestions.${this.form_name}.petrol`)},
            {name: 'diesel', value: t(`adQuestions.${this.form_name}.diesel`)},
            {name: 'gas', value: t(`adQuestions.${this.form_name}.gas`)},
            {name: 'petrol+gas', value: t(`adQuestions.${this.form_name}.petrol+gas`)},
          ].map((v, key) => (
            <Button key={key} style={{marginVertical: 4, width: '100%'}}
                    onPress={() => this.props.onValidValue(v.name)}
                    mode={this.props.defaultValue === v.name ? 'contained' : 'outlined'}>{v.value}</Button>
          ))}
        </View>
      </>
    );
  }
}

@withTranslation()
class WageOfVacancyQuestion extends React.Component {
  form_name = 'wage_of_vacancy';

  constructor(props) {
    super(props);

    const {defaultValue} = this.props;

    this.initialState = {
      price: (defaultValue && defaultValue.price) || '',
      currency: (defaultValue && defaultValue.currency) || 'сом',
    };

    this.state = this.initialState;
  }

  setData = () => {
    let data = null;
    if (!(this.state.price.length < 1 || this.state.price < 1) && this.state.price.length > 0 && this.state.price > 0) data = {
      price: this.state.price,
      currency: this.state.currency
    };
    return data;
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState.price !== this.state.price || prevState.currency !== this.state.currency) {
      this.props.onValidValue(this.setData());
    }

    (this.state.price.length < 1 || this.state.price < 1) && this.props.nextQuestion && this.props.nextQuestion();
  }

  negotiableHandler = () => {
    this.setState({price: ''});
    this.props.nextQuestion && this.props.nextQuestion();
  };

  render() {
    const {t} = this.props;

    return (
      <>
        <TextInput
          returnKeyType={'done'}
          mode={'outlined'}
          blurOnSubmit={true}
          value={this.state.price}
          dense={true}
          keyboardType={'numeric'}
          style={{marginBottom: 6}}
          placeholder={t(`adQuestions.questions.${this.form_name}`)}
          autoFocus={true}
          onChangeText={(text) => this.setState({price: text})}
          error={this.state.price.length > 0 && this.state.price < 1}
        />

        <RadioButton.Group style={{marginBottom: 8}} value={this.state.currency}>
          <TouchableOpacity
            style={{alignItems: 'center', flexDirection: 'row'}}
            onPress={() => this.setState({currency: 'сом'})}>
            <RadioButton onPress={() => this.setState({currency: 'сом'})} value="сом"/>
            <Text>сом</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{alignItems: 'center', flexDirection: 'row'}}
            onPress={() => this.setState({currency: 'доллар'})}>
            <RadioButton onPress={() => this.setState({currency: 'доллар'})} value="доллар"/>
            <Text>доллар</Text>
          </TouchableOpacity>
        </RadioButton.Group>

        <Button onPress={() => this.negotiableHandler()} icon={(props) => <FontAwesome5 name={'handshake'} {...props}/>}
                mode={this.state.price.length < 1 || this.state.price < 1 ? 'contained' : 'outlined'}>
          {t(`adQuestions.negotiable`)}
        </Button>
      </>
    );
  }
}

@withTranslation()
class StateQuestion extends React.Component {
  form_name = 'state';

  render() {
    const {t} = this.props;

    return (
      <>
        <View flexDirection={'row'} justifyContent={'space-between'} flexWrap={'wrap'}>
          {[
            {name: 'new', value: t(`adQuestions.${this.form_name}.new`)},
            {name: 'used', value: t(`adQuestions.${this.form_name}.used`)},
          ].map((v, key) => (
            <Button key={key} style={{marginVertical: 4, width: '100%'}}
                    onPress={() => this.props.onValidValue(v.name)}
                    mode={this.props.defaultValue === v.name ? 'contained' : 'outlined'}>{v.value}</Button>
          ))}
        </View>
      </>
    );
  }
}

@withTranslation()
@observer
class CarMakeQuestion extends React.Component {
  form_name = 'car_make';

  @observable visible = false;

  @action open = () => {
    this.visible = true;
  };

  @action close = () => {
    this.visible = false;
  };

  render() {
    const {t} = this.props;

    return (
      <>
        <Portal>
          <Dialog
            visible={this.visible}
            onDismiss={this.close}>
            <Dialog.Content>
              <FlatList
                ItemSeparatorComponent={() => <Divider/>}
                keyExtractor={({index}) => index}
                renderItem={({item, index}) => (
                  <TouchableOpacity onPress={() => this.props.onValidValue(item)} style={{paddingVertical: 8}}
                                    key={index}>
                    <Text>{item || t(`adQuestions.${this.form_name}.another`)}</Text>
                  </TouchableOpacity>
                )}
                data={Object.keys(CAR_MAKES)}/>
            </Dialog.Content>
          </Dialog>
        </Portal>

        <TouchableWithoutFeedback onPress={this.open}>
          <View>
            <TextInput
              label={'Марка'}
              editable={false}
              value={this.props.defaultValue || ''}
              mode={'outlined'}
              dense={true}
            />
          </View>
        </TouchableWithoutFeedback>
      </>
    );
  }
}

@withTranslation()
@observer
class CarModelQuestion extends React.Component {
  form_name = 'car_model';

  @observable visible = false;

  @action open = () => {
    this.visible = true;
  };

  @action close = () => {
    this.visible = false;
  };

  render() {
    const {t} = this.props;

    return (
      <>
        <Portal>
          <Dialog
            visible={this.visible}
            onDismiss={this.close}>
            <Dialog.Content>
              <FlatList
                ItemSeparatorComponent={() => <Divider/>}
                keyExtractor={({index}) => index}
                renderItem={({item, index}) => (
                  <TouchableOpacity onPress={() => this.props.onValidValue(item)} style={{paddingVertical: 8}}
                                    key={index}>
                    <Text>{item || t(`adQuestions.${this.form_name}.another`)}</Text>
                  </TouchableOpacity>
                )}
                data={CAR_MAKES[this.props.carMake]}/>
            </Dialog.Content>
          </Dialog>
        </Portal>

        <TouchableWithoutFeedback onPress={this.open}>
          <View>
            <TextInput
              label={'Модель'}
              editable={false}
              value={this.props.defaultValue || ''}
              mode={'outlined'}
              dense={true}
            />
          </View>
        </TouchableWithoutFeedback>
      </>
    );
  }
}

@observer
class YearOfIssueQuestion extends React.Component {
  form_name = 'year_of_issue';

  constructor(props) {
    super(props);

    this.year = moment().year();
  }

  @observable visible = false;

  @action open = () => {
    this.visible = true;
  };

  @action close = () => {
    this.visible = false;
  };

  render() {
    return (
      <>
        <Portal>
          <Dialog
            visible={this.visible}
            onDismiss={this.close}>
            <Dialog.Content>
              <FlatList
                ItemSeparatorComponent={() => <Divider/>}
                keyExtractor={({index}) => index}
                renderItem={({item, index}) => (
                  <TouchableOpacity onPress={() => this.props.onValidValue(this.year - item)}
                                    style={{paddingVertical: 8}}
                                    key={index}>
                    <Text>{this.year - item}</Text>
                  </TouchableOpacity>
                )}
                data={Array.from(Array(40).keys())}/>
            </Dialog.Content>
          </Dialog>
        </Portal>

        <TouchableWithoutFeedback onPress={this.open}>
          <View>
            <TextInput
              editable={false}
              value={this.props.defaultValue || 'Выберите'}
              mode={'outlined'}
              dense={true}
            />
          </View>
        </TouchableWithoutFeedback>
      </>
    );
  }
}

@withTranslation()
class GearTypeQuestion extends React.Component {
  form_name = 'gear_type';

  render() {
    const {t} = this.props;

    return (
      <>
        <View flexDirection={'row'} justifyContent={'space-between'} flexWrap={'wrap'}>
          {[
            {name: 'automatic', value: t(`adQuestions.${this.form_name}.automatic`)},
            {name: 'mechanics', value: t(`adQuestions.${this.form_name}.mechanics`)},
          ].map((v, key) => (
            <Button key={key} style={{marginVertical: 4, width: '100%'}}
                    onPress={() => this.props.onValidValue(v.name)}
                    mode={this.props.defaultValue === v.name ? 'contained' : 'outlined'}>{v.value}</Button>
          ))}
        </View>
      </>
    );
  }
}

@withTranslation()
@observer
class MinibusMakeQuestion extends React.Component {
  form_name = 'minibus_make';

  @observable visible = false;

  @action open = () => {
    this.visible = true;
  };

  @action close = () => {
    this.visible = false;
  };

  render() {
    const {t} = this.props;

    return (
      <>
        <Portal>
          <Dialog
            visible={this.visible}
            onDismiss={this.close}>
            <Dialog.Content>
              <FlatList
                ItemSeparatorComponent={() => <Divider/>}
                keyExtractor={({index}) => index}
                renderItem={({item, index}) => (
                  <TouchableOpacity onPress={() => this.props.onValidValue(item)}
                                    style={{paddingVertical: 8}}
                                    key={index}>
                    <Text>{item || t(`adQuestions.${this.form_name}.another`)}</Text>
                  </TouchableOpacity>
                )}
                data={MINIBUS_MAKES}/>
            </Dialog.Content>
          </Dialog>
        </Portal>

        <TouchableWithoutFeedback onPress={this.open}>
          <View>
            <TextInput
              editable={false}
              value={this.props.defaultValue || 'Выберите'}
              mode={'outlined'}
              dense={true}
            />
          </View>
        </TouchableWithoutFeedback>
      </>
    );
  }
}

@withTranslation()
@observer
class TruckMakeQuestion extends React.Component {
  form_name = 'truck_make';

  @observable visible = false;

  @action open = () => {
    this.visible = true;
  };

  @action close = () => {
    this.visible = false;
  };

  render() {
    const {t} = this.props;

    return (
      <>
        <Portal>
          <Dialog
            visible={this.visible}
            onDismiss={this.close}>
            <Dialog.Content>
              <FlatList
                ItemSeparatorComponent={() => <Divider/>}
                keyExtractor={({index}) => index}
                renderItem={({item, index}) => (
                  <TouchableOpacity onPress={() => this.props.onValidValue(item)}
                                    style={{paddingVertical: 8}}
                                    key={index}>
                    <Text>{item || t(`adQuestions.${this.form_name}.another`)}</Text>
                  </TouchableOpacity>
                )}
                data={TRUCK_MAKES}/>
            </Dialog.Content>
          </Dialog>
        </Portal>

        <TouchableWithoutFeedback onPress={this.open}>
          <View>
            <TextInput
              editable={false}
              value={this.props.defaultValue || 'Выберите'}
              mode={'outlined'}
              dense={true}
            />
          </View>
        </TouchableWithoutFeedback>
      </>
    );
  }
}

export default {
  PriceQuestion,
  RoomsQuestion,
  FurnitureQuestion,
  PlaceQuestion,
  AgencyOrOwnerQuestion,
  AreaHundredthsQuestion,
  AreaSquareMeterQuestion,
  ActionQuestion,
  StateQuestion,
  CarMakeQuestion,
  CarModelQuestion,
  YearOfIssueQuestion,
  GearTypeQuestion,
  FuelQuestion,
  WageOfVacancyQuestion,
  MinibusMakeQuestion,
  TruckMakeQuestion,
  GenderQuestion,
};
