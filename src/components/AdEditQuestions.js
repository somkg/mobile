import React from 'react';
import {withTranslation} from "react-i18next";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {Button, Card, Dialog, Divider, List, Portal, RadioButton, Text, TextInput, Title} from "react-native-paper";
import {TouchableOpacity, View, TouchableWithoutFeedback, FlatList} from "react-native";
import {checkBoolean} from "../utils";
import {inject, observer} from "mobx-react";
import {
  CAR_MAKES,
  COMPUTER_MAKES, EDUCATION_OF_SUMMARY, EXPERIENCES_OF_VACANCY,
  GENDER,
  MINIBUS_MAKES,
  PHONE_MAKES,
  SCHEDULES_OF_VACANCY, SUMMARY,
  TRUCK_MAKES,
  VACANCIES
} from "../utils/data";
import {action, observable} from "mobx";
import moment from 'moment';
import SelectModal from "../ui/SelectModal";
import {ruAgeToWord} from "../utils/generators";

@withTranslation(['add_ad', 'common'])
class PriceQuestion extends React.Component {
  form_name = 'price';

  constructor(props) {
    super(props);

    const {defaultValue} = this.props;

    this.initialState = {
      price: (defaultValue && defaultValue.price) || '',
      currency: (defaultValue && defaultValue.currency) || 'сом',
    };

    this.state = this.initialState;
  }

  setData = () => {
    let data = null;
    if (!(this.state.price.length < 1 || this.state.price < 1) && this.state.price.length > 0 && this.state.price > 0) data = {
      price: this.state.price,
      currency: this.state.currency
    };
    return data;
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState.price !== this.state.price || prevState.currency !== this.state.currency) {
      this.props.onValidValue(this.setData());
    }
  }

  negotiableHandler = () => {
    this.props.onValidValue(null);
  };

  render() {
    const {t} = this.props;

    return (
      <Card elevation={2} style={{margin: 8, marginTop: 0}}>
        <Card.Title title={t(`common:adQuestions.add_form_names.${this.form_name}`)}/>
        <Card.Content>
          <TextInput
            returnKeyType={'done'}
            mode={'outlined'}
            blurOnSubmit={true}
            value={this.state.price}
            dense={true}
            keyboardType={'numeric'}
            style={{marginBottom: 6}}
            placeholder={t(`common:adQuestions.questions.${this.form_name}`)}
            onChangeText={(text) => this.setState({price: text})}
            error={this.state.price.length > 0 && this.state.price < 1}
          />

          <RadioButton.Group style={{marginBottom: 8}} value={this.state.currency}>
            <TouchableOpacity
              style={{alignItems: 'center', flexDirection: 'row'}}
              onPress={() => this.setState({currency: 'сом'})}>
              <RadioButton onPress={() => this.setState({currency: 'сом'})} value="сом"/>
              <Text>сом</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{alignItems: 'center', flexDirection: 'row'}}
              onPress={() => this.setState({currency: 'доллар'})}>
              <RadioButton onPress={() => this.setState({currency: 'доллар'})} value="доллар"/>
              <Text>доллар</Text>
            </TouchableOpacity>
          </RadioButton.Group>

          <Button onPress={() => this.negotiableHandler()}
                  icon={(props) => <FontAwesome5 name={'handshake'} {...props}/>}
                  mode={this.state.price.length < 1 || this.state.price < 1 ? 'contained' : 'outlined'}>
            {t(`common:adQuestions.negotiable`)}
          </Button>
        </Card.Content>
      </Card>
    );
  }
}

@withTranslation()
class RoomsQuestion extends React.Component {
  form_name = 'rooms';

  render() {
    const {t} = this.props;

    return (
      <>
        <SelectModal
          ref={(ref) => this.modal_ref = ref}
          value={this.props.defaultValue}
          items={[1, 2, 3, 4, 5, '6+'].map(v => ({value: v, title: v}))}
          title={t(`adQuestions.formNames.${this.form_name}`)}
          onSelect={(v) => this.props.onValidValue(v)}
        />
        <Card onPress={() => this.modal_ref.show()} elevation={2} style={{margin: 8, marginTop: 0}}>
          <Card.Content>
            <Text>{t(`adQuestions.formNames.${this.form_name}`)}: {this.props.defaultValue}</Text>
          </Card.Content>
        </Card>
      </>
    );
  }
}

@withTranslation()
class FurnitureQuestion extends React.Component {
  form_name = 'furniture';

  render() {
    const {t} = this.props;
    const defaultValue = checkBoolean(this.props.defaultValue);

    return (
      <Card elevation={2} style={{margin: 8, marginTop: 0}}>
        <Card.Title title={t('adQuestions.formNames.' + this.form_name)}/>
        <Card.Content>
          <View flexDirection={'row'}>
            <Button
              flexGrow={1} style={{marginRight: 4}}
              onPress={() => this.props.onValidValue(true)}
              disabled={defaultValue === true && this.props.defaultValue !== undefined}
              mode={defaultValue === true && this.props.defaultValue !== undefined ? 'contained' : 'outlined'}>{t(`adQuestions.has`)}</Button>
            <Button
              flexGrow={1} style={{marginLeft: 4}}
              onPress={() => this.props.onValidValue(false)}
              disabled={defaultValue === false && this.props.defaultValue !== undefined}
              mode={defaultValue === false && this.props.defaultValue !== undefined ? 'contained' : 'outlined'}>{t(`adQuestions.has_not`)}</Button>
          </View>
        </Card.Content>
      </Card>
    );
  }
}

@withTranslation() @inject('store') @observer
class PlaceQuestion extends React.Component {
  form_name = 'place';

  constructor(props) {
    super(props);

    this.placeStore = this.props.store.placeStore;
  }

  render() {
    const {t} = this.props;
    const id = (this.props.defaultValue && this.props.defaultValue.id) || null;

    return (
      <>
        <SelectModal
          ref={(ref) => this.modal_ref = ref}
          value={id}
          items={this.placeStore.items.map(v => ({title: v[`title_${t('language')}`], value: v.id}))}
          title={t(`adQuestions.add_form_names.${this.form_name}`)}
          onSelect={(v) => this.props.onValidValue(this.placeStore.items.find(g => v === g.id))}
        />
        <Card onPress={() => this.modal_ref.show()} elevation={2} style={{margin: 8, marginTop: 0}}>
          <Card.Content>
            <Text>{t(`adQuestions.formNames.${this.form_name}`)}: {this.props.defaultValue[`title_${t('language')}`]}</Text>
          </Card.Content>
        </Card>
      </>
    );
  }
}

@withTranslation()
class AgencyOrOwnerQuestion extends React.Component {
  form_name = 'agency_or_owner';

  render() {
    const {t} = this.props;

    return (
      <>
        <SelectModal
          ref={(ref) => this.modal_ref = ref}
          value={this.props.defaultValue}
          items={[
            {value: 'agency', title: t(`adQuestions.agency`)},
            {value: 'owner', title: t(`adQuestions.owner`)},
          ]}
          title={t(`adQuestions.add_form_names.${this.form_name}`)}
          onSelect={(v) => this.props.onValidValue(v)}
        />
        <Card onPress={() => this.modal_ref.show()} elevation={2} style={{margin: 8, marginTop: 0}}>
          <Card.Content>
            <Text>{t(`adQuestions.add_form_names.${this.form_name}`)}: {t(`adQuestions.${this.props.defaultValue}`)}</Text>
          </Card.Content>
        </Card>
      </>
    );
  }
}

@withTranslation()
class GenderQuestion extends React.Component {
  form_name = 'gender';

  render() {
    const {t} = this.props;

    return (
      <Card elevation={2} style={{margin: 8, marginTop: 0}}>
        <Card.Title title={t('adQuestions.formNames.' + this.form_name)}/>
        <Card.Content>
          <View flexDirection={'row'} justifyContent={'space-between'} flexWrap={'wrap'}>
            {GENDER.map((v, key) => (
              <Button key={key} style={{marginVertical: 4, width: '100%'}}
                      onPress={() => this.props.onValidValue(v.code)}
                      mode={this.props.defaultValue === v.code ? 'contained' : 'outlined'}>{v[t('common:language')]}</Button>
            ))}
          </View>
        </Card.Content>
      </Card>
    );
  }
}

@withTranslation()
class AreaHundredthsQuestion extends React.Component {
  form_name = 'area_hundredths';

  render() {
    const {t} = this.props;

    return (
      <Card elevation={2} style={{margin: 8, marginTop: 0}}>
        <Card.Title title={t('adQuestions.add_form_names.' + this.form_name)}/>
        <Card.Content>
          <TextInput
            returnKeyType={'done'}
            mode={'outlined'}
            blurOnSubmit={true}
            value={this.props.defaultValue + ''}
            dense={true}
            keyboardType={'numeric'}
            placeholder={'Площадь цифры сотых'}
            onChangeText={(text) => this.props.onValidValue(text)}
            error={this.props.defaultValue.length === 0 || +this.props.defaultValue < 1}
          />
        </Card.Content>
      </Card>
    );
  }
}

@withTranslation()
class AreaSquareMeterQuestion extends React.Component {
  form_name = 'area_square_meter';

  render() {
    const {t} = this.props;

    return (
      <Card elevation={2} style={{margin: 8, marginTop: 0}}>
        <Card.Title title={t(`adQuestions.formNames.${this.form_name}`)}/>
        <Card.Content>
          <TextInput
            returnKeyType={'done'}
            mode={'outlined'}
            blurOnSubmit={true}
            value={this.props.defaultValue + ''}
            dense={true}
            keyboardType={'numeric'}
            placeholder={'Площадь — цифры кв.м.'}
            onChangeText={(text) => this.props.onValidValue(text)}
            error={this.props.defaultValue.length === 0 || +this.props.defaultValue < 1}
          />
        </Card.Content>
      </Card>
    );
  }
}

@withTranslation()
class ActionQuestion extends React.Component {
  form_name = 'action';

  render() {
    const {t} = this.props;

    return (
      <>
        <SelectModal
          ref={(ref) => this.modal_ref = ref}
          value={this.props.defaultValue}
          items={[
            {value: 'buy', title: t(`adQuestions.${this.form_name}.buy`)},
            {value: 'sell', title: t(`adQuestions.${this.form_name}.sell`)},
            {value: 'rent', title: t(`adQuestions.${this.form_name}.rent`)},
          ]}
          title={t(`adQuestions.formNames.${this.form_name}`)}
          onSelect={(v) => this.props.onValidValue(v)}
        />
        <Card onPress={() => this.modal_ref.show()} elevation={2} style={{margin: 8, marginTop: 0}}>
          <Card.Content>
            <Text>{t(`adQuestions.formNames.${this.form_name}`)}: {t(`adQuestions.${this.form_name}.${this.props.defaultValue}`)}</Text>
          </Card.Content>
        </Card>
      </>
    );
  }
}

@withTranslation()
class FuelQuestion extends React.Component {
  form_name = 'fuel';

  render() {
    const {t} = this.props;

    return (
      <>
        <SelectModal
          ref={(ref) => this.modal_ref = ref}
          value={this.props.defaultValue}
          items={[
            {value: 'petrol', title: t(`adQuestions.${this.form_name}.petrol`)},
            {value: 'diesel', title: t(`adQuestions.${this.form_name}.diesel`)},
            {value: 'gas', title: t(`adQuestions.${this.form_name}.gas`)},
            {value: 'petrol+gas', title: t(`adQuestions.${this.form_name}.petrol+gas`)},
          ]}
          title={t(`adQuestions.add_form_names.${this.form_name}`)}
          onSelect={(v) => this.props.onValidValue(v)}
        />
        <Card onPress={() => this.modal_ref.show()} elevation={2} style={{margin: 8, marginTop: 0}}>
          <Card.Content>
            <Text>{t(`adQuestions.formNames.${this.form_name}`)}: {t(`adQuestions.${this.form_name}.${this.props.defaultValue}`)}</Text>
          </Card.Content>
        </Card>
      </>
    );
  }
}

@withTranslation()
class WageOfVacancyQuestion extends React.Component {
  form_name = 'wage_of_vacancy';

  constructor(props) {
    super(props);

    const {defaultValue} = this.props;

    this.initialState = {
      price: (defaultValue && defaultValue.price) || '',
      currency: (defaultValue && defaultValue.currency) || 'сом',
    };

    this.state = this.initialState;
  }

  setData = () => {
    let data = null;
    if (!(this.state.price.length < 1 || this.state.price < 1) && this.state.price.length > 0 && this.state.price > 0) data = {
      price: this.state.price,
      currency: this.state.currency
    };
    return data;
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState.price !== this.state.price || prevState.currency !== this.state.currency) {
      this.props.onValidValue(this.setData());
    }
  }

  negotiableHandler = () => {
    this.setState({price: ''});
  };

  render() {
    const {t} = this.props;

    return (
      <Card elevation={2} style={{margin: 8, marginTop: 0}}>
        <Card.Title title={t(`common:adQuestions.add_form_names.${this.form_name}`)}/>
        <Card.Content>
          <TextInput
            returnKeyType={'done'}
            mode={'outlined'}
            blurOnSubmit={true}
            value={this.state.price}
            dense={true}
            keyboardType={'numeric'}
            style={{marginBottom: 6}}
            placeholder={t(`adQuestions.questions.${this.form_name}`)}
            onChangeText={(text) => this.setState({price: text})}
            error={this.state.price.length > 0 && this.state.price < 1}
          />

          <RadioButton.Group style={{marginBottom: 8}} value={this.state.currency}>
            <TouchableOpacity
              style={{alignItems: 'center', flexDirection: 'row'}}
              onPress={() => this.setState({currency: 'сом'})}>
              <RadioButton onPress={() => this.setState({currency: 'сом'})} value="сом"/>
              <Text>сом</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{alignItems: 'center', flexDirection: 'row'}}
              onPress={() => this.setState({currency: 'доллар'})}>
              <RadioButton onPress={() => this.setState({currency: 'доллар'})} value="доллар"/>
              <Text>доллар</Text>
            </TouchableOpacity>
          </RadioButton.Group>

          <Button onPress={() => this.negotiableHandler()}
                  icon={(props) => <FontAwesome5 name={'handshake'} {...props}/>}
                  mode={this.state.price.length < 1 || this.state.price < 1 ? 'contained' : 'outlined'}>
            {t(`adQuestions.negotiable`)}
          </Button>
        </Card.Content>
      </Card>
    );
  }
}

@withTranslation()
class AgeOfSummaryQuestion extends React.Component {
  form_name = 'age_of_summary';

  render() {
    const {t} = this.props;

    return (
      <>
        <SelectModal
          ref={(ref) => this.modal_ref = ref}
          value={this.props.defaultValue}
          items={Array.from(Array(82).keys()).map(v => ({title: v + 18, value: v + 18}))}
          title={t(`adQuestions.formNames.${this.form_name}`)}
          onSelect={(v) => this.props.onValidValue(v)}
        />
        <Card onPress={() => this.modal_ref.show()} elevation={2} style={{margin: 8, marginTop: 0}}>
          <Card.Content>
            <Text>{t(`adQuestions.formNames.${this.form_name}`)}: {`${this.props.defaultValue} ${t('language') === 'ky' ? 'жаш' : ruAgeToWord(this.props.defaultValue)}`}</Text>
          </Card.Content>
        </Card>
      </>
    );
  }
}

@withTranslation()
class StateQuestion extends React.Component {
  form_name = 'state';
  modal_ref = React.createRef();

  render() {
    const {t} = this.props;

    return (
      <>
        <SelectModal
          ref={(ref) => this.modal_ref = ref}
          value={this.props.defaultValue}
          items={[
            {value: 'new', title: t(`adQuestions.${this.form_name}.new`)},
            {value: 'used', title: t(`adQuestions.${this.form_name}.used`)},
          ]}
          title={t(`adQuestions.formNames.${this.form_name}`)}
          onSelect={(v) => this.props.onValidValue(v)}
        />
        <Card onPress={() => this.modal_ref.show()} elevation={2} style={{margin: 8, marginTop: 0}}>
          <Card.Content>
            <Text>{t(`adQuestions.formNames.${this.form_name}`)}: {t(`adQuestions.${this.form_name}.${this.props.defaultValue}`)}</Text>
          </Card.Content>
        </Card>
      </>
    );
  }
}

@withTranslation()
class ComputerMakeQuestion extends React.Component {
  form_name = 'computer_make';
  modal_ref = React.createRef();

  render() {
    const {t} = this.props;

    return (
      <>
        <SelectModal
          ref={(ref) => this.modal_ref = ref}
          value={this.props.defaultValue}
          items={COMPUTER_MAKES.map((v) => ({title: v || t(`adQuestions.${this.form_name}.another`), value: v}))}
          title={t(`adQuestions.formNames.${this.form_name}`)}
          onSelect={(v) => this.props.onValidValue(v)}
        />
        <Card onPress={() => this.modal_ref.show()} elevation={2} style={{margin: 8, marginTop: 0}}>
          <Card.Content>
            <Text>{t(`adQuestions.formNames.${this.form_name}`)}: {this.props.defaultValue}</Text>
          </Card.Content>
        </Card>
      </>
    );
  }
}

@withTranslation()
@observer
class CarMakeQuestion extends React.Component {
  form_name = 'car_make';

  @observable visible = false;

  @action open = () => {
    this.visible = true;
  };

  @action close = () => {
    this.visible = false;
  };

  onValidValue = (item) => {
    this.props.onValidValue(item);
    this.close();
  };

  render() {
    const {t} = this.props;

    return (
      <>
        <Portal>
          <Dialog
            visible={this.visible}
            onDismiss={this.close}>
            <Dialog.Content>
              <FlatList
                ItemSeparatorComponent={() => <Divider/>}
                keyExtractor={({index}) => index}
                renderItem={({item, index}) => (
                  <TouchableOpacity onPress={() => this.onValidValue(item)} style={{paddingVertical: 8}}
                                    key={index}>
                    <Text>{item || t(`adQuestions.${this.form_name}.another`)}</Text>
                  </TouchableOpacity>
                )}
                data={Object.keys(CAR_MAKES)}/>
            </Dialog.Content>
          </Dialog>
        </Portal>

        <Card elevation={2} style={{margin: 8, marginTop: 0}}>
          <Card.Content>
            <TouchableWithoutFeedback onPress={this.open}>
              <View>
                <TextInput
                  label={'Марка'}
                  editable={false}
                  value={this.props.defaultValue || t(`adQuestions.${this.form_name}.another`)}
                  mode={'outlined'}
                  dense={true}
                />
              </View>
            </TouchableWithoutFeedback>
          </Card.Content>
        </Card>
      </>
    );
  }
}

@withTranslation()
@observer
class CarModelQuestion extends React.Component {
  form_name = 'car_model';

  @observable visible = false;

  @action open = () => {
    this.visible = true;
  };

  @action close = () => {
    this.visible = false;
  };

  render() {
    const {t} = this.props;

    return (
      <>
        <Portal>
          <Dialog
            visible={this.visible}
            onDismiss={this.close}>
            <Dialog.Content>
              <FlatList
                ItemSeparatorComponent={() => <Divider/>}
                keyExtractor={({index}) => index}
                renderItem={({item, index}) => (
                  <TouchableOpacity onPress={() => this.props.onValidValue(item)} style={{paddingVertical: 8}}
                                    key={index}>
                    <Text>{item || t(`adQuestions.${this.form_name}.another`)}</Text>
                  </TouchableOpacity>
                )}
                data={CAR_MAKES[this.props.carMake]}/>
            </Dialog.Content>
          </Dialog>
        </Portal>

        <TouchableWithoutFeedback onPress={this.open}>
          <View>
            <TextInput
              label={'Модель'}
              editable={false}
              value={this.props.defaultValue || ''}
              mode={'outlined'}
              dense={true}
            />
          </View>
        </TouchableWithoutFeedback>
      </>
    );
  }
}

@withTranslation() @observer
class YearOfIssueQuestion extends React.Component {
  form_name = 'year_of_issue';

  constructor(props) {
    super(props);

    this.year = moment().year();
  }

  @observable visible = false;

  @action open = () => {
    this.visible = true;
  };

  @action close = () => {
    this.visible = false;
  };

  onSelect = (item) => {
    this.props.onValidValue(this.year - item);
    this.close();
  };

  render() {
    const {t} = this.props;

    return (
      <>
        <Portal>
          <Dialog
            visible={this.visible}
            onDismiss={this.close}>
            <Dialog.Content>
              <FlatList
                ItemSeparatorComponent={() => <Divider/>}
                keyExtractor={({index}) => index}
                renderItem={({item, index}) => (
                  <TouchableOpacity onPress={() => this.onSelect(item)}
                                    style={{paddingVertical: 8}}
                                    key={index}>
                    <Text>{this.year - item}</Text>
                  </TouchableOpacity>
                )}
                data={Array.from(Array(40).keys())}/>
            </Dialog.Content>
          </Dialog>
        </Portal>

        <Card elevation={2} style={{margin: 8, marginTop: 0}}>
          <Card.Title title={t('adQuestions.formNames.' + this.form_name)}/>
          <Card.Content>
            <TouchableWithoutFeedback onPress={this.open}>
              <View>
                <TextInput
                  editable={false}
                  value={this.props.defaultValue + '' || 'Выберите'}
                  mode={'outlined'}
                  dense={true}
                />
              </View>
            </TouchableWithoutFeedback>
          </Card.Content>
        </Card>
      </>
    );
  }
}

@withTranslation()
class GearTypeQuestion extends React.Component {
  form_name = 'gear_type';

  render() {
    const {t} = this.props;

    return (
      <>
        <Card elevation={2} style={{margin: 8, marginTop: 0}}>
          <Card.Title title={t('adQuestions.formNames.' + this.form_name)}/>
          <Card.Content>
            <View flexDirection={'row'} justifyContent={'space-between'} flexWrap={'wrap'}>
              {[
                {name: 'automatic', value: t(`adQuestions.${this.form_name}.automatic`)},
                {name: 'mechanics', value: t(`adQuestions.${this.form_name}.mechanics`)},
              ].map((v, key) => (
                <Button key={key} style={{marginVertical: 4, width: '100%'}}
                        onPress={() => this.props.onValidValue(v.name)}
                        disabled={this.props.defaultValue === v.name}
                        mode={this.props.defaultValue === v.name ? 'contained' : 'outlined'}>{v.value}</Button>
              ))}
            </View>
          </Card.Content>
        </Card>
      </>
    );
  }
}

@withTranslation()
@observer
class MinibusMakeQuestion extends React.Component {
  form_name = 'minibus_make';

  @observable visible = false;

  @action open = () => {
    this.visible = true;
  };

  @action close = () => {
    this.visible = false;
  };

  onValidValue = item => {
    this.props.onValidValue(item);
    this.close();
  };

  render() {
    const {t} = this.props;

    return (
      <>
        <Portal>
          <Dialog
            visible={this.visible}
            onDismiss={this.close}>
            <Dialog.Content>
              <FlatList
                ItemSeparatorComponent={() => <Divider/>}
                keyExtractor={({index}) => index}
                renderItem={({item, index}) => (
                  <TouchableOpacity onPress={() => this.onValidValue(item)}
                                    style={{paddingVertical: 8}}
                                    key={index}>
                    <Text>{item || t(`adQuestions.${this.form_name}.another`)}</Text>
                  </TouchableOpacity>
                )}
                data={MINIBUS_MAKES}/>
            </Dialog.Content>
          </Dialog>
        </Portal>

        <Card elevation={2} style={{margin: 8, marginTop: 0}}>
          <Card.Title title={t('adQuestions.formNames.' + this.form_name)}/>
          <Card.Content>
            <TouchableWithoutFeedback onPress={this.open}>
              <View>
                <TextInput
                  editable={false}
                  value={this.props.defaultValue || t(`adQuestions.${this.form_name}.another`)}
                  mode={'outlined'}
                  dense={true}
                />
              </View>
            </TouchableWithoutFeedback>
          </Card.Content>
        </Card>
      </>
    );
  }
}

@withTranslation()
@observer
class TruckMakeQuestion extends React.Component {
  form_name = 'truck_make';

  @observable visible = false;

  @action open = () => {
    this.visible = true;
  };

  @action close = () => {
    this.visible = false;
  };

  onValidValue = item => {
    this.props.onValidValue(item);
    this.close();
  };

  render() {
    const {t} = this.props;

    return (
      <>
        <Portal>
          <Dialog
            visible={this.visible}
            onDismiss={this.close}>
            <Dialog.Content>
              <FlatList
                ItemSeparatorComponent={() => <Divider/>}
                keyExtractor={({index}) => index}
                renderItem={({item, index}) => (
                  <TouchableOpacity onPress={() => this.onValidValue(item)}
                                    style={{paddingVertical: 8}}
                                    key={index}>
                    <Text>{item || t(`adQuestions.${this.form_name}.another`)}</Text>
                  </TouchableOpacity>
                )}
                data={TRUCK_MAKES}/>
            </Dialog.Content>
          </Dialog>
        </Portal>

        <Card elevation={2} style={{margin: 8, marginTop: 0}}>
          <Card.Title title={t(`adQuestions.formNames.${this.form_name}`)}/>
          <Card.Content>
            <TouchableWithoutFeedback onPress={this.open}>
              <View>
                <TextInput
                  editable={false}
                  value={this.props.defaultValue || t(`adQuestions.${this.form_name}.another`)}
                  mode={'outlined'}
                  dense={true}
                />
              </View>
            </TouchableWithoutFeedback>
          </Card.Content>
        </Card>
      </>
    );
  }
}

@withTranslation()
class PhoneMakeQuestion extends React.Component {
  form_name = 'phone_make';

  render() {
    const {t} = this.props;

    return (
      <>
        <Card elevation={2} style={{margin: 8, marginTop: 0}}>
          <Card.Content>
            <FlatList
              renderItem={({item}) => (
                <List.Item
                  onPress={() => this.props.onValidValue(item)}
                  title={item || t(`adQuestions.${this.form_name}.another`)}
                  right={props => <List.Icon {...props} icon={item === this.props.defaultValue ? 'check' : 'arrow-right'}/>}
                />
              )}
              ListHeaderComponent={() => <Title>{t(`adQuestions.add_form_names.${this.form_name}`)}</Title>}
              data={PHONE_MAKES}
              ItemSeparatorComponent={() => <Divider/>}
            />
          </Card.Content>
        </Card>
      </>
    );
  }
}

@withTranslation()
class VacancyQuestion extends React.Component {
  form_name = 'vacancy';

  render() {
    const {t} = this.props;

    return (
      <Card elevation={2} style={{margin: 8, marginTop: 0}}>
        <Card.Title title={t(`adQuestions.formNames.${this.form_name}`)}/>
        <Card.Content>
          <FlatList
            renderItem={({item}) => (
              <List.Item
                onPress={() => this.props.onValidValue(item.code)}
                title={item[t('language')]}
                right={props => <List.Icon {...props} icon={item.code === this.props.defaultValue ? 'check' : 'arrow-right'}/>}
              />
            )}
            data={VACANCIES}
            ItemSeparatorComponent={() => <Divider/>}
          />
        </Card.Content>
      </Card>
    );
  }
}

@withTranslation()
class ScheduleOfVacancyQuestion extends React.Component {
  form_name = 'schedule_of_vacancy';

  render() {
    const {t} = this.props;

    return (
      <Card elevation={2} style={{margin: 8, marginTop: 0}}>
        <Card.Title title={t(`adQuestions.formNames.${this.form_name}`)}/>
        <Card.Content>
          <FlatList
            renderItem={({item}) => (
              <List.Item
                onPress={() => this.props.onValidValue(item.code)}
                title={item[t('language')]}
                right={props => <List.Icon {...props} icon={item.code === this.props.defaultValue ? 'check' : 'arrow-right'}/>}
              />
            )}
            data={SCHEDULES_OF_VACANCY}
            ItemSeparatorComponent={() => <Divider/>}
          />
        </Card.Content>
      </Card>
    );
  }
}

@withTranslation()
class ExperienceOfVacancyQuestion extends React.Component {
  form_name = 'experience_of_vacancy';

  render() {
    const {t} = this.props;

    return (
      <Card elevation={0} style={{margin: 8, marginTop: 0}}>
        <Card.Title title={t(`adQuestions.formNames.${this.form_name}`)}/>
        <Card.Content>
          <FlatList
            renderItem={({item}) => (
              <List.Item
                onPress={() => this.props.onValidValue(item.code)}
                title={item[t('language')]}
                right={props => <List.Icon {...props} icon={item.code === this.props.defaultValue ? 'check' : 'arrow-right'}/>}
              />
            )}
            data={EXPERIENCES_OF_VACANCY}
            ItemSeparatorComponent={() => <Divider/>}
          />
        </Card.Content>
      </Card>
    );
  }
}

@withTranslation()
class SummaryQuestion extends React.Component {
  form_name = 'summary';

  render() {
    const {t} = this.props;

    return (
      <Card elevation={2} style={{margin: 8, marginTop: 0}}>
        <Card.Title title={t(`adQuestions.formNames.${this.form_name}`)}/>
        <Card.Content>
          <FlatList
            renderItem={({item}) => (
              <List.Item
                onPress={() => this.props.onValidValue(item.code)}
                title={item[t('language')]}
                right={props => <List.Icon {...props} icon={item.code === this.props.defaultValue ? 'check' : 'arrow-right'}/>}
              />
            )}
            data={SUMMARY}
            ItemSeparatorComponent={() => <Divider/>}
          />
        </Card.Content>
      </Card>
    );
  }
}

@withTranslation()
class EducationOfSummaryQuestion extends React.Component {
  form_name = 'education_of_summary';

  render() {
    const {t} = this.props;

    return (
      <Card elevation={2} style={{margin: 8, marginTop: 0}}>
        <Card.Title title={t(`adQuestions.formNames.${this.form_name}`)}/>
        <Card.Content>
          <FlatList
            renderItem={({item}) => (
              <List.Item
                onPress={() => this.props.onValidValue(item.code)}
                title={item[t('language')]}
                right={props => <List.Icon {...props} icon={item.code === this.props.defaultValue ? 'check' : 'arrow-right'}/>}
              />
            )}
            data={EDUCATION_OF_SUMMARY}
            ItemSeparatorComponent={() => <Divider/>}
          />
        </Card.Content>
      </Card>
    );
  }
}

export default {
  PriceQuestion,
  FurnitureQuestion,
  PlaceQuestion,
  AgencyOrOwnerQuestion,
  AreaHundredthsQuestion,
  AreaSquareMeterQuestion,
  ActionQuestion,
  StateQuestion,
  ComputerMakeQuestion,
  CarMakeQuestion,
  CarModelQuestion, // not ready
  YearOfIssueQuestion,
  GearTypeQuestion,
  FuelQuestion,
  WageOfVacancyQuestion,
  MinibusMakeQuestion,
  TruckMakeQuestion,
  GenderQuestion,
  AgeOfSummaryQuestion,
  VacancyQuestion,
  ScheduleOfVacancyQuestion,
  ExperienceOfVacancyQuestion,
  SummaryQuestion,
  EducationOfSummaryQuestion,
  PhoneMakeQuestion,
};
