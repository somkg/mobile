import React from 'react';
import {Subheading, Portal, Dialog, RadioButton, Text} from "react-native-paper";
import {TouchableOpacity} from "react-native";
import {inject, observer} from "mobx-react";

@inject('store') @observer
class WelcomeModal extends React.Component {
  constructor(props) {
    super(props);

    this.store = this.props.store;
    this.appStore = this.store.appStore;
  }

  setLanguage = (l) => this.appStore.setLanguage(l, false, true);

  render() {
    return (
      <Portal>
        <Dialog
          visible={this.appStore.show_welcome_modal}>
          <Dialog.Title>Добро пожаловать! / Кош келиңиз!</Dialog.Title>
          <Dialog.Content>
            <Subheading>Выберите язык / Тил тандаңыз</Subheading>
            <RadioButton.Group>
              <TouchableOpacity
                style={{alignItems: 'center', flexDirection: 'row'}}
                onPress={() => this.setLanguage('ru')}>
                <RadioButton onPress={() => this.setLanguage('ru')} value="ru"/>
                <Text>Русский</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{alignItems: 'center', flexDirection: 'row'}}
                onPress={() => this.setLanguage('ky')}>
                <RadioButton onPress={() => this.setLanguage('ky')} value="ky"/>
                <Text>Кыргызча</Text>
              </TouchableOpacity>
            </RadioButton.Group>
          </Dialog.Content>
        </Dialog>
      </Portal>
    );
  }
}

export default WelcomeModal;
