import React from 'react';
import {Portal, Dialog, RadioButton, Text} from "react-native-paper";
import {TouchableOpacity} from "react-native";
import {inject, observer} from "mobx-react";
import {withTranslation} from "react-i18next";

@withTranslation('home') @inject('store') @observer
class AdFilterByPlaceModal extends React.Component {
  static defaultProps = {
    make_store: true,
  };

  constructor(props) {
    super(props);

    this.store = this.props.store;
    this.appStore = this.store.appStore;
    this.placeStore = this.store.placeStore;
    this.adStore = this.store.adStore;
  }

  onSelectFilter = (p) => {
    if (this.props.make_store) {
      if (p.id > 0) {
        this.appStore.setPlace(p);
        this.adStore.toggleFilter(false);
      } else {
        this.adStore.toggleFilter(true);
      }
    }
    this.props.onChange(p.id > 0, p);
    this.props.dismiss();
  };

  render() {
    if (!this.appStore.place && this.props.make_store) return null;

    const {t} = this.props;

    return (
      <Portal>
        <Dialog
          dismissable={true}
          onDismiss={() => this.props.dismiss()}
          visible={this.props.show}>
          <Dialog.Title>{t('ad_filter_by_place')}</Dialog.Title>
          <Dialog.Content>
            <RadioButton.Group value={`id_${this.props.make_store ? (this.adStore.filter_is_disabled ? 0 : this.appStore.place.id) : this.props.value}`}>
              {[{title_ru: 'Кыргызстан', title_ky: 'Кыргызстан', id: 0,}, ...this.placeStore.items].map((v, k) => (
                <TouchableOpacity
                  key={k}
                  style={{alignItems: 'center', flexDirection: 'row'}}
                  onPress={() => this.onSelectFilter(v)}>
                  <RadioButton value={`id_${v.id}`} onPress={() => this.onSelectFilter(v)}/>
                  <Text>{v[`title_${this.appStore.language}`]}</Text>
                </TouchableOpacity>
              ))}
            </RadioButton.Group>
          </Dialog.Content>
        </Dialog>
      </Portal>
    );
  }
}

export default AdFilterByPlaceModal;
