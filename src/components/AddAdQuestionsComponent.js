import React from 'react';
import {inject, observer} from "mobx-react";
import {codeTranslator} from "../utils";
import {View, FlatList, ScrollView} from 'react-native';
import {Card, Button, Title, List, Divider} from "react-native-paper";
import Questions from "./AdQuestions";
import {withTranslation} from "react-i18next";
import {
  COMPUTER_MAKES,
  EDUCATION_OF_SUMMARY,
  EXPERIENCES_OF_VACANCY,
  PHONE_MAKES,
  SCHEDULES_OF_VACANCY,
  SUMMARY,
  VACANCIES
} from "../utils/data";
import {ruAgeToWord} from "../utils/generators";

@withTranslation(['common', 'add_ad'])
@inject('store')
@observer
class AddAdPriceQuestion extends React.Component {
  form_name = 'price';

  constructor(props) {
    super(props);

    this.adStore = this.props.store.adStore;

    this.state = {value: null};
  }

  nextQuestion = () => {
    this.adStore.setAddAdAdditionals(this.form_name, this.state.value);
    this.props.nextQuestion();
  };

  prevQuestion = () => {
    this.adStore.setAddAdAdditionals(this.form_name, this.state.value);
    this.props.prevQuestion();
  };

  onValidValue = (value) => {
    this.setState({value});
  };

  render() {
    if (this.adStore.addAdCurrentShow !== this.form_name) return null;

    const {t} = this.props;
    const Question = Questions[`${codeTranslator(this.form_name)}Question`];

    return (
      <ScrollView keyboardShouldPersistTaps={'always'}>
        <Card>
          <Card.Title title={t(`common:adQuestions.add_form_names.${this.form_name}`)}/>
          <Card.Content>
            <Question
              defaultValue={this.adStore.addAdAdditionals[this.form_name]}
              onValidValue={this.onValidValue}
              nextQuestion={() => this.nextQuestion()}
              prevQuestion={() => this.prevQuestion()}
            />
          </Card.Content>
        </Card>
        <View flexDirection={'row'} style={{marginTop: 8}}>
          <Button flexGrow={1} style={{marginRight: 4}} mode={'outlined'} onPress={() => this.prevQuestion()}>
            {t("add_ad:label.prev_question")}
          </Button>
          <Button flexGrow={1} style={{marginLeft: 4}} mode={'contained'} onPress={() => this.nextQuestion()}>
            {t("add_ad:label.next_question")}
          </Button>
        </View>
      </ScrollView>
    );
  }
}

@withTranslation(['common', 'add_ad'])
@inject('store')
@observer
class AddAdRoomsQuestion extends React.Component {
  form_name = 'rooms';

  constructor(props) {
    super(props);

    this.adStore = this.props.store.adStore;
  }

  onValidValue = (t) => {
    this.adStore.setAddAdAdditionals(this.form_name, t);
    this.props.nextQuestion();
  };

  render() {
    if (this.adStore.addAdCurrentShow !== this.form_name) return null;

    const {t} = this.props;
    const Question = Questions[`${codeTranslator(this.form_name)}Question`];

    return (
      <>
        <Card>
          <Card.Title title={t(`common:adQuestions.add_form_names.${this.form_name}`)}/>
          <Card.Content>
            <Question
              defaultValue={this.adStore.addAdAdditionals[this.form_name]}
              onValidValue={this.onValidValue}
              nextQuestion={() => this.props.nextQuestion()}
              prevQuestion={() => this.props.prevQuestion()}
            />
          </Card.Content>
        </Card>
        <View flexDirection={'row'}>
          <Button flexGrow={1} style={{marginRight: 4}} mode={'outlined'} onPress={() => this.props.prevQuestion()}>
            {t("add_ad:label.prev_question")}
          </Button>
          <Button flexGrow={1} style={{marginLeft: 4}} mode={'contained'} onPress={() => this.props.nextQuestion()}
                  disabled={!this.adStore.addAdAdditionals[this.form_name]}>
            {t("add_ad:label.next_question")}
          </Button>
        </View>
      </>
    );
  }
}

@withTranslation(['common', 'add_ad'])
@inject('store')
@observer
class AddAdFurnitureQuestion extends React.Component {
  form_name = 'furniture';

  constructor(props) {
    super(props);

    this.adStore = this.props.store.adStore;
  }

  onValidValue = (t) => {
    this.adStore.setAddAdAdditionals(this.form_name, t);
    this.props.nextQuestion();
  };

  render() {
    if (this.adStore.addAdCurrentShow !== this.form_name) return null;

    const {t} = this.props;
    const Question = Questions[`${codeTranslator(this.form_name)}Question`];

    return (
      <>
        <Card>
          <Card.Title title={t(`common:adQuestions.add_form_names.${this.form_name}`)}/>
          <Card.Content>
            <Question
              defaultValue={this.adStore.addAdAdditionals[this.form_name]}
              onValidValue={this.onValidValue}
              nextQuestion={() => this.props.nextQuestion()}
              prevQuestion={() => this.props.prevQuestion()}
            />
          </Card.Content>
        </Card>
        <View flexDirection={'row'}>
          <Button flexGrow={1} style={{marginRight: 4}} mode={'outlined'} onPress={() => this.props.prevQuestion()}>
            {t("add_ad:label.prev_question")}
          </Button>
          <Button flexGrow={1} style={{marginLeft: 4}} mode={'contained'} onPress={() => this.props.nextQuestion()}
                  disabled={typeof this.adStore.addAdAdditionals[this.form_name] !== 'boolean'}>
            {t("add_ad:label.next_question")}
          </Button>
        </View>
      </>
    );
  }
}

@withTranslation(['common', 'add_ad'])
@inject('store')
@observer
class AddAdPlaceQuestion extends React.Component {
  form_name = 'place';

  constructor(props) {
    super(props);

    this.adStore = this.props.store.adStore;
  }

  onValidValue = (value) => {
    this.adStore.setAddAdAdditionals(this.form_name, value);
    this.props.nextQuestion();
  };

  render() {
    if (this.adStore.addAdCurrentShow !== this.form_name) return null;

    const {t} = this.props;
    const Question = Questions[`${codeTranslator(this.form_name)}Question`];

    return (
      <>
        <Card>
          <Card.Title title={t(`common:adQuestions.add_form_names.${this.form_name}`)}/>
          <Card.Content>
            <Question
              defaultValue={this.adStore.addAdAdditionals[this.form_name]}
              onValidValue={this.onValidValue}
              nextQuestion={() => this.props.nextQuestion()}
              prevQuestion={() => this.props.prevQuestion()}
            />
          </Card.Content>
        </Card>
        <View flexDirection={'row'}>
          <Button flexGrow={1} style={{marginRight: 4}} mode={'outlined'} onPress={() => this.props.prevQuestion()}>
            {t("add_ad:label.prev_question")}
          </Button>
          <Button flexGrow={1} style={{marginLeft: 4}} mode={'contained'} onPress={() => this.props.nextQuestion()}
                  disabled={!this.adStore.addAdAdditionals[this.form_name]}>
            {t("add_ad:label.next_question")}
          </Button>
        </View>
      </>
    );
  }
}

@withTranslation(['common', 'add_ad'])
@inject('store')
@observer
class AddAdAgencyOrOwnerQuestion extends React.Component {
  form_name = 'agency_or_owner';

  constructor(props) {
    super(props);

    this.adStore = this.props.store.adStore;
  }

  onValidValue = (t) => {
    this.adStore.setAddAdAdditionals(this.form_name, t);
    this.props.nextQuestion();
  };

  render() {
    if (this.adStore.addAdCurrentShow !== this.form_name) return null;

    const {t} = this.props;
    const Question = Questions[`${codeTranslator(this.form_name)}Question`];

    return (
      <>
        <Card>
          <Card.Content>
            <Title>{t(`common:adQuestions.add_form_names.${this.form_name}`)}</Title>
            <Question
              defaultValue={this.adStore.addAdAdditionals[this.form_name]}
              onValidValue={this.onValidValue}
              nextQuestion={() => this.props.nextQuestion()}
              prevQuestion={() => this.props.prevQuestion()}
            />
          </Card.Content>
        </Card>
        <View flexDirection={'row'}>
          <Button flexGrow={1} style={{marginRight: 4}} mode={'outlined'} onPress={() => this.props.prevQuestion()}>
            {t("add_ad:label.prev_question")}
          </Button>
          <Button flexGrow={1} style={{marginLeft: 4}} mode={'contained'} onPress={() => this.props.nextQuestion()}
                  disabled={!this.adStore.addAdAdditionals[this.form_name]}>
            {t("add_ad:label.next_question")}
          </Button>
        </View>
      </>
    );
  }
}

@withTranslation(['add_ad'])
@inject('store')
@observer
class AddAdAreaHundredthsQuestion extends React.Component {
  form_name = 'area_hundredths';

  constructor(props) {
    super(props);

    this.adStore = this.props.store.adStore;
  }

  onValidValue = (t) => {
    this.adStore.setAddAdAdditionals(this.form_name, t);
  };

  render() {
    if (this.adStore.addAdCurrentShow !== this.form_name) return null;

    const {t} = this.props;
    const Question = Questions[`${codeTranslator(this.form_name)}Question`];

    return (
      <ScrollView keyboardShouldPersistTaps={'always'}>
        <Card>
          <Card.Content>
            <Title>Площадь цифры сотых</Title>
            <Question
              defaultValue={this.adStore.addAdAdditionals[this.form_name]}
              onValidValue={this.onValidValue}
            />
          </Card.Content>
        </Card>
        <View flexDirection={'row'} style={{marginTop: 8}}>
          <Button flexGrow={1} style={{marginRight: 4}} mode={'outlined'} onPress={() => this.props.prevQuestion()}>
            {t("add_ad:label.prev_question")}
          </Button>
          <Button flexGrow={1} style={{marginLeft: 4}} mode={'contained'} onPress={() => this.props.nextQuestion()}
                  disabled={this.form_name in this.adStore.addAdAdditionals ? +this.adStore.addAdAdditionals[this.form_name] < 1 : true}>
            {t("add_ad:label.next_question")}
          </Button>
        </View>
      </ScrollView>
    );
  }
}

@withTranslation(['add_ad'])
@inject('store')
@observer
class AddAdAreaSquareMeterQuestion extends React.Component {
  form_name = 'area_square_meter';

  constructor(props) {
    super(props);

    this.adStore = this.props.store.adStore;
  }

  onValidValue = (t) => {
    this.adStore.setAddAdAdditionals(this.form_name, t);
  };

  render() {
    if (this.adStore.addAdCurrentShow !== this.form_name) return null;

    const {t} = this.props;
    const Question = Questions[`${codeTranslator(this.form_name)}Question`];

    return (
      <ScrollView keyboardShouldPersistTaps={'always'}>
        <Card>
          <Card.Content>
            <Title>Площадь — цифры кв.м.</Title>
            <Question
              defaultValue={this.adStore.addAdAdditionals[this.form_name]}
              onValidValue={this.onValidValue}
            />
          </Card.Content>
        </Card>
        <View flexDirection={'row'} style={{marginTop: 8}}>
          <Button flexGrow={1} style={{marginRight: 4}} mode={'outlined'} onPress={() => this.props.prevQuestion()}>
            {t("add_ad:label.prev_question")}
          </Button>
          <Button flexGrow={1} style={{marginLeft: 4}} mode={'contained'} onPress={() => this.props.nextQuestion()}
                  disabled={this.form_name in this.adStore.addAdAdditionals ? +this.adStore.addAdAdditionals[this.form_name] < 1 : true}>
            {t("add_ad:label.next_question")}
          </Button>
        </View>
      </ScrollView>
    );
  }
}

@withTranslation(['add_ad'])
@inject('store')
@observer
class AddAdActionQuestion extends React.Component {
  form_name = 'action';

  constructor(props) {
    super(props);

    this.adStore = this.props.store.adStore;
  }

  setData = (t) => {
    this.adStore.setAddAdAdditionals(this.form_name, t);
    this.props.nextQuestion();
  };

  render() {
    if (this.adStore.addAdCurrentShow !== this.form_name) return null;

    const {t} = this.props;
    const Question = Questions[`${codeTranslator(this.form_name)}Question`];

    return (
      <>
        <Card>
          <Card.Content>
            <Question
              defaultValue={this.adStore.addAdAdditionals[this.form_name]}
              onValidValue={this.setData}
            />
          </Card.Content>
        </Card>
        <View flexDirection={'row'}>
          <Button flexGrow={1} style={{marginRight: 4}} mode={'outlined'} onPress={() => this.props.prevQuestion()}>
            {t("add_ad:label.prev_question")}
          </Button>
          <Button flexGrow={1} style={{marginLeft: 4}} mode={'contained'} onPress={() => this.props.nextQuestion()}
                  disabled={!this.adStore.addAdAdditionals[this.form_name]}>
            {t("add_ad:label.next_question")}
          </Button>
        </View>
      </>
    );
  }
}

@withTranslation(['common', 'add_ad'])
@inject('store')
@observer
class AddAdFuelQuestion extends React.Component {
  form_name = 'fuel';

  constructor(props) {
    super(props);

    this.adStore = this.props.store.adStore;
  }

  setData = (t) => {
    this.adStore.setAddAdAdditionals(this.form_name, t);
    this.props.nextQuestion();
  };

  render() {
    if (this.adStore.addAdCurrentShow !== this.form_name) return null;

    const {t} = this.props;
    const Question = Questions[`${codeTranslator(this.form_name)}Question`];

    return (
      <>
        <Card>
          <Card.Content>
            <Title>{t(`common:adQuestions.add_form_names.${this.form_name}`)}</Title>
            <Question
              defaultValue={this.adStore.addAdAdditionals[this.form_name]}
              onValidValue={this.setData}
            />
          </Card.Content>
        </Card>
        <View flexDirection={'row'}>
          <Button flexGrow={1} style={{marginRight: 4}} mode={'outlined'} onPress={() => this.props.prevQuestion()}>
            {t("add_ad:label.prev_question")}
          </Button>
          <Button flexGrow={1} style={{marginLeft: 4}} mode={'contained'} onPress={() => this.props.nextQuestion()}
                  disabled={!this.adStore.addAdAdditionals[this.form_name]}>
            {t("add_ad:label.next_question")}
          </Button>
        </View>
      </>
    );
  }
}

@withTranslation(['add_ad'])
@inject('store') @observer
class AddAdWageOfVacancyQuestion extends React.Component {
  form_name = 'wage_of_vacancy';

  constructor(props) {
    super(props);

    this.adStore = this.props.store.adStore;

    this.state = {value: null};
  }

  nextQuestion = () => {
    this.adStore.setAddAdAdditionals(this.form_name, this.state.value);
    this.props.nextQuestion();
  };

  prevQuestion = () => {
    this.adStore.setAddAdAdditionals(this.form_name, this.state.value);
    this.props.prevQuestion();
  };

  onValidValue = (value) => {
    this.setState({value});
  };

  render() {
    if (this.adStore.addAdCurrentShow !== this.form_name) return null;

    const {t} = this.props;
    const Question = Questions[`${codeTranslator(this.form_name)}Question`];

    return (
      <ScrollView keyboardShouldPersistTaps={'always'}>
        <Card>
          <Card.Title title={t(`common:adQuestions.add_form_names.${this.form_name}`)}/>
          <Card.Content>
            <Question
              defaultValue={this.adStore.addAdAdditionals[this.form_name]}
              onValidValue={this.onValidValue}
              nextQuestion={() => this.nextQuestion()}
              prevQuestion={() => this.prevQuestion()}
            />
          </Card.Content>
        </Card>
        <View flexDirection={'row'} style={{marginTop: 8}}>
          <Button flexGrow={1} style={{marginRight: 4}} mode={'outlined'} onPress={() => this.prevQuestion()}>
            {t("add_ad:label.prev_question")}
          </Button>
          <Button flexGrow={1} style={{marginLeft: 4}} mode={'contained'} onPress={() => this.nextQuestion()}>
            {t("add_ad:label.next_question")}
          </Button>
        </View>
      </ScrollView>
    );
  }
}

@withTranslation(['add_ad', 'common'])
@inject('store')
@observer
class AddAdStateQuestion extends React.Component {
  form_name = 'state';

  constructor(props) {
    super(props);

    this.adStore = this.props.store.adStore;
  }

  setData = (t) => {
    this.adStore.setAddAdAdditionals(this.form_name, t);
    this.props.nextQuestion();
  };

  render() {
    if (this.adStore.addAdCurrentShow !== this.form_name) return null;

    const {t} = this.props;
    const Question = Questions[`${codeTranslator(this.form_name)}Question`];

    return (
      <>
        <Card>
          <Card.Content>
            <Title>{t(`common:adQuestions.add_form_names.${this.form_name}`)}</Title>
            <Question
              defaultValue={this.adStore.addAdAdditionals[this.form_name]}
              onValidValue={this.setData}
            />
          </Card.Content>
        </Card>
        <View flexDirection={'row'}>
          <Button flexGrow={1} style={{marginRight: 4}} mode={'outlined'} onPress={() => this.props.prevQuestion()}>
            {t("add_ad:label.prev_question")}
          </Button>
          <Button flexGrow={1} style={{marginLeft: 4}} mode={'contained'} onPress={() => this.props.nextQuestion()}
                  disabled={!this.adStore.addAdAdditionals[this.form_name]}>
            {t("add_ad:label.next_question")}
          </Button>
        </View>
      </>
    );
  }
}

@withTranslation(['add_ad', 'common'])
@inject('store')
@observer
class AddAdGearTypeQuestion extends React.Component {
  form_name = 'gear_type';

  constructor(props) {
    super(props);

    this.adStore = this.props.store.adStore;
  }

  setData = (t) => {
    this.adStore.setAddAdAdditionals(this.form_name, t);
    this.props.nextQuestion();
  };

  render() {
    if (this.adStore.addAdCurrentShow !== this.form_name) return null;

    const {t} = this.props;
    const Question = Questions[`${codeTranslator(this.form_name)}Question`];

    return (
      <>
        <Card>
          <Card.Content>
            <Title>{t(`common:adQuestions.add_form_names.${this.form_name}`)}</Title>
            <Question
              defaultValue={this.adStore.addAdAdditionals[this.form_name]}
              onValidValue={this.setData}
            />
          </Card.Content>
        </Card>

        <View flexDirection={'row'}>
          <Button flexGrow={1} style={{marginRight: 4}} mode={'outlined'} onPress={() => this.props.prevQuestion()}>
            {t("add_ad:label.prev_question")}
          </Button>
          <Button flexGrow={1} style={{marginLeft: 4}} mode={'contained'} onPress={() => this.props.nextQuestion()}
                  disabled={!this.adStore.addAdAdditionals[this.form_name]}>
            {t("add_ad:label.next_question")}
          </Button>
        </View>
      </>
    );
  }
}

@withTranslation(['add_ad'])
@inject('store')
@observer
class AddAdMinibusMakeQuestion extends React.Component {
  form_name = 'minibus_make';

  constructor(props) {
    super(props);

    this.adStore = this.props.store.adStore;
  }

  setData = (t) => {
    this.adStore.setAddAdAdditionals(this.form_name, t);
    this.props.nextQuestion();
  };

  render() {
    if (this.adStore.addAdCurrentShow !== this.form_name) return null;

    const {t} = this.props;
    const Question = Questions[`${codeTranslator(this.form_name)}Question`];

    return (
      <>
        <Card>
          <Card.Content>
            <Title>Марка</Title>
            <Question
              defaultValue={this.adStore.addAdAdditionals[this.form_name]}
              onValidValue={this.setData}
            />
          </Card.Content>
        </Card>
        <View flexDirection={'row'}>
          <Button flexGrow={1} style={{marginRight: 4}} mode={'outlined'} onPress={() => this.props.prevQuestion()}>
            {t("add_ad:label.prev_question")}
          </Button>
          <Button flexGrow={1} style={{marginLeft: 4}} mode={'contained'} onPress={() => this.props.nextQuestion()}
                  disabled={!this.adStore.addAdAdditionals[this.form_name]}>
            {t("add_ad:label.next_question")}
          </Button>
        </View>
      </>
    );
  }
}

@withTranslation(['add_ad', 'common'])
@inject('store')
@observer
class AddAdYearOfIssueQuestion extends React.Component {
  form_name = 'year_of_issue';

  constructor(props) {
    super(props);

    this.adStore = this.props.store.adStore;
  }

  setData = (t) => {
    this.adStore.setAddAdAdditionals(this.form_name, t);
    this.props.nextQuestion();
  };

  render() {
    if (this.adStore.addAdCurrentShow !== this.form_name) return null;

    const {t} = this.props;
    const Question = Questions[`${codeTranslator(this.form_name)}Question`];

    return (
      <>
        <Card>
          <Card.Content>
            <Title>{t(`common:adQuestions.add_form_names.${this.form_name}`)}</Title>
            <Question
              defaultValue={this.adStore.addAdAdditionals[this.form_name]}
              onValidValue={this.setData}
            />
          </Card.Content>
        </Card>
        <View flexDirection={'row'}>
          <Button flexGrow={1} style={{marginRight: 4}} mode={'outlined'} onPress={() => this.props.prevQuestion()}>
            {t("add_ad:label.prev_question")}
          </Button>
          <Button flexGrow={1} style={{marginLeft: 4}} mode={'contained'} onPress={() => this.props.nextQuestion()}
                  disabled={!this.adStore.addAdAdditionals[this.form_name]}>
            {t("add_ad:label.next_question")}
          </Button>
        </View>
      </>
    );
  }
}

@withTranslation(['add_ad'])
@inject('store')
@observer
class AddAdCarMakeQuestion extends React.Component {
  form_name = 'car_make';

  constructor(props) {
    super(props);

    this.adStore = this.props.store.adStore;
  }

  setData = (t) => {
    this.adStore.setAddAdAdditionals(this.form_name, t);
    this.props.nextQuestion();
  };

  render() {
    if (this.adStore.addAdCurrentShow !== this.form_name) return null;

    const {t} = this.props;
    const Question = Questions[`${codeTranslator(this.form_name)}Question`];

    return (
      <>
        <Card>
          <Card.Content>
            <Title>Марка</Title>
            <Question
              defaultValue={this.adStore.addAdAdditionals[this.form_name]}
              onValidValue={this.setData}
            />
          </Card.Content>
        </Card>
        <View flexDirection={'row'}>
          <Button flexGrow={1} style={{marginRight: 4}} mode={'outlined'} onPress={() => this.props.prevQuestion()}>
            {t("add_ad:label.prev_question")}
          </Button>
          <Button flexGrow={1} style={{marginLeft: 4}} mode={'contained'} onPress={() => this.props.nextQuestion()}
                  disabled={!this.adStore.addAdAdditionals[this.form_name]}>
            {t("add_ad:label.next_question")}
          </Button>
        </View>
      </>
    );
  }
}

@withTranslation(['add_ad', 'common'])
@inject('store')
@observer
class AddAdVacancyQuestion extends React.Component {
  form_name = 'vacancy';

  constructor(props) {
    super(props);

    this.adStore = this.props.store.adStore;
    this.appStore = this.props.store.appStore;
  }

  setData(t) {
    this.adStore.setAddAdAdditionals(this.form_name, t);
    this.props.nextQuestion();
  }

  render() {
    if (this.adStore.addAdCurrentShow !== this.form_name) return null;

    const {t} = this.props;

    return (
      <View flex={1}>
        <Card flex={1} flexGrow={1} style={{marginBottom: 8}}>
          <Card.Content>
            <FlatList
              renderItem={({item}) => (
                <List.Item
                  onPress={() => this.setData(item.code)}
                  title={item[this.appStore.language]}
                  right={props => <List.Icon {...props} icon={item.code === this.adStore.addAdAdditionals[this.form_name] ? 'check' : 'arrow-right'} />}
                />
              )}
              ListHeaderComponent={() => <Title>{t(`common:adQuestions.add_form_names.${this.form_name}`)}</Title>}
              data={VACANCIES}
              ItemSeparatorComponent={() => <Divider/>}
            />
          </Card.Content>
        </Card>
        <View flexDirection={'row'}>
          <Button flexGrow={1} style={{marginRight: 4}} mode={'outlined'} onPress={() => this.props.prevQuestion()}>
            {t("add_ad:label.prev_question")}
          </Button>
          <Button flexGrow={1} style={{marginLeft: 4}} mode={'contained'} onPress={() => this.props.nextQuestion()}
                  disabled={!this.adStore.addAdAdditionals[this.form_name]}>
            {t("add_ad:label.next_question")}
          </Button>
        </View>
      </View>
    );
  }
}

@withTranslation(['add_ad', 'common'])
@inject('store')
@observer
class AddAdScheduleOfVacancyQuestion extends React.Component {
  form_name = 'schedule_of_vacancy';

  constructor(props) {
    super(props);

    this.adStore = this.props.store.adStore;
    this.appStore = this.props.store.appStore;
  }

  setData(t) {
    this.adStore.setAddAdAdditionals(this.form_name, t);
    this.props.nextQuestion();
  }

  render() {
    if (this.adStore.addAdCurrentShow !== this.form_name) return null;

    const {t} = this.props;

    return (
      <View flex={1}>
        <Card flex={1} flexGrow={1} style={{marginBottom: 8}}>
          <Card.Content>
            <FlatList
              renderItem={({item}) => (
                <List.Item
                  onPress={() => this.setData(item.code)}
                  title={item[this.appStore.language]}
                  right={props => <List.Icon {...props} icon={item.code === this.adStore.addAdAdditionals[this.form_name] ? 'check' : 'arrow-right'} />}
                />
              )}
              ListHeaderComponent={() => <Title>{t(`common:adQuestions.add_form_names.${this.form_name}`)}</Title>}
              data={SCHEDULES_OF_VACANCY}
              ItemSeparatorComponent={() => <Divider/>}
            />
          </Card.Content>
        </Card>
        <View flexDirection={'row'}>
          <Button flexGrow={1} style={{marginRight: 4}} mode={'outlined'} onPress={() => this.props.prevQuestion()}>
            {t("add_ad:label.prev_question")}
          </Button>
          <Button flexGrow={1} style={{marginLeft: 4}} mode={'contained'} onPress={() => this.props.nextQuestion()}
                  disabled={!this.adStore.addAdAdditionals[this.form_name]}>
            {t("add_ad:label.next_question")}
          </Button>
        </View>
      </View>
    );
  }
}

@withTranslation(['add_ad', 'common'])
@inject('store')
@observer
class AddAdExperienceOfVacancyQuestion extends React.Component {
  form_name = 'experience_of_vacancy';

  constructor(props) {
    super(props);

    this.adStore = this.props.store.adStore;
    this.appStore = this.props.store.appStore;
  }

  setData(t) {
    this.adStore.setAddAdAdditionals(this.form_name, t);
    this.props.nextQuestion();
  }

  render() {
    if (this.adStore.addAdCurrentShow !== this.form_name) return null;

    const {t} = this.props;

    return (
      <View flex={1}>
        <Card flex={1} flexGrow={1} style={{marginBottom: 8}}>
          <Card.Content>
            <FlatList
              renderItem={({item}) => (
                <List.Item
                  onPress={() => this.setData(item.code)}
                  title={item[this.appStore.language]}
                  right={props => <List.Icon {...props} icon={item.code === this.adStore.addAdAdditionals[this.form_name] ? 'check' : 'arrow-right'} />}
                />
              )}
              ListHeaderComponent={() => <Title>{t(`common:adQuestions.add_form_names.${this.form_name}`)}</Title>}
              data={EXPERIENCES_OF_VACANCY}
              ItemSeparatorComponent={() => <Divider/>}
            />
          </Card.Content>
        </Card>
        <View flexDirection={'row'}>
          <Button flexGrow={1} style={{marginRight: 4}} mode={'outlined'} onPress={() => this.props.prevQuestion()}>
            {t("add_ad:label.prev_question")}
          </Button>
          <Button flexGrow={1} style={{marginLeft: 4}} mode={'contained'} onPress={() => this.props.nextQuestion()}
                  disabled={!this.adStore.addAdAdditionals[this.form_name]}>
            {t("add_ad:label.next_question")}
          </Button>
        </View>
      </View>
    );
  }
}

@withTranslation(['add_ad'])
@inject('store')
@observer
class AddAdCarModelQuestion extends React.Component {
  form_name = 'car_model';

  constructor(props) {
    super(props);

    this.adStore = this.props.store.adStore;
  }

  setData = (t) => {
    this.adStore.setAddAdAdditionals(this.form_name, t);
    this.props.nextQuestion();
  };

  render() {
    if (this.adStore.addAdCurrentShow !== this.form_name) return null;

    const {t} = this.props;
    const Question = Questions[`${codeTranslator(this.form_name)}Question`];

    return (
      <>
        <Card>
          <Card.Content>
            <Title>Модель</Title>
            <Question
              defaultValue={this.adStore.addAdAdditionals[this.form_name]}
              onValidValue={this.setData}
              carMake={this.adStore.addAdAdditionals['car_make']}
            />
          </Card.Content>
        </Card>
        <View flexDirection={'row'}>
          <Button flexGrow={1} style={{marginRight: 4}} mode={'outlined'} onPress={() => this.props.prevQuestion()}>
            {t("add_ad:label.prev_question")}
          </Button>
          <Button flexGrow={1} style={{marginLeft: 4}} mode={'contained'} onPress={() => this.props.nextQuestion()}
                  disabled={!this.adStore.addAdAdditionals[this.form_name]}>
            {t("add_ad:label.next_question")}
          </Button>
        </View>
      </>
    );
  }
}

@withTranslation(['ad_add'])
@inject('store')
@observer
class AddAdTruckMakeQuestion extends React.Component {
  form_name = 'truck_make';

  constructor(props) {
    super(props);

    this.adStore = this.props.store.adStore;
  }

  setData = (t) => {
    this.adStore.setAddAdAdditionals(this.form_name, t);
    this.props.nextQuestion();
  };

  render() {
    if (this.adStore.addAdCurrentShow !== this.form_name) return null;

    const {t} = this.props;
    const Question = Questions[`${codeTranslator(this.form_name)}Question`];

    return (
      <>
        <Card>
          <Card.Content>
            <Title>Марка</Title>
            <Question
              defaultValue={this.adStore.addAdAdditionals[this.form_name]}
              onValidValue={this.setData}
            />
          </Card.Content>
        </Card>
        <View flexDirection={'row'}>
          <Button flexGrow={1} style={{marginRight: 4}} mode={'outlined'} onPress={() => this.props.prevQuestion()}>
            {t("add_ad:label.prev_question")}
          </Button>
          <Button flexGrow={1} style={{marginLeft: 4}} mode={'contained'} onPress={() => this.props.nextQuestion()}
                  disabled={!this.adStore.addAdAdditionals[this.form_name]}>
            {t("add_ad:label.next_question")}
          </Button>
        </View>
      </>
    );
  }
}

@withTranslation(['add_ad', 'common'])
@inject('store')
@observer
class AddAdSummaryQuestion extends React.Component {
  form_name = 'summary';

  constructor(props) {
    super(props);

    this.adStore = this.props.store.adStore;
    this.appStore = this.props.store.appStore;
  }

  setData(t) {
    this.adStore.setAddAdAdditionals(this.form_name, t);
    this.props.nextQuestion();
  }

  render() {
    if (this.adStore.addAdCurrentShow !== this.form_name) return null;

    const {t} = this.props;

    return (
      <View flex={1}>
        <Card flex={1} flexGrow={1} style={{marginBottom: 8}}>
          <Card.Content>
            <FlatList
              renderItem={({item}) => (
                <List.Item
                  onPress={() => this.setData(item.code)}
                  title={item[this.appStore.language]}
                  right={props => <List.Icon {...props} icon={item.code === this.adStore.addAdAdditionals[this.form_name] ? 'check' : 'arrow-right'} />}
                />
              )}
              ListHeaderComponent={() => <Title>{t(`common:adQuestions.add_form_names.${this.form_name}`)}</Title>}
              data={SUMMARY}
              ItemSeparatorComponent={() => <Divider/>}
            />
          </Card.Content>
        </Card>
        <View flexDirection={'row'}>
          <Button flexGrow={1} style={{marginRight: 4}} mode={'outlined'} onPress={() => this.props.prevQuestion()}>
            {t("add_ad:label.prev_question")}
          </Button>
          <Button flexGrow={1} style={{marginLeft: 4}} mode={'contained'} onPress={() => this.props.nextQuestion()}
                  disabled={!this.adStore.addAdAdditionals[this.form_name]}>
            {t("add_ad:label.next_question")}
          </Button>
        </View>
      </View>
    );
  }
}

@withTranslation(['add_ad', 'common'])
@inject('store')
@observer
class AddAdEducationOfSummaryQuestion extends React.Component {
  form_name = 'education_of_summary';

  constructor(props) {
    super(props);

    this.adStore = this.props.store.adStore;
    this.appStore = this.props.store.appStore;
  }

  setData(t) {
    this.adStore.setAddAdAdditionals(this.form_name, t);
    this.props.nextQuestion();
  }

  render() {
    if (this.adStore.addAdCurrentShow !== this.form_name) return null;

    const {t} = this.props;

    return (
      <View flex={1}>
        <Card flex={1} flexGrow={1} style={{marginBottom: 8}}>
          <Card.Content>
            <FlatList
              renderItem={({item}) => (
                <List.Item
                  onPress={() => this.setData(item.code)}
                  title={item[this.appStore.language]}
                  right={props => <List.Icon {...props} icon={item.code === this.adStore.addAdAdditionals[this.form_name] ? 'check' : 'arrow-right'} />}
                />
              )}
              ListHeaderComponent={() => <Title>{t(`common:adQuestions.add_form_names.${this.form_name}`)}</Title>}
              data={EDUCATION_OF_SUMMARY}
              ItemSeparatorComponent={() => <Divider/>}
            />
          </Card.Content>
        </Card>
        <View flexDirection={'row'}>
          <Button flexGrow={1} style={{marginRight: 4}} mode={'outlined'} onPress={() => this.props.prevQuestion()}>
            {t("add_ad:label.prev_question")}
          </Button>
          <Button flexGrow={1} style={{marginLeft: 4}} mode={'contained'} onPress={() => this.props.nextQuestion()}
                  disabled={!this.adStore.addAdAdditionals[this.form_name]}>
            {t("add_ad:label.next_question")}
          </Button>
        </View>
      </View>
    );
  }
}

@withTranslation(['add_ad', 'common'])
@inject('store')
@observer
class AddAdAgeOfSummaryQuestion extends React.Component {
  form_name = 'age_of_summary';

  constructor(props) {
    super(props);

    this.adStore = this.props.store.adStore;
    this.appStore = this.props.store.appStore;
  }

  setData(t) {
    this.adStore.setAddAdAdditionals(this.form_name, t);
    this.props.nextQuestion();
  }

  render() {
    if (this.adStore.addAdCurrentShow !== this.form_name) return null;

    const {t} = this.props;

    return (
      <View flex={1}>
        <Card flex={1} flexGrow={1} style={{marginBottom: 8}}>
          <Card.Content>
            <FlatList
              renderItem={({item}) => (
                <List.Item
                  onPress={() => this.setData(item + 18)}
                  title={`${item + 18} ${t('common:language') === 'ky' ? 'жаш' : ruAgeToWord(item + 18)}`}
                  right={props => <List.Icon {...props} icon={item.code === this.adStore.addAdAdditionals[this.form_name] ? 'check' : 'arrow-right'} />}
                />
              )}
              ListHeaderComponent={() => <Title>{t(`common:adQuestions.add_form_names.${this.form_name}`)}</Title>}
              data={Array.from(Array(82).keys())}
              ItemSeparatorComponent={() => <Divider/>}
            />
          </Card.Content>
        </Card>
        <View flexDirection={'row'}>
          <Button flexGrow={1} style={{marginRight: 4}} mode={'outlined'} onPress={() => this.props.prevQuestion()}>
            {t("add_ad:label.prev_question")}
          </Button>
          <Button flexGrow={1} style={{marginLeft: 4}} mode={'contained'} onPress={() => this.props.nextQuestion()}
                  disabled={!this.adStore.addAdAdditionals[this.form_name]}>
            {t("add_ad:label.next_question")}
          </Button>
        </View>
      </View>
    );
  }
}

@withTranslation(['common', 'add_ad'])
@inject('store')
@observer
class AddAdGenderQuestion extends React.Component {
  form_name = 'gender';

  constructor(props) {
    super(props);

    this.adStore = this.props.store.adStore;
  }

  onValidValue = (t) => {
    this.adStore.setAddAdAdditionals(this.form_name, t);
    this.props.nextQuestion();
  };

  render() {
    if (this.adStore.addAdCurrentShow !== this.form_name) return null;

    const {t} = this.props;
    const Question = Questions[`${codeTranslator(this.form_name)}Question`];

    return (
      <>
        <Card>
          <Card.Content>
            <Title>{t(`common:adQuestions.add_form_names.${this.form_name}`)}</Title>
            <Question
              defaultValue={this.adStore.addAdAdditionals[this.form_name]}
              onValidValue={this.onValidValue}
              nextQuestion={() => this.props.nextQuestion()}
              prevQuestion={() => this.props.prevQuestion()}
            />
          </Card.Content>
        </Card>
        <View flexDirection={'row'}>
          <Button flexGrow={1} style={{marginRight: 4}} mode={'outlined'} onPress={() => this.props.prevQuestion()}>
            {t("add_ad:label.prev_question")}
          </Button>
          <Button flexGrow={1} style={{marginLeft: 4}} mode={'contained'} onPress={() => this.props.nextQuestion()}
                  disabled={!this.adStore.addAdAdditionals[this.form_name]}>
            {t("add_ad:label.next_question")}
          </Button>
        </View>
      </>
    );
  }
}

@withTranslation(['common', 'add_ad'])
@inject('store')
@observer
class AddAdPhoneMakeQuestion extends React.Component {
  form_name = 'phone_make';

  constructor(props) {
    super(props);

    this.adStore = this.props.store.adStore;
  }

  setData = (t) => {
    this.adStore.setAddAdAdditionals(this.form_name, t);
    this.props.nextQuestion();
  };

  render() {
    if (this.adStore.addAdCurrentShow !== this.form_name) return null;

    const {t} = this.props;

    return (
      <View flex={1}>
        <Card flex={1} flexGrow={1} style={{marginBottom: 8}}>
          <Card.Content>
            <FlatList
              renderItem={({item}) => (
                <List.Item
                  onPress={() => this.setData(item)}
                  title={item || t(`adQuestions.${this.form_name}.another`)}
                  right={props => <List.Icon {...props} icon={item === this.adStore.addAdAdditionals[this.form_name] ? 'check' : 'arrow-right'} />}
                />
              )}
              ListHeaderComponent={() => <Title>{t(`common:adQuestions.add_form_names.${this.form_name}`)}</Title>}
              data={PHONE_MAKES}
              ItemSeparatorComponent={() => <Divider/>}
            />
          </Card.Content>
        </Card>
        <View flexDirection={'row'}>
          <Button flexGrow={1} style={{marginRight: 4}} mode={'outlined'} onPress={() => this.props.prevQuestion()}>
            {t("add_ad:label.prev_question")}
          </Button>
          <Button flexGrow={1} style={{marginLeft: 4}} mode={'contained'} onPress={() => this.props.nextQuestion()}
                  disabled={!this.adStore.addAdAdditionals[this.form_name]}>
            {t("add_ad:label.next_question")}
          </Button>
        </View>
      </View>
    );
  }
}

@withTranslation(['common', 'add_ad'])
@inject('store')
@observer
class AddAdComputerMakeQuestion extends React.Component {
  form_name = 'computer_make';

  constructor(props) {
    super(props);

    this.adStore = this.props.store.adStore;
  }

  setData = (t) => {
    this.adStore.setAddAdAdditionals(this.form_name, t);
    this.props.nextQuestion();
  };

  render() {
    if (this.adStore.addAdCurrentShow !== this.form_name) return null;

    const {t} = this.props;

    return (
      <View flex={1}>
        <Card flex={1} flexGrow={1} style={{marginBottom: 8}}>
          <Card.Content>
            <FlatList
              renderItem={({item}) => (
                <List.Item
                  onPress={() => this.setData(item)}
                  title={item || t(`adQuestions.${this.form_name}.another`)}
                  right={props => <List.Icon {...props} icon={item === this.adStore.addAdAdditionals[this.form_name] ? 'check' : 'arrow-right'} />}
                />
              )}
              ListHeaderComponent={() => <Title>{t(`common:adQuestions.add_form_names.${this.form_name}`)}</Title>}
              data={COMPUTER_MAKES}
              ItemSeparatorComponent={() => <Divider/>}
            />
          </Card.Content>
        </Card>
        <View flexDirection={'row'}>
          <Button flexGrow={1} style={{marginRight: 4}} mode={'outlined'} onPress={() => this.props.prevQuestion()}>
            {t("add_ad:label.prev_question")}
          </Button>
          <Button flexGrow={1} style={{marginLeft: 4}} mode={'contained'} onPress={() => this.props.nextQuestion()}
                  disabled={!this.adStore.addAdAdditionals[this.form_name]}>
            {t("add_ad:label.next_question")}
          </Button>
        </View>
      </View>
    );
  }
}

export default {
  AddAdPriceQuestion,
  AddAdRoomsQuestion,
  AddAdFurnitureQuestion,
  AddAdPlaceQuestion,
  AddAdAgencyOrOwnerQuestion,
  AddAdAreaHundredthsQuestion,
  AddAdAreaSquareMeterQuestion,
  AddAdActionQuestion,
  AddAdStateQuestion,
  AddAdCarMakeQuestion,
  AddAdCarModelQuestion,
  AddAdYearOfIssueQuestion,
  AddAdGearTypeQuestion,
  AddAdFuelQuestion,
  AddAdMinibusMakeQuestion,
  AddAdTruckMakeQuestion,
  AddAdWageOfVacancyQuestion,
  AddAdVacancyQuestion,
  AddAdScheduleOfVacancyQuestion,
  AddAdExperienceOfVacancyQuestion,
  AddAdSummaryQuestion,
  AddAdEducationOfSummaryQuestion,
  AddAdGenderQuestion,
  AddAdPhoneMakeQuestion,
  AddAdComputerMakeQuestion,
  AddAdAgeOfSummaryQuestion,
};
