import React, {Component} from 'react';
import {inject, observer} from 'mobx-react';
import {View, FlatList} from 'react-native';
import {Button, Subheading, List, Divider} from 'react-native-paper';
import {action, computed, observable} from 'mobx';
import storage from '../../utils/storage';
import {requester} from '../../utils';
import {withTranslation} from 'react-i18next';

@withTranslation(['common', 'add_ad'])
@inject('store')
@observer
class Categories extends Component {
  @observable loading = false;
  @observable items = observable.array([]);
  @observable history = [];
  @observable index = 0;

  constructor(props) {
    super(props);

    this.store = this.props.store.appStore;
    this.adStore = this.props.store.adStore;

    this.history = [this.items];

    this.props.getContext(this);
  }

  @action fetchItems = async () => {
    if (this.loading) {
      return;
    }
    this.loading = true;
    try {
      const data = await requester.get('/category/get');
      this.items = data.data;
      this.history = [this.items];
      await storage.set('categories', data.data);
    } catch (e) {
      console.log(e);
      this.items = [];
    } finally {
      this.loading = false;
    }
  };

  async componentDidMount() {
    await this.fetchItems();
  }

  @action prevQuestionHandler = () => {
    if (this.index > 0) this.index--;
  };

  @action nextQuestionHandler = () => {
    if (this.history.length <= this.index + 1) return;
    this.index++;
  };

  @action selectCategory = c => {
    if (c.children.length > 0) {
      if (this.history[this.index + 1]) {
        this.history[this.index + 1] = c.children;
      } else {
        this.history = [...this.history, c.children];
      }
      this.index++;
    } else {
      this.adStore.setAddAdCategory(c.id);
      this.adStore.setAddAdForms(c.forms);
      this.adStore.setAddAdCurrentShow(c.forms[0]);
    }
    return false;
  };

  @computed get prevQuestionIsDisabled() {
    return this.index === 0 || this.history.length === 0;
  }

  @computed get nextQuestionIsDisabled() {
    return this.history.length <= this.index + 1;
  }

  render() {
    const {t} = this.props;
    return (
      <>
        <Subheading
          style={{
            textAlign: 'center',
          }}>
          {t('select_a_category')}
        </Subheading>
        <FlatList
          refreshing={this.loading}
          onRefresh={() => false}
          data={this.history[this.index]}
          style={{
            marginVertical: 8,
          }}
          flexGrow={1}
          ItemSeparatorComponent={Divider}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item, key}) => (
            <List.Item
              key={key}
              onPress={() => this.selectCategory(item)}
              title={item[`title_${this.store.language}`]}
            />
          )}
        />
        <View flexDirection={'row'}>
          <Button
            disabled={this.prevQuestionIsDisabled}
            onPress={this.prevQuestionHandler}
            flexGrow={1}
            style={{
              marginRight: 4,
            }}
            mode={'outlined'}>
            {t('add_ad:label.prev_question')}
          </Button>
          <Button
            disabled={this.nextQuestionIsDisabled || this.loading}
            onPress={this.nextQuestionHandler}
            flexGrow={1}
            style={{
              marginLeft: 4,
            }}
            mode={'contained'}>
            {t('add_ad:label.next_question')}
          </Button>
        </View>
      </>
    );
  }
}

export default Categories;
