import React from "react";
import ScreenWrapper from "../../ui/ScreenWrapper";
import {inject, observer} from "mobx-react";
import {Button, Card, Text, TextInput} from 'react-native-paper';
import {withTranslation} from "react-i18next";
import {View, ScrollView, BackHandler} from "react-native";
import {action, observable, observe} from "mobx";
import AdQuestionsComponent from "../AddAdQuestionsComponent";
import {codeTranslator} from "../../utils";
import DefaultQuestionsComponent from "./DefaultQuestionsComponent";

@withTranslation('add_ad')
@inject('store') @observer
class MainComponent extends React.Component {
  title_ref = React.createRef();
  description_ref = React.createRef();
  categories_ref = React.createRef();
  back_handler = null;
  @observable components = observable.array([]);

  constructor(props) {
    super(props);

    this.store = this.props.store.appStore;
    this.adStore = this.props.store.adStore;

    observe(this.adStore, "addAdForms", this.onChangeForms);
    observe(this.adStore, "addAdCurrentQuestion", this.onChangeCurrentQuestion);
  }

  componentDidMount() {
    this.props.navigation.addListener('focus', () => {
      this.back_handler = BackHandler.addEventListener('hardwareBackPress', this.backAction);
    });

    this.props.navigation.addListener('blur', () => {
      if (this.back_handler) this.back_handler.remove();
    });

    this.renderForms();
  }

  @action renderForms = () => {
    const props = {
      nextQuestion: () => this.nextQuestionHandler(),
      prevQuestion: () => this.prevQuestionHandler(),
    };
    this.components = this.adStore.addAdForms.map((f, i) => {
      const Component = AdQuestionsComponent[`AddAd${codeTranslator(f)}Question`];
      if (!Component) return null;
      return <Component key={i} {...props} />;
    });
  };

  backAction = () => {
    if (this.adStore.addAdCategory < 1) this.categories_ref.prevQuestionHandler();
    else this.prevQuestionHandler();
    return true;
  };

  componentWillUnmount() {
    if (this.back_handler) this.back_handler.remove();
  }

  onChangeCurrentQuestion = (nv) => {
    let {newValue} = nv;
    this.adStore.setAddAdCurrentShow(this.adStore.addAdForms[newValue]);
  };

  @action onChangeForms = (nv) => {
    const props = {
      nextQuestion: () => this.nextQuestionHandler(),
      prevQuestion: () => this.prevQuestionHandler(),
    };
    this.components = nv.newValue.map((f, i) => {
      const Component = AdQuestionsComponent[`AddAd${codeTranslator(f)}Question`];
      if (!Component) return null;
      return <Component key={i} {...props} />;
    });
  };

  nextQuestionHandler = () => {
    this.adStore.setAddAdCurrentQuestion(this.adStore.addAdCurrentQuestion + 1);
  };

  prevQuestionHandler = () => {
    if (this.adStore.addAdCurrentQuestion === -1) {
      this.adStore.clearAddAdForm();
    } else {
      this.adStore.setAddAdCurrentQuestion(this.adStore.addAdCurrentQuestion - 1);
    }
  };

  render() {
    const {t} = this.props;

    return (
      <ScreenWrapper style={{justifyContent: 'space-between', alignItems: 'stretch'}} padding>
        {this.adStore.addAdCurrentQuestion === -1 && (
          <ScrollView keyboardShouldPersistTaps={'always'}>
            <View>
              <Card>
                <Card.Title title={`${t('label.title')} (${this.adStore.addAdTitle.length}/70)`}/>
                <Card.Content>
                  <TextInput
                    ref={r => this.title_ref = r}
                    returnKeyType={'next'}
                    mode={'outlined'}
                    blurOnSubmit={true}
                    value={this.adStore.addAdTitle}
                    dense={true}
                    autoFocus={true}
                    onSubmitEditing={() => this.description_ref.focus()}
                    onChangeText={text => this.adStore.setAddAdTitle(text.substr(0, 70))}
                    error={this.adStore.addAdTitle.length >= 70}
                  />
                  <Text style={{marginTop: 6, color: '#888888'}}>
                    {t('Example_s')}
                  </Text>
                </Card.Content>
              </Card>

              <Card style={{marginTop: 8}}>
                <Card.Title title={`${t('label.description')} (${this.adStore.addAdDescription.length}/1300)`}/>
                <Card.Content>
                  <TextInput
                    ref={r => this.description_ref = r}
                    returnKeyType={'done'}
                    scrollEnabled={true}
                    multiline={true}
                    numberOfLines={5}
                    mode={'outlined'}
                    blurOnSubmit={true}
                    value={this.adStore.addAdDescription}
                    dense={true}
                    error={this.adStore.addAdDescription.length >= 1300}
                    onChangeText={text => this.adStore.setAddAdDescription(text.substr(0, 1300))}
                  />
                  <Text style={{marginTop: 6, color: '#888888'}}>
                    {t('Example')}
                  </Text>
                </Card.Content>
              </Card>
            </View>

            <View flexDirection={'row'} style={{marginTop: 8}}>
              <Button onPress={this.prevQuestionHandler}
                      flexGrow={1} style={{marginRight: 4}} mode={'outlined'}>{t("label.prev_question")}</Button>
              <Button disabled={this.adStore.addAdTitle.length < 1} onPress={this.nextQuestionHandler} flexGrow={1}
                      style={{marginLeft: 4}} mode={'contained'}>{t("label.next_question")}</Button>
            </View>
          </ScrollView>
        )}

        <DefaultQuestionsComponent
          question={this.adStore.addAdCurrentQuestion - this.adStore.addAdForms.length}
          t={t}
          userContacts={this.store.user.phones}
          nextQuestion={() => this.nextQuestionHandler()}
          prevQuestion={() => this.prevQuestionHandler()}/>
      </ScreenWrapper>
    );
  }
}

export default MainComponent;
