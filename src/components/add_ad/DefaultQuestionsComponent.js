import React, {Component} from 'react';
import {inject, observer} from 'mobx-react';
import {buildArrayForBlob, codeTranslator, daysToWord, services} from '../../utils';
import {additionalQuestionRenderer} from '../../utils/generators';
import {
  Button,
  Caption,
  Card,
  Title,
  Divider,
  Text,
  Colors,
  Subheading,
  TextInput,
  Checkbox,
  Modal,
} from 'react-native-paper';
import {
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  FlatList,
} from 'react-native';
import ImagePicker from '../../ui/ImagePicker';
import RootNavigation from '../../utils/RootNavigation';
import Ad from '../../models/Ad';
import PhoneNumber from '../../utils/PhoneNumber';
import RNFetchBlob from 'rn-fetch-blob';
import {API_URL} from '../../utils/settings';
import {withTranslation} from 'react-i18next';
import storage from "../../utils/storage";
import {CODE} from "../../screens/modals/LiftUpModalScreen";
import Questions from "../AdQuestions";

@inject('store')
@observer
class AddAdPreview extends Component {
  constructor(props) {
    super(props);

    this.adStore = this.props.store.adStore;
    this.store = this.props.store.appStore;

    this.initialState = {
      sending: false,
    };

    this.state = this.initialState;
  }

  send = () => {
    const store = this.adStore;
    if (this.state.sending) {
      return;
    }
    const {t} = this.props;

    let params = [
      {name: 'category', data: store.addAdCategory > 0 ? store.addAdCategory + '' : ''},
      {name: 'description', data: store.addAdDescription},
      {name: 'title', data: store.addAdTitle},
    ];

    store.addAdImages.map(async (p, i) => {
      params.push({
        name: `images[${i}]`,
        filename: `image_${i}.jpg`,
        data: RNFetchBlob.wrap(p.path),
      });
    });

    buildArrayForBlob(store.addAdAdditionals, [], 'additionals').map(v => {
      params.push(v);
    });

    if (store.addAdContacts && this.store.is_agent) {
      params.push({name: 'phone_1', data: store.addAdContacts[0].value + ''});
      params.push({name: 'phone_2', data: store.addAdContacts[1].value + ''});
      params.push({name: 'phone_3', data: store.addAdContacts[2].value + ''});
      params.push({
        name: 'whatsapp_1',
        data: store.addAdContacts[0].whatsapp ? 'true' : 'false',
      });
      params.push({
        name: 'whatsapp_2',
        data: store.addAdContacts[1].whatsapp ? 'true' : 'false',
      });
      params.push({
        name: 'whatsapp_3',
        data: store.addAdContacts[2].whatsapp ? 'true' : 'false',
      });
    }

    this.setState({sending: true});
    RNFetchBlob.fetch(
      'POST',
      `${API_URL()}/ad/create`,
      {
        Authorization: `Basic ${this.store.token}`,
        'Content-Type': 'multipart/form-data',
        Accept: 'application/json',
      },
      params,
    )
      .then(result => {
        console.log(result);
        result = JSON.parse(result.data);
        if (result.status === 'success') {
          this.store.showSuccess(t('successfully_sent'));
          this.adStore.fetchMyItems();

          RootNavigation.navigate('Home', {
            screen: 'AdView',
            params: {item: new Ad(result.data)},
          });

          setTimeout(async () => {
            if ((await storage.get('skipped_lift_up_modal', 0)) < CODE) {
              RootNavigation.navigate('LiftUpModal');
            }
          }, 1000);

          // TODO: платный услугалар иштеп баштаганда ондош керек

          setTimeout(() => {
            this.adStore.clearAddAdForm();
          }, 500);
        } else {
          console.log(result);
          this.store.showError(JSON.stringify(result));
        }
      })
      .catch(error => {
        console.error(error);
        alert(error.message);
      })
      .finally(() => {
        this.setState({sending: false});
      });
  };

  render() {
    if (this.props.show === false) {
      return null;
    }

    const {t} = this.props;
    const data = this.adStore.addAdForms.map(value =>
      additionalQuestionRenderer({
        name: value,
        value: this.adStore.addAdAdditionals[value],
      }),
    );

    return (
      <>
        {/*<Modal.Body>*/}
        {/*  <div className="h4 mb-3">{t("_.Preliminary")}</div>*/}
        {/*  <ListGroup variant="flush">*/}
        {/*    {data.map((value, index) => <ListGroup.Item key={index}>{value}</ListGroup.Item>)}*/}
        {/*  </ListGroup>*/}
        {/*  <div className="text-muted text-center mt-3">{t("_.alright")}</div>*/}
        {/*</Modal.Body>*/}
        <FlatList
          ListHeaderComponent={(
            <>
              <Subheading style={{textAlign: 'center', marginVertical: 8}}>{t('label.preview')}</Subheading>
              <Text>{this.adStore.addAdTitle}</Text>
            </>
          )}
          ListFooterComponent={(
            <Subheading
              style={{
                color: Colors.grey600,
                textAlign: 'center',
                marginVertical: 8,
              }}>
              {t('label.fine')}
            </Subheading>
          )}
          ItemSeparatorComponent={() => <Divider/>}
          data={data}
          renderItem={({item}) => (
            <Text style={{marginVertical: 8}}>
              {item.title && (
                <Text style={{color: Colors.grey600}}>{item.title}: </Text>
              )}
              {item.content}
            </Text>
          )}
          keyExtractor={(item, index) => index.toString()}
        />
        <View style={{marginTop: 8}} flexDirection={'row'}>
          <Button
            disabled={this.state.sending}
            flexGrow={1}
            style={{marginRight: 4}}
            mode={'outlined'}
            onPress={() => this.props.prevQuestion()}>
            {t('add_ad:label.prev_question')}
          </Button>
          <Button disabled={this.state.sending} flexGrow={1} style={{marginLeft: 4}} mode={'contained'}
                  onPress={() => this.send()} loading={this.state.sending}>
            {t('add_ad:label.send')}
          </Button>
        </View>
      </>
    );
  }
}

@inject('store')
class AddAdServices extends Component {
  constructor(props) {
    super(props);

    this.initialState = {
      service: services.free,
      days: 1,
    };

    this.state = this.initialState;
    this.adStore = this.props.store.adStore;
    this.store = this.props.store.appStore;

    this.serviceChangeHandler = this.serviceChangeHandler.bind(this);
    this.nextQuestion = this.nextQuestion.bind(this);
  }

  serviceChangeHandler(service) {
    this.setState({service});
  }

  nextQuestion() {
    this.adStore.setAddAdService(this.state.service, this.state.days);
    this.props.nextQuestion();
  }

  button(label, name, variant) {
    return (
      <Button
        onClick={() => this.serviceChangeHandler(services[name])}
        variant={this.state.service.name === name ? 'light' : variant}>
        {label}
      </Button>
    );
  }

  cancel() {
    setTimeout(() => {
      this.adStore.clearAddAdForm();
    }, 500);
  }

  render() {
    if (this.props.show === false) {
      return null;
    }

    const {t} = this.props;
    const {service, days} = this.state;

    return (
      <React.Fragment>
        <Modal.Body>
          <div className="h4">Выберите услугу</div>
          <div className={'d-flex justify-content-between'}>
            {Object.keys(services).map(name =>
              this.button(services[name].title, name, services[name].style),
            )}
          </div>
          <div className={'mt-2'}>
            <div>
              Вы выбрали услугу: <strong> {service.title} </strong>
            </div>
            <div>
              Плата за 1 день:
              <strong>
                {service.price} сом
              </strong>
            </div>
            <Alert className={'mt-2'} variant={service.style}>
              {days} {daysToWord(days, 'ru')}(включая сегодня) -
              <strong>
                {service.price * days} сом
              </strong>
            </Alert>
            {service.price > 0 && (
              <div className={'p-3'}>
                <InputRange
                  maxValue={30}
                  minValue={1}
                  value={days}
                  onChange={value => this.setState({days: value})}
                />
              </div>
            )}
          </div>
        </Modal.Body>
        <Modal.Footer className="d-flex justify-content-between">
          <Button
            size="sm"
            variant="secondary"
            onClick={this.cancel.bind(this)}>
            {'Отмена'}
          </Button>
          <Button size="sm" variant="primary" onClick={this.cancel.bind(this)}>
            {'Подключить'}
          </Button>
        </Modal.Footer>
      </React.Fragment>
    );
  }
}

@inject('store')
@observer
class AddAdPhotos extends Component {
  image_picker = React.createRef();

  constructor(props) {
    super(props);

    this.store = this.props.store.adStore;
  }

  openImagesPicker = () => {
    if (this.store.addAdImages.length >= 10) {
      return;
    }

    const {t} = this.props;

    const options = {
      title: t('add_ad:label.select_image'),
      cancelButtonTitle: t('add_ad:cancel'),
    };

    // ImagePicker.showImagePicker(options, response => {
    //   if (response.didCancel) {
    //     console.log('User cancelled image picker');
    //   } else if (response.error) {
    //     console.log('ImagePicker Error: ', response.error);
    //   } else {
    //     ImageResizer.createResizedImage(response.uri, 500, 300, 'JPEG', 80, 0)
    //       .then(res => {
    //         this.store.setAddAdImages([...this.store.addAdImages, res]);
    //       })
    //       .catch(err => {
    //         console.log('ImageResizer Error: ', err);
    //       });
    //   }
    // });
  };

  removePhotoByIndexHandler = index => {
    let photos = [...this.store.addAdImages];
    photos.splice(index, 1);
    this.store.setAddAdImages(photos);
  };

  render() {
    if (this.props.show === false) {
      return null;
    }

    const {t} = this.props;

    return (
      <ScrollView>
        <Card>
          <Card.Content>
            <Title>
              {t('add_ad:label.images')}({this.store.addAdImages.length}/ 10)
            </Title>
            <ImagePicker onSelectImage={(v) => {
              this.store.setAddAdImages([...this.store.addAdImages, v]);
            }} onSelectImages={(i) => {
              i.map(v => {
                if (this.store.addAdImages.length < 10) this.store.setAddAdImages([...this.store.addAdImages, v]);
              });
            }} getContext={r => this.image_picker = r}/>
            {this.store.addAdImages.length > 0 && (
              <>
                <View
                  justifyContent={'space-around'}
                  flexDirection={'row'}
                  flexWrap={'wrap'}>
                  {this.store.addAdImages.map((v, key) => (
                    <TouchableOpacity
                      style={{width: 130, height: 100, margin: 8}}
                      onPress={() => this.removePhotoByIndexHandler(key)}
                      key={key}>
                      <Image
                        style={{width: '100%', height: '100%'}}
                        source={{uri: v.path}}
                      />
                    </TouchableOpacity>
                  ))}
                </View>
                <Caption>{t('Click_on_the_photo_to_delete')}</Caption>
              </>
            )}
            <Button
              onPress={() => this.image_picker.open()}
              disabled={this.store.addAdImages.length >= 10}
              mode={'outlined'}>
              {t('add_ad:label.select_image')}
            </Button>
          </Card.Content>
        </Card>
        <View style={{marginTop: 8}} flexDirection={'row'}>
          <Button
            flexGrow={1}
            style={{marginRight: 4}}
            mode={'outlined'}
            onPress={() => this.props.prevQuestion()}>
            {t('add_ad:label.prev_question')}
          </Button>
          <Button
            flexGrow={1}
            style={{marginLeft: 4}}
            mode={'contained'}
            onPress={() => this.props.nextQuestion()}>
            {t('add_ad:label.next_question')}
          </Button>
        </View>
      </ScrollView>
    );
  }
}

@withTranslation()
export class PhoneRenderer extends React.Component {
  defaultProps = {
    disableFocus: false,
  };

  render() {
    const {props} = this;
    const {t} = this.props;

    return (
      <>
        <TextInput
          label={`Телефон номер #${props.code}`}
          autoCompleteType={'tel'}
          keyboardType={'phone-pad'}
          returnKeyType={'next'}
          mode={'outlined'}
          blurOnSubmit={true}
          value={props.value}
          dense={true}
          dataDetectorTypes={'phoneNumber'}
          autoFocus={!this.props.disableFocus && props.code === 1}
          textContentType={'telephoneNumber'}
          onChangeText={text => props.onChange(props.code, text)}
        />
        <View flex={1} alignItems={'center'} flexDirection={'row'}>
          <Checkbox.Android
            onPress={() => props.onChangeWhatsapp(`whatsapp_${props.code}`)}
            status={props.checked ? 'checked' : 'unchecked'}
          />
          <Text onPress={() => props.onChangeWhatsapp(`whatsapp_${props.code}`)}>
            WhatsApp {t('there_is')}
          </Text>
        </View>
      </>
    );
  }
}

@withTranslation() @inject('store')
class AddAdContacts extends Component {
  constructor(props) {
    super(props);

    this.phones = [
      this.props.userContacts[0],
      this.props.userContacts[1],
      this.props.userContacts[2],
    ];

    this.initialState = {
      phone_1: '',
      phone_2: '',
      phone_3: '',
      p_object_1: new PhoneNumber(),
      p_object_2: new PhoneNumber(),
      p_object_3: new PhoneNumber(),
      whatsapp_1: false,
      whatsapp_2: false,
      whatsapp_3: false,
      can_next: false,
    };

    this.state = this.initialState;
    this.adStore = this.props.store.adStore;
  }

  useUserContacts = () => {
    this.setState(prev => {
      return {
        phone_1: this.phones[0].phone,
        phone_2: this.phones[1].phone,
        phone_3: this.phones[2].phone,
        whatsapp_1: this.phones[0].whatsapp,
        whatsapp_2: this.phones[1].whatsapp,
        whatsapp_3: this.phones[2].whatsapp,
        p_object_1: prev.p_object_1.parseFromString(this.phones[0].phone),
        p_object_2: prev.p_object_2.parseFromString(this.phones[1].phone),
        p_object_3: prev.p_object_3.parseFromString(this.phones[2].phone),
      };
    });
  };

  onChangePhone = (n, e) => {
    this.setState(prev => ({
      [`phone_${n}`]: e,
      [`p_object_${n}`]: prev[`p_object_${n}`].parseFromString(e),
    }));
  };

  onChangeWhatsapp = n => {
    this.setState(prev => ({
      [n]: !prev[n],
    }));
  };

  nextQuestion = () => {
    this.props.nextQuestion();
    this.adStore.setAddAdContacts([
      {
        value: (this.state.p_object_1.getE164Format() || '').slice(1),
        whatsapp: this.state.whatsapp_1,
      },
      {
        value: (this.state.p_object_2.getE164Format() || '').slice(1),
        whatsapp: this.state.whatsapp_2,
      },
      {
        value: (this.state.p_object_3.getE164Format() || '').slice(1),
        whatsapp: this.state.whatsapp_3,
      },
    ]);
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      prevState.phone_1 !== this.state.phone_1 ||
      prevState.phone_2 !== this.state.phone_2 ||
      prevState.phone_3 !== this.state.phone_3
    ) {
      this.setState({
        can_next:
          this.state.p_object_1.isValid() ||
          this.state.p_object_2.isValid() ||
          this.state.p_object_3.isValid(),
      });
    }
  }

  render() {
    if (this.props.show === false) {
      return null;
    }

    const {t} = this.props;

    return (
      <ScrollView keyboardShouldPersistTaps={'always'}>
        <Card>
          <Card.Content>
            <Title style={{marginBottom: 8}}>{t('Contacts')}</Title>
            <PhoneRenderer
              onChangeWhatsapp={this.onChangeWhatsapp}
              checked={this.state.whatsapp_1}
              t={t}
              code={1}
              value={this.state.phone_1}
              onChange={this.onChangePhone}
            />
            <PhoneRenderer
              onChangeWhatsapp={this.onChangeWhatsapp}
              checked={this.state.whatsapp_2}
              t={t}
              code={2}
              value={this.state.phone_2}
              onChange={this.onChangePhone}
            />
            <PhoneRenderer
              onChangeWhatsapp={this.onChangeWhatsapp}
              checked={this.state.whatsapp_3}
              t={t}
              code={3}
              value={this.state.phone_3}
              onChange={this.onChangePhone}
            />
            {(this.phones[0].exists ||
              this.phones[1].exists ||
              this.phones[2].exists) && (
              <Button
                style={{marginTop: 8}}
                labelStyle={{fontSize: 12}}
                onPress={() => this.useUserContacts()}
                mode={'outlined'}>
                {t('Use_my_contact_details')}
              </Button>
            )}
          </Card.Content>
        </Card>
        <View style={{marginTop: 8}} flexDirection={'row'}>
          <Button
            flexGrow={1}
            style={{marginRight: 4}}
            mode={'outlined'}
            onPress={() => this.props.prevQuestion()}>
            {t('add_ad:label.prev_question')}
          </Button>
          <Button
            flexGrow={1}
            disabled={!this.state.can_next}
            style={{marginLeft: 4}}
            mode={'contained'}
            onPress={() => this.nextQuestion()}>
            {t('add_ad:label.next_question')}
          </Button>
        </View>
      </ScrollView>
    );
  }
}

@withTranslation(['common', 'add_ad'])
@inject('store')
@observer
class AddAdPlaceQuestion extends React.Component {
  form_name = 'place';

  constructor(props) {
    super(props);

    this.adStore = this.props.store.adStore;
  }

  onValidValue = (value) => {
    this.adStore.setAddAdAdditionals(this.form_name, value);
    this.props.nextQuestion();
  };

  render() {
    if (!this.props.show) return null;

    const {t} = this.props;
    const Question = Questions[`${codeTranslator(this.form_name)}Question`];

    return (
      <ScrollView keyboardShouldPersistTaps={'always'}>
        <Card>
          <Card.Title title={t(`common:adQuestions.add_form_names.${this.form_name}`)}/>
          <Card.Content>
            <Question
              defaultValue={this.adStore.addAdAdditionals[this.form_name]}
              onValidValue={this.onValidValue}
              nextQuestion={() => this.props.nextQuestion()}
              prevQuestion={() => this.props.prevQuestion()}
            />
          </Card.Content>
        </Card>
        <View flexDirection={'row'}>
          <Button flexGrow={1} style={{marginRight: 4}} mode={'outlined'} onPress={() => this.props.prevQuestion()}>
            {t("add_ad:label.prev_question")}
          </Button>
          <Button flexGrow={1} style={{marginLeft: 4}} mode={'contained'} onPress={() => this.props.nextQuestion()}
                  disabled={!this.adStore.addAdAdditionals[this.form_name]}>
            {t("add_ad:label.next_question")}
          </Button>
        </View>
      </ScrollView>
    );
  }
}

@inject('store')
class DefaultQuestionsComponent extends Component {
  constructor(props) {
    super(props);

    this.appStore = this.props.store.appStore;
  }

  render() {
    const {t} = this.props;

    return (
      <>
        <AddAdPlaceQuestion
          show={this.props.question === 0}
          nextQuestion={this.props.nextQuestion}
          prevQuestion={this.props.prevQuestion}
        />
        <AddAdPhotos
          show={this.props.question === 1}
          t={t}
          nextQuestion={this.props.nextQuestion}
          prevQuestion={this.props.prevQuestion}
        />
        <AddAdContacts
          userContacts={this.props.userContacts}
          show={this.props.question === 2 && this.appStore.is_agent}
          t={t}
          nextQuestion={this.props.nextQuestion}
          prevQuestion={this.props.prevQuestion}
        />
        <AddAdPreview
          show={this.props.question + (this.appStore.is_agent ? 0 : 1) === 3}
          t={t}
          nextQuestion={this.props.nextQuestion}
          prevQuestion={this.props.prevQuestion}
        />
        {/*<AddAdServices*/}
        {/*  show={(this.props.question + (this.appStore.is_agent ? 0 : 1)) === 3}*/}
        {/*  t={t} nextQuestion={this.props.nextQuestion}*/}
        {/*  prevQuestion={this.props.prevQuestion}/>*/}
      </>
    );
  }
}

export default DefaultQuestionsComponent;
