import React from 'react';
import {Portal, Dialog, RadioButton, Text} from "react-native-paper";
import {TouchableOpacity} from "react-native";
import {inject, observer} from "mobx-react";
import {withTranslation} from "react-i18next";

@withTranslation() @inject('store') @observer
class PlaceModal extends React.Component {
  constructor(props) {
    super(props);

    this.store = this.props.store;
    this.appStore = this.store.appStore;
    this.placeStore = this.store.placeStore;
  }

  render() {
    const {t} = this.props;

    return (
      <Portal>
        <Dialog
          visible={!this.appStore.show_welcome_modal && this.appStore.show_place_modal}>
          <Dialog.Title>{t('Your_town')}</Dialog.Title>
          <Dialog.Content>
            <RadioButton.Group>
              {this.placeStore.items.map((v, k) => (
                <TouchableOpacity
                  key={k}
                  style={{alignItems: 'center', flexDirection: 'row'}}
                  onPress={() => this.appStore.setPlace(v, false)}>
                  <RadioButton value={v} onPress={() => this.appStore.setPlace(v, false)}/>
                  <Text>{v[`title_${t('language')}`]}</Text>
                </TouchableOpacity>
              ))}
            </RadioButton.Group>
          </Dialog.Content>
        </Dialog>
      </Portal>
    );
  }
}

export default PlaceModal;
