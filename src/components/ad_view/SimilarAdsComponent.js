import React from 'react';
import {withTranslation} from "react-i18next";
import {inject, observer} from "mobx-react";
import {action, observable} from "mobx";
import {ActivityIndicator, Card, Colors, Subheading} from "react-native-paper";
import {FlatList, View} from "react-native";
import AdItem from "../../ui/AdItem";
import Ad from "../../models/Ad";

@withTranslation('ad_view') @inject('store') @observer
class SimilarAdsComponent extends React.Component {
  @observable item = null;
  @observable error = null;
  @observable loading = false;

  constructor(props) {
    super(props);

    this.adStore = this.props.store.adStore;
    this.store = this.props.store.appStore;
  }

  @action fetchById = async (id) => {
    if (this.loading) return;
    this.loading = true;
    try {
      this.item = (await this.adStore.fetchByCategory(id, 1, 10)).items.map(v => new Ad(v));
    } catch (e) {
      this.error = e;
    } finally {
      this.loading = false;
    }
  };

  async componentDidMount(): void {
    await this.fetchById(this.props.category.id);
  }

  render() {
    if (!this.props.category.id || this.error || (this.item && this.item.length === 0)) return null;

    if (this.loading) return (
      <Card style={{marginTop: 8, marginHorizontal: 8}}>
        <Card.Content>
          <ActivityIndicator/>
        </Card.Content>
      </Card>
    );

    const {t} = this.props;

    return (
      <>
        <Subheading style={{marginTop: 8, marginLeft: 8, color: Colors.grey600, fontWeight: 'bold'}}>{t('similar_ads')}</Subheading>
        <FlatList
          horizontal={true}
          renderItem={({item}) => (
            <View style={{flex: 1}}>
              <AdItem inline={true} data={item}/>
            </View>
          )}
          style={{marginTop: 8, marginHorizontal: 8}}
          data={(this.item || []).slice()}
        />
      </>
    );
  }
}

export default SimilarAdsComponent;
