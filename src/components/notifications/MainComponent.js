import React from "react";
import ScreenWrapper from "../../ui/ScreenWrapper";
import {FlatList, View} from "react-native";
import {Card, Text} from "react-native-paper";
import {inject, observer, Observer} from "mobx-react";
import moment from 'moment';
import {APP_COLOR} from "../../utils/settings";
import {withTranslation} from 'react-i18next';

@withTranslation() @inject('store') @observer
class MainComponent extends React.Component {
  constructor(props) {
    super(props);

    this.store = this.props.store.appStore;
  }

  componentDidMount(): void {
    setTimeout(() => {
      this.store.markAsReadNotifications();
    }, 3000);
  }

  render() {
    const {t} = this.props;
    return (
      <ScreenWrapper style={{padding: 4}}>
        <Observer>
          {() => (
            <FlatList
              renderItem={({item}) => (
                <View style={{flex: 1}}>
                  <Card elevation={2} style={{margin: 4, padding: 8, flex: 1}}>
                    <Text>{item.is_new && <><View style={{width: 10, height: 10, backgroundColor: APP_COLOR, borderRadius: 10}}/>{' '}</>}{item.content}</Text>
                    <Text style={{fontSize: 12, color: 'grey'}}>{moment(item.created_at).calendar()}</Text>
                  </Card>
                </View>
              )}
              onRefresh={() => this.store.fetchNotifications()}
              refreshing={this.store.notification_busy}
              keyExtractor={(item, index) => index.toString()}
              data={this.store.notifications}
              ListEmptyComponent={(
                <View alignItems={'center'}>
                  <Text style={{color: 'grey', marginTop: 16}}>{t('Notifications_will_appear_here')}</Text>
                </View>
              )}
            />
          )}
        </Observer>
      </ScreenWrapper>
    );
  }
}

export default MainComponent;
