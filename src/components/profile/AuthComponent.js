import React from "react";
import ScreenWrapper from "../../ui/ScreenWrapper";
import {Button, Card, Text, TextInput} from "react-native-paper";
import {inject, observer} from "mobx-react";
import {action, computed, observable} from "mobx";
import PhoneNumber from "../../utils/PhoneNumber";
import {APP_VERSION} from "../../utils/settings";
import {requester} from "../../utils";

@inject('store') @observer
class AuthComponent extends React.Component {
  static defaultProps = {
    helpText: false,
  };

  constructor(props) {
    super(props);

    this.store = this.props.store.appStore;
  }

  phone_number_ref = React.createRef();
  code_ref = React.createRef();
  full_name_ref = React.createRef();

  @observable phone_number = '';
  @observable loading = false;
  @observable valid_phone_number = false;
  @observable phone_E164_format = null;
  @observable status = '';
  @observable code = '';
  @observable full_name = '';

  @action setValue = (name, value) => this[name] = value;

  @action send = async () => {
    if (this.button_disabled) return;
    this.phone_number_ref.blur();
    this.code_ref.blur && this.code_ref.blur();
    this.full_name_ref.blur && this.full_name_ref.blur();
    this.loading = true;
    try {
      let cmd = '';
      let params = {
        phone: this.phone_E164_format.slice(1),
        language: this.store.language,
        place: this.store.place.id,
        device: JSON.stringify({
          platform: 'android',
          app_version: APP_VERSION,
        }),
      };
      if (this.status === 'exists') {
        cmd = '/login';
        params['code'] = this.code;
      } else if (this.status === 'not_exists') {
        cmd = '/register';
        params['code'] = this.code;
        params['full_name'] = this.full_name;
      }
      const data = await requester.post(`/user/auth/phone${cmd}`, params);
      if (data.status === 'not_exists' || data.status === 'exists') {
        this.status = data.status;
        this.code_ref.focus();
        this.store.showSuccess('Мы отправили вам код!');
      } else {
        if (data.status === 'registered' || data.status === 'logged_in') {
          this.store.makeAuth(data.data, data.status);
        } else if (data.status === 'incorrect_code') {
          this.code = '';
          this.code_ref.focus();
          this.store.showError('Неверный код!');
        } else {
          this.store.showError('Unknown error!');
        }
      }
    } catch (e) {
      console.log(e);
    } finally {
      this.loading = false;
    }
  };

  @computed get button_disabled() {
    if (this.phone_number.length < 1 || this.loading || !this.valid_phone_number) return true;
    if (this.status === 'exists') {
      return this.code.length !== 4;
    } else if (this.status === 'not_exists') {
      return this.code.length !== 4 || this.full_name.length <= 0;
    }
  }

  @action inputOnChangeHandler = (name, value) => {
    if (name === 'phone_number') {
      const phone = new PhoneNumber();
      phone.parseFromString(value);
      this.valid_phone_number = phone.isValid();
      this.phone_E164_format = phone.getE164Format();
    }
    if (name in this) this[name] = value;
  };

  render() {
    return (
      <ScreenWrapper padding scroll>
        {this.props.helpText && (
          <Card style={{marginBottom: 8}} elevation={3}>
            <Card.Content>
              <Text>Вы должны быть авторизованным пользователем, чтобы использовать этот раздел.</Text>
            </Card.Content>
          </Card>
        )}

        <Card elevation={3}>
          <Card.Content>
            <Text style={{marginBottom: 8}}>Пожалуйста, авторизируйтесь</Text>
            <TextInput
              ref={r => this.phone_number_ref = r}
              label={'Телефон номер'}
              autoCompleteType={'tel'}
              keyboardType={'phone-pad'}
              returnKeyType={'next'}
              mode={'outlined'}
              blurOnSubmit={true}
              value={this.phone_number}
              dense={true}
              dataDetectorTypes={'phoneNumber'}
              autoFocus={true}
              onSubmitEditing={() => this.status !== '' ? this.code_ref.focus() : this.send()}
              textContentType={'telephoneNumber'}
              onChangeText={text => this.inputOnChangeHandler('phone_number', text)}
            />
            {(this.status !== '') && (
              <TextInput
                ref={r => this.code_ref = r}
                label={'Код подтверждения'}
                keyboardType={'numeric'}
                returnKeyType={'next'}
                mode={'outlined'}
                blurOnSubmit={true}
                onSubmitEditing={() => this.status === 'not_exists' ? this.full_name_ref.focus() : this.send()}
                value={this.code}
                dense={true}
                onChangeText={text => this.inputOnChangeHandler('code', text)}
              />
            )}
            {(this.status === 'not_exists') && (
              <TextInput
                ref={r => this.full_name_ref = r}
                label={'Полное имя'}
                returnKeyType={'next'}
                autoCompleteType={'name'}
                mode={'outlined'}
                blurOnSubmit={true}
                onSubmitEditing={() => this.send()}
                value={this.full_name}
                dense={true}
                onChangeText={text => this.inputOnChangeHandler('full_name', text)}
              />
            )}
            <Button loading={this.loading} disabled={this.button_disabled} onPress={() => this.send()}
                    style={{marginTop: 8}} mode={'contained'}>
              {this.status === '' ? 'Отправить код' : (this.status === 'exists' ? 'Подтвердить код' : 'Регистрация')}
            </Button>
          </Card.Content>
        </Card>
      </ScreenWrapper>
    );
  }
}

export default AuthComponent;
