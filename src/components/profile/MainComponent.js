import React from "react";
import {inject, observer} from "mobx-react";
import {View, FlatList} from 'react-native';
import {Avatar, Button, Colors, Card, Headline, Subheading, Text, Title, ActivityIndicator} from 'react-native-paper';
import {withTranslation} from "react-i18next";
import Icon from "react-native-vector-icons/MaterialIcons";
import {APP_COLOR} from "../../utils/settings";
import moment from "moment";
import AdItem from "../../ui/AdItem";
import RootNavigation from "../../utils/RootNavigation";
import {observable, action} from "mobx";
import {size} from "../../utils";

@withTranslation('profile') @inject('store') @observer
class MainComponent extends React.Component {
  @observable next_page_loading = false;
  @observable current_page = 1;

  constructor(props) {
    super(props);

    this.store = this.props.store.appStore;
    this.adStore = this.props.store.adStore;
  }

  componentDidMount() {
    this.adStore.fetchMyItems().then();
  }

  @action nextPage = async () => {
    if (this.next_page_loading || this.adStore.fetching_my_items) return;
    this.next_page_loading = true;
    this.current_page++;
    try {
      let s = this.adStore.my_items.items.all.length;
      await this.adStore.fetchMyItems(0, this.current_page);
      if (s === this.adStore.my_items.items.all.length) this.current_page--;
    } catch (e) {
      this.current_page--;
    } finally {
      this.next_page_loading = false;
    }
  };

  render() {
    if (!this.store.authenticated) return null;

    const {t} = this.props;
    const {user} = this.store;
    const last_action = moment(user.last_action);
    const is_online = moment().diff(last_action, 'minutes') <= 10;

    return (
      <FlatList
        style={{margin: 8}}
        data={(this.adStore.fetching_my_items ? [] : this.adStore.my_items.items.all).slice()}
        renderItem={({item}) => (
          <View style={{flex: 1}}>
            <AdItem data={item}/>
          </View>
        )}
        numColumns={2}
        keyExtractor={(item, index) => index.toString()}
        onEndReached={() => this.nextPage()}
        progressViewOffset={20}
        ListFooterComponent={this.next_page_loading ? <ActivityIndicator/> : null}
        ListFooterComponentStyle={{margin: size(8)}}
        ListHeaderComponent={(
          <>
            <View flexDirection={'column'} alignItems={'center'} style={{marginVertical: 25}} justifyContent={'center'}>
              {user.picture ? <Avatar.Image size={80} source={{uri: user.picture}}/> :
                <Avatar.Text size={80} label={user.short_name}/>}
              <View style={{marginTop: 15}} flexDirection={'row'} alignItems={'center'}>
                <Headline style={{
                  fontSize: 20,
                  marginRight: user.is_agent ? 5 : 0,
                  color: 'rgba(0, 0, 0, 0.6)',
                }}>{user.full_name}</Headline>
                {user.is_agent ? <Icon color={APP_COLOR} size={20} name={'verified-user'}/> : null}
              </View>
            </View>
            <Card>
              <Card.Content>
                <Text>Идентификатор: {user.id}</Text>
                <Text>Провайдер ID: {user.provider_id}</Text>
                <Text>Регион: {user.place[`title_${this.store.language}`]}</Text>
                <Text>{t('Last_action')} {is_online ? 'онлайн' : last_action.calendar()}</Text>
                <Text>{t('Registration_date')} {moment(user.created_at).calendar()}</Text>
              </Card.Content>
            </Card>
            <Card style={{marginTop: 8}}>
              <Card.Content>
                <Subheading>{t('balance')} ({user.balance} сом)</Subheading>
                {/*<Button style={{marginTop: 5}} mode={'text'}>{t('transactions')}</Button>*/}
                <Button onPress={() => this.props.navigation.navigate('Replenish')} style={{marginTop: 5}}
                        mode={'outlined'}>{t('replenish')}</Button>
              </Card.Content>
            </Card>
            <Title style={{marginTop: 8, color: Colors.grey600}}>{t('my_ads')} ({this.adStore.my_items.counts.all})</Title>
          </>
        )}
        ListEmptyComponent={(
          <Card style={{marginTop: 6}}>
            <Card.Content>
              {this.adStore.fetching_my_items ? (
                <ActivityIndicator/>
              ) : (
                <>
                  <Subheading style={{color: Colors.grey500, textAlign: 'center', fontWeight: 'bold'}}>{t('no_ad')}</Subheading>
                  <Subheading style={{color: Colors.grey600, textAlign: 'center'}}>{t('Want_to_add')}</Subheading>
                  <Button onPress={() => RootNavigation.navigate('AddAd')} style={{marginTop: 8}} mode={'contained'}>{t('To_add_an_advert')}</Button>
                </>
              )}
            </Card.Content>
          </Card>
        )}
      />
    );
  }
}

export default MainComponent;
