import React from 'react';
import {Button, Caption, Card, Headline, Paragraph, Subheading, Text} from "react-native-paper";
import {View, Image, Linking} from "react-native";
import ScreenWrapper from "../../ui/ScreenWrapper";
import {inject, observer} from "mobx-react";
import balanceImage from '../../images/balance-kg.jpg';
import elsomImage from '../../images/elsom.jpg';
import {withTranslation} from "react-i18next";

@withTranslation('replenish') @inject('store') @observer
class ReplenishComponent extends React.Component{
  constructor(props) {
    super(props);

    this.store = this.props.store.appStore;
  }

  render() {
    const {user} = this.store;
    const {t} = this.props;
    const whatsapp_text = 'Мен SOM.KG\'ден кайрылым жатам.';

    return (
      <ScreenWrapper padding scroll>
        <Headline style={{marginBottom: 8}}>{t('title')}</Headline>
        <Card>
          <Card.Content>
            <Subheading>{t('how_to')}</Subheading>
            <View style={{justifyContent: 'center', alignItems: 'center', flexDirection: 'column', marginVertical: 8}}>
              <Caption style={{fontSize: 15}}>{t('your_id')}</Caption>
              <Text style={{fontSize: 40}}>{user.id}</Text>
            </View>
            <Caption style={{fontSize: 14}}>{t('help_text.1')}</Caption>
            <Caption style={{fontSize: 14, marginTop: 8}}>{t('help_text.2')}</Caption>
            <View>
              <Image style={{height: 100, width: 'auto', resizeMode: 'contain'}} source={balanceImage} />
              <Image style={{height: 100, width: 'auto', resizeMode: 'contain'}} source={elsomImage} />
            </View>
            <Paragraph style={{marginTop: 8}}>{t('help_text.3')}</Paragraph>
            <Button
              icon={'whatsapp'}
              onPress={() => Linking.openURL(`whatsapp://send?phone=${'996775600691'}&text=${whatsapp_text}`)}
              style={{marginTop: 8}} mode={'contained'}
            >Открыть WhatsApp</Button>
          </Card.Content>
        </Card>
        <Card style={{marginTop: 8}}>
          <Card.Content>
            <Paragraph>Если у вас возникли проблемы с пополнением SOM кошелек, свяжитесь с нами.</Paragraph>
            <Paragraph>Контакты: +996 775 600 691</Paragraph>
            <Button onPress={() => Linking.openURL('tel:+996775600691')} mode={'outlined'} icon={'phone'} style={{marginTop: 8}}>Звонок</Button>
          </Card.Content>
        </Card>
      </ScreenWrapper>
    );
  }
}

export default ReplenishComponent;
