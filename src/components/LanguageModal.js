import React from 'react';
import {Portal, Dialog, RadioButton, Text} from 'react-native-paper';
import {TouchableOpacity} from 'react-native';
import {inject, observer} from 'mobx-react';
import {withTranslation} from 'react-i18next';

@withTranslation('home') @inject('store') @observer
class LanguageModal extends React.Component {
  constructor(props) {
    super(props);

    this.store = this.props.store.appStore;
  }

  onSelect = (lng) => {
    this.store.setLanguage(lng);
    this.props.onSelect(lng);
  };

  render() {
    if (!this.store.language) {
      return null;
    }

    const {t} = this.props;

    return (
      <Portal>
        <Dialog
          dismissable={true}
          onDismiss={() => this.props.dismiss()}
          visible={this.props.show}>
          <Dialog.Title>{t('select_language')}</Dialog.Title>
          <Dialog.Content>
            <RadioButton.Group value={this.store.language}>
              {[
                {name: 'ru', value: 'Русский'},
                {name: 'ky', value: 'Кыргызча'},
              ].map((v, k) => (
                <TouchableOpacity
                  key={k}
                  style={{alignItems: 'center', flexDirection: 'row'}}
                  onPress={() => this.onSelect(v.name)}>
                  <RadioButton value={v.name} onPress={() => this.onSelect(v.name)}/>
                  <Text>{v.value}</Text>
                </TouchableOpacity>
              ))}
            </RadioButton.Group>
          </Dialog.Content>
        </Dialog>
      </Portal>
    );
  }
}

export default LanguageModal;
